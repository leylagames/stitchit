﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public class FinishUI : MonoBehaviour
{
    public int totalUnlockingItems;
    public TextMeshProUGUI finishPercentageText;
    public Image finishPercentageFillImage;
    public TextMeshProUGUI cashEarnedBasic, cashEarnedSpecial;
    public AudioSource winSound;
    public DOTweenAnimation continueButtonAnim;
    public GameObject comparisonPanel, nextToolPanel;
    public Text comparisonHeading;
    public Text newToolHeading;
    public GameObject getToolButton;
    public Image nextToClaimSprite;
    public Text itemHeading;
    public string[] itemHeadingText;

    public ParticleSystem confetti;
    
    public Sprite[] itemImages;
    public Sprite[] itemFillImages;

    public Image unlockItemBG, unlockItemFillImage;
    public Text fillPercentage;
    
    public int[] unlockingLevels;

    private int _finishPercentage;
    private int _currentLevel;

    public static bool once;

    private int earnedCash;

    [BoxGroup("Match Image Properties"), SerializeField]
    public Camera _camera;
    public Camera _secondaryCamera;

    [BoxGroup("Match Image Properties"), SerializeField]
    RawImage userImage;

    [BoxGroup("Match Image Properties"), SerializeField]
    Image requiredImage; 
    
    [BoxGroup("Match Image Properties"), SerializeField]
    Material projectorMat;

    private void Start()
    {
        once = false;
        requiredImage.sprite = DataCarrier.Instance.levelsData[GameManager.Instance.CurrentLevel].matchSprite;
        comparisonHeading.text = "LEVEL " + (GameManager.Instance.CurrentLevel + 1).ToString() + " \n " + "COMPLETE";
    }

    public void Initialize(int finishPercentage)
    {
        _finishPercentage = finishPercentage;
        cashEarnedBasic.text = finishPercentage.ToString();
        earnedCash = finishPercentage;
        gameObject.SetActive(true);
    }

    private void OnEnable()
    {
        _camera.gameObject.SetActive(true);
        StartCoroutine(FillRoutine(_finishPercentage, 1f));
    }

    public void SaveRenderToImage()
    {
        RenderTexture rt;
        rt = _camera.activeTexture;
        RenderTexture.active = rt;
        Texture2D tex = new Texture2D(rt.width, rt.height, TextureFormat.RGBA32, false);
        tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        RenderTexture.active = null;

        byte[] bytes;
        bytes = tex.EncodeToPNG();
        string path = Application.persistentDataPath + "/" + DataCarrier.Instance.levelsData[GameManager.Instance.CurrentLevel].lvlPrefab.name + ".png";
        File.WriteAllBytes(path, bytes);
        
        PlayerPrefs.SetString("Level" + (GameManager.Instance.CurrentLevel).ToString() , path);
        DataCarrier.Instance.LoadSprites(GameManager.Instance.CurrentLevel);
        
    }

    [Button]
    public void SaveImage()
    {
        RenderTexture rt;
        rt = _camera.activeTexture;
        RenderTexture.active = rt;
        Texture2D tex = new Texture2D(rt.width, rt.height, TextureFormat.RGBA32, false);
        tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        RenderTexture.active = null;

        byte[] bytes;
        bytes = tex.EncodeToPNG();
        string path = Application.persistentDataPath + "/" + "testCapture" + ".png";
        File.WriteAllBytes(path, bytes);
    }
    
    public void ConvertRendererToTexture()
    {
        _camera.gameObject.SetActive(true);
        RenderTexture rt = _camera.activeTexture;

        RenderTexture.active = rt;
        Texture2D tex = new Texture2D(rt.width, rt.height, TextureFormat.RGBA32, false);
        tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        RenderTexture.active = null;

        byte[] bytes;
        bytes = tex.EncodeToPNG();
        Texture2D texture = new Texture2D(1, 1);
        texture.LoadImage(bytes);
        
        projectorMat.mainTexture = texture;
        
        
    }

    public IEnumerator FillRoutine(int finishPercentage, float duration)
    {
        if (once)
        {
            yield break;
        }

        once = true;

        float counter = 0;
        float startValue = 0;
        float endValue = startValue + (float) finishPercentage;
        float newValue = startValue;

        earnedCash = finishPercentage;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            newValue = Mathf.Lerp(startValue, endValue, counter / duration);
            finishPercentageFillImage.fillAmount = newValue / 100f;
            finishPercentageText.text = Mathf.FloorToInt(newValue) + "%";
            yield return null;
        }

        winSound.Play();
        
        if (PlayerPrefs.GetInt("ItemToUnlock") > totalUnlockingItems - 1)
        {
            continueButtonAnim.DOPlay();
            confetti.gameObject.SetActive(true);
        }
        else if(!PlayerPrefs.HasKey("DoneLevel" + GameManager.Instance.CurrentLevel.ToString()))
        {
            PlayerPrefs.SetInt("DoneLevel" + GameManager.Instance.CurrentLevel.ToString(),1);
            confetti.gameObject.SetActive(true);
            yield return new WaitForSeconds(1f);
            comparisonPanel.transform.GetChild(1).DOScale(Vector3.zero, 0.5f);
            comparisonPanel.transform.GetChild(0).DOScale(Vector3.zero, 0.5f);
            yield return new WaitForSeconds(0.5f);
            var value = ShowNextToClaim();
            nextToolPanel.transform.GetChild(1).DOScale(Vector3.one * 0.5f, 0.5f);
            nextToolPanel.transform.GetChild(0).DOScale(Vector3.one , 0.5f);
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(FillRoutineCash(value, 1));
        }
        else
        {
            continueButtonAnim.DOPlay();
            confetti.gameObject.SetActive(true);
        }
    }

    public float ShowNextToClaim()
    {
        nextToClaimSprite.enabled = true;
        newToolHeading.text = "NEXT TO CLAIM";
        int itemToUnlock = PlayerPrefs.GetInt("ItemToUnlock");
        unlockItemBG.sprite = itemImages[itemToUnlock];
        unlockItemFillImage.sprite = itemFillImages[itemToUnlock];
        unlockItemFillImage.fillAmount = PlayerPrefs.GetFloat("EndValue");
        fillPercentage.text = (unlockItemFillImage.fillAmount * 100 ).ToString("F0") + " %";

        itemHeading.text = itemHeadingText[itemToUnlock];

        float value = 0.0f;
        
        if (itemToUnlock > 0)
        {
            value = unlockingLevels[itemToUnlock] - unlockingLevels[itemToUnlock - 1];
        }
        else
        {
            value = unlockingLevels[itemToUnlock];
        }

        value = 1f / value;
        return value;
    }

    public IEnumerator FillRoutineCash(float percentageIncrease, float duration)
    {
        float counter = 0;
        float startValue = PlayerPrefs.GetFloat("EndValue");
        float endValue = startValue + percentageIncrease;
        float newValue = startValue;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            newValue = Mathf.Lerp(startValue, endValue, counter / duration);
            unlockItemFillImage.fillAmount = newValue;
            fillPercentage.text = (unlockItemFillImage.fillAmount * 100 ).ToString("F0") + " %";
            yield return null;
        }
        
        if (endValue > 0.99f)
        {
            confetti.Play();
            PlayerPrefs.SetFloat("EndValue",0);
            newToolHeading.text = "NEW TOOL" + "\n" + "UNLOCKED";
            getToolButton.SetActive(true);
            var _toolUnlockedValue = PlayerPrefs.GetInt("ItemToUnlock");
            StorePlayerPrefs(_toolUnlockedValue);
            PlayerPrefs.SetInt("ItemToUnlock" , _toolUnlockedValue + 1);
        }
        else
        {
            PlayerPrefs.SetFloat("EndValue",endValue);
        }
        
        continueButtonAnim.DOPlay();
    }

    void StorePlayerPrefs(int number)
    {
        switch (number)
        {
            case 0:
                PlayerPrefs.SetInt("Machine1",1);
                PlayerPrefs.SetInt("SelectedMachine",PlayerPrefs.GetInt("SelectedMachine") + 1);
                break;
            case 1:
                PlayerPrefs.SetInt("Stitch1",1);
                //PlayerPrefs.SetInt("SelectedStitch",PlayerPrefs.GetInt("SelectedStitch") + 1);
                break;
            case 2:
                PlayerPrefs.SetInt("Stitch2",1);
                PlayerPrefs.SetInt("SelectedStitch",PlayerPrefs.GetInt("SelectedStitch") + 1);
                break;
            case 3:
                PlayerPrefs.SetInt("Machine2",1);
                PlayerPrefs.SetInt("SelectedMachine",PlayerPrefs.GetInt("SelectedMachine") + 1);
                break;
            case 4:
                PlayerPrefs.SetInt("Machine3",1);
                PlayerPrefs.SetInt("SelectedMachine",PlayerPrefs.GetInt("SelectedMachine") + 1);
                break;
        }
    }

    public void NextLevelButton()
    {
        SaveRenderToImage();
        GameController.Instance.HandleFinishGameComplete();
        PlayerPrefs.SetInt("Currency", PlayerPrefs.GetInt("Currency") + earnedCash);
    }

    public void SaveCurrentStitchModelForProjection()
    {
        ConvertRendererToTexture();
    }

    public void RestartButton()
    {
        GameController.Instance.HandleFinishGameFailed();
    }
}
