using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

namespace Leyla.Utility
{
    [ExecuteInEditMode]
    public class ChaseCam : MonoBehaviour
    {
        public Transform LookAt;
        public Transform[] MoveTo;
        public Transform RollTo;
        public float ChaseTime=0.5f;

        public float shakeDuration = .5f;
        public float shakeAmount = 0.7f;
        public float decreaseFactor = 1.0f;

        private Vector3 shakeMoveTo;
        private Vector3 mVelocity;
        private Vector3 mRollVelocity;
        private int cameraMode;
        private float chaseTimeReserve;

        private void Awake()
        {
            chaseTimeReserve = ChaseTime;
        }
#if UNITY_EDITOR
        void Update()
        {
            if (!Application.isPlaying)
            {
                if (MoveTo[cameraMode])
                    transform.position = MoveTo[cameraMode].position;
                if (LookAt)
                {
                    if (!RollTo) transform.LookAt (LookAt);
                    else transform.LookAt (LookAt, RollTo.up);
                }
                // if (RollTo)
                //     transform.rotation = Quaternion.Euler (new Vector3 (transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, RollTo.rotation.eulerAngles.z));
            }
        }
#endif

        // Update is called once per frame
        void LateUpdate()
        {
            if(shakeDuration > 0)
            {
                transform.localPosition = shakeMoveTo + transform.InverseTransformPoint(MoveTo[cameraMode].position) + Random.insideUnitSphere * shakeAmount;
                shakeDuration -= Time.deltaTime * decreaseFactor;
            }
            else
            {
                if (MoveTo.Length > 0)
                    transform.position = Vector3.SmoothDamp(transform.position, MoveTo[cameraMode].position, ref mVelocity, ChaseTime);
                if (LookAt)
                {
                    if (!RollTo) transform.LookAt(LookAt);
                    else transform.LookAt(LookAt, Vector3.SmoothDamp(transform.up, RollTo.up, ref mRollVelocity, ChaseTime));
                }
            }
          
            // if (RollTo)
            //     transform.rotation = Quaternion.Euler (new Vector3 (transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, RollTo.rotation.eulerAngles.z));
        }

        public void DisableRotation()
        {
            ChaseTime = chaseTimeReserve * 4f;
        }

        public void EnableRotation()
        {
            ChaseTime = chaseTimeReserve;
        }

        public void Finish()
        {
            MoveTo[cameraMode].localPosition = new Vector3(-6, 0, 0);
        }

        [Button("Shake")]
        public void Shake()
        {
            shakeMoveTo = transform.localPosition - transform.InverseTransformPoint(MoveTo[cameraMode].position);
            shakeDuration = .5f;
        }

        public void SetCameraMode(int cMode)
        {
            Debug.Log("Camera Mode: " + cMode);
            if(MoveTo.Length > cMode)
            {
                this.cameraMode = cMode;
            }
        }
    }
}
