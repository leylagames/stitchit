﻿using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerScriptableObject", order = 1)]
public class SpawnManagerScriptableObject : ScriptableObject
{
    public GameObject asset;
    [Button("DoIt")]
    public void DoIt()
    {
        //SaveTextureAsPNG(UnityEditor.AssetPreview.GetMiniThumbnail(asset));
    }

    public static void SaveTextureAsPNG(Texture2D _texture)
    {
        byte[] _bytes = _texture.EncodeToPNG();
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/MyPNG.png", _bytes);
    }
}