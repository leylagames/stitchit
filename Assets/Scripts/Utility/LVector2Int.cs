﻿using UnityEngine;

namespace Leyla.Utility
{
    [System.Serializable]
    public struct LVector2Int
    {
        public int X;
        public int Y;

        public LVector2Int(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        public LVector2Int(Vector2 vector2)
        {
            X = (int)vector2.x;
            Y = (int)vector2.y;
        }
        public Vector2 GetVector2()
        {
            return new Vector2(X, Y);
        }

        public void SetFromVector2(Vector2 vector2)
        {
            X = (int)vector2.x;
            Y = (int)vector2.y;
        }

        public void Add(LVector2Int addedVector)
        {
            X += addedVector.X;
            Y += addedVector.Y;
        }
    }
}
  


