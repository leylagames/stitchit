﻿using Facebook.Unity;
using System.Collections.Generic;

namespace Leyla.Utility
{
    public class EventSender
    {
        public static void SendCompleteLevelEvent(string levelName)
        {
            var levelParams = new Dictionary<string, object>();
            levelParams[AppEventParameterName.Level] = levelName + "_Completed";
            FB.LogAppEvent(AppEventName.AchievedLevel, parameters: levelParams);
        }

        public static void SendStartLevelEvent(string levelName)
        {
            var levelParams = new Dictionary<string, object>();
            levelParams[AppEventParameterName.Level] = levelName + "_Start";
            FB.LogAppEvent(AppEventName.AchievedLevel, parameters: levelParams);
        }

        public static void SendReplayLevelEvent(string levelName)
        {
            var levelParams = new Dictionary<string, object>();
            levelParams[AppEventParameterName.Level] = levelName + "_Replay";
            FB.LogAppEvent(AppEventName.AchievedLevel, parameters: levelParams);
        }
    }
}

