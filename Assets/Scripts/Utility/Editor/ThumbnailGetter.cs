﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class ThumbnailGetter : MonoBehaviour
{
    public GameObject asset;
    // Start is called before the first frame update
    [Button("DoIt")]
    public void DoIt()
    {
        SaveTextureAsPNG(UnityEditor.AssetPreview.GetMiniThumbnail(asset));
    }

    public static void SaveTextureAsPNG(Texture2D _texture)
    {
        byte[] _bytes = _texture.EncodeToPNG();
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/MyPNG.png", _bytes);
    }
}
