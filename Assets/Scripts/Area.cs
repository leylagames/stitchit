﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(Outline))]
public class Area : MonoBehaviour
{
    public int areaId;
    private MeshRenderer _meshRenderer;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshRenderer.enabled = false;
    }

    public void HightLight(bool enable)
    {
        if (enable)
        {
            _meshRenderer.enabled = true;
        }
        else
        {
            _meshRenderer.enabled = false;
        }
    }

    void Update()
    {
        //_myMaterial.color = Color.Lerp(_myMaterial.color, _targetColor, Time.deltaTime * _deltaSpeed);
    }
}
