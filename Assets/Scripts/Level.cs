﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Transform carpet;
    public Transform pathContainer;
    public Transform stichesContainer;

    public Transform obstaclesContainer;
    public Transform overlockStitchContainer;

    public PointType[] extraColors;

    public bool hasOverlocking;
    

    [System.Serializable]
    public class TransformSaver
    {
        public Vector3 pos;
        public Quaternion rot;
        public Vector3 scale;
    }

    [SerializeField] public TransformSaver levelTransform;

    
    [Button]
    public void SaveTransform()
    {
        levelTransform.pos = transform.localPosition;
        levelTransform.rot = transform.localRotation;
        levelTransform.scale = transform.localScale;
    }

    [Button]
    public void ApplyTransform()
    {
        transform.localPosition = levelTransform.pos;
        transform.localRotation = levelTransform.rot;
        transform.localScale = levelTransform.scale;
    }
    

    private void Start()
    {
        if (extraColors.Length > 0)
        {
            for (int i = 0; i < extraColors.Length; i++)
            {
                GameController.Instance.AddExtraColors((int)extraColors[i]);
            }
        }

        if (!DataCarrier.Instance.useObstacles)
        {
            obstaclesContainer.gameObject.SetActive(false);
        }
    }

    [Button]
    public void EnableStitches()
    {
        
        for (int i = 0; i < pathContainer.childCount; i++)
        {
            pathContainer.GetChild(i).GetComponent<MeshRenderer>().enabled = true;
        }

        obstaclesContainer.gameObject.SetActive(false);

        // for (int j = 0; j < overlockStitchContainer.childCount; j++)
        // {
        //     overlockStitchContainer.GetChild(j).transform.GetChild(0).gameObject.SetActive(true);
        //     overlockStitchContainer.GetChild(j).transform.GetChild(0).gameObject.layer = 0;
        //     overlockStitchContainer.GetChild(j).transform.GetChild(1).gameObject.SetActive(true);
        //     overlockStitchContainer.GetChild(j).transform.GetChild(1).gameObject.layer = 0;
        // }
    }

    [Button]
    public void DisableStitches()
    {
        
        for (int i = 0; i < pathContainer.childCount; i++)
        {
            pathContainer.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
        }
        obstaclesContainer.gameObject.SetActive(true);
        
        // for (int j = 0; j < overlockStitchContainer.childCount; j++)
        // {
        //     overlockStitchContainer.GetChild(j).transform.GetChild(0).gameObject.SetActive(false);
        //     overlockStitchContainer.GetChild(j).transform.GetChild(0).gameObject.layer = 10;
        //     overlockStitchContainer.GetChild(j).transform.GetChild(1).gameObject.SetActive(false);
        //     overlockStitchContainer.GetChild(j).transform.GetChild(1).gameObject.layer = 10;
        // }
    }
    
}

public enum PointType
{
    None = -1,
    PinkLight = 0,
    PinkMedium = 1,
    PinkDark = 2,
    PurpleLight = 3,
    PurpleMedium = 4,
    PurpleDark = 5,
    OrangeLight = 6,
    OrangeMedium = 7,
    OrangeDark = 8,
    YellowLight = 9,
    YellowMedium = 10,
    YellowDark = 11,
    GreenLight = 12,
    GreenMedium = 13,
    GreenDark = 14,
    TurquoiseLight = 15,
    TurquoiseMedium = 16,
    TurquoiseDark = 17,
    BlueLight = 18,
    BlueMedium = 19,
    BlueDark = 20,
    BlackLight = 21,
    BlackMedium = 22,
    BlackDark = 23,
    WhiteLight = 24,
    WhiteMedium = 25,
    WhiteDark = 26,
    RedLight = 27,
    RedMedium = 28,
    RedDark = 29,
    DGreenLight = 30,
    DGreenMedium = 31,
    DGreenDark = 32,
    VioletLight = 33,
    VioletMedium = 34,
    VioletDark = 35,
    Silver = 36,
    Golden = 37,
    Rainbow = 38
}
