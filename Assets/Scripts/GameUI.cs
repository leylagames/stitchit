﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
	public UIController uiController;
	public GameObject[] stitchLocks;
	public GameObject[] machineLocks;
	public GameObject[] colorButtons;
	public GameObject[] stitchButtons;
	public GameObject[] machineButtons;
	public Image[] stitchIcons;
	public GameObject levelProgressbar;
	public Image levelIconImage;
	public Image progressFillImage;
	public TextMeshProUGUI levelText;
	public GameObject toolsPanel;
	public GameObject selectedColorPointer;
	public GameObject selectedStitchPointer;
	public GameObject selectedMachinePointer;
	public GameObject undoButton;
	public GameObject stitchEventTriggerButton;
	public GameObject autoSelection;
	public GameObject colorPalette, stitchPalette, machinePalette;
	public GameObject colorSelectButton, stitchSelectButton, machineSelectButton;
	public RectTransform buttonsSelection;
	public GameObject referencePicture;

	public Text playerCashLabel;

	public HorizontalLayoutGroup group;
	public List<int> values;

	public bool stitchButton;

	
	void Start()
	{
		for (int i = 0; i < colorButtons.Length; i++)
		{
			int index = i;
			colorButtons[index].GetComponent<Button>().onClick.AddListener(() =>
			{
				SelectColor(index);
				
			});
			
			EventTrigger colorTrigger = colorButtons[i].GetComponent<EventTrigger>();

			EventTrigger.Entry buttonDown = new EventTrigger.Entry();
			buttonDown.eventID = EventTriggerType.PointerDown;
			buttonDown.callback.AddListener((data) =>
			{
				//GameController.Instance.startMachineMovement = true;
				GameController.Instance.tutorial.HideTutorial();
			});
			colorTrigger.triggers.Add(buttonDown);

			EventTrigger.Entry buttonUp = new EventTrigger.Entry();
			buttonUp.eventID = EventTriggerType.PointerUp;
			buttonUp.callback.AddListener((data) =>
			{
				//GameController.Instance.startMachineMovement = false;
			});
			colorTrigger.triggers.Add(buttonUp);
			
			EventTrigger.Entry buttonHover = new EventTrigger.Entry();
			buttonHover.eventID = EventTriggerType.PointerEnter;
			buttonHover.callback.AddListener((data) =>
			{
				SelectColor(index);
			});
			colorTrigger.triggers.Add(buttonHover);
		}

		for (int j = 0; j < stitchButtons.Length; j++)
		{
			int index = j;
			stitchButtons[index].GetComponent<Button>().onClick.AddListener(() =>
			{
				SelectStitch(index); 
				
			});
			
			EventTrigger stitchTrigger = stitchButtons[j].GetComponent<EventTrigger>();
			
			EventTrigger.Entry buttonDown = new EventTrigger.Entry();
			buttonDown.eventID = EventTriggerType.PointerDown;
			buttonDown.callback.AddListener((data) =>
			{
				SelectStitch(index);
				//GameController.Instance.startMachineMovement = true;
				GameController.Instance.tutorial.HideTutorial();
			});
			stitchTrigger.triggers.Add(buttonDown);
			
			EventTrigger.Entry buttonUp = new EventTrigger.Entry();
			buttonUp.eventID = EventTriggerType.PointerUp;
			buttonUp.callback.AddListener((data) =>
			{
				GameController.Instance.startMachineMovement = false;
			});
			stitchTrigger.triggers.Add(buttonUp);
		}
		
		for (int k = 0; k < machineButtons.Length; k++)
		{
			int index = k;
			machineButtons[index].GetComponent<Button>().onClick.AddListener(() =>
			{
				SelectMachine(index); 
			});
			
			EventTrigger machineTrigger = machineButtons[k].GetComponent<EventTrigger>();
			
			EventTrigger.Entry buttonDown = new EventTrigger.Entry();
			buttonDown.eventID = EventTriggerType.PointerDown;
			buttonDown.callback.AddListener((data) =>
			{
				SelectMachine(index);
				//GameController.Instance.startMachineMovement = true;
				GameController.Instance.tutorial.HideTutorial();
			});
			machineTrigger.triggers.Add(buttonDown);
			
			EventTrigger.Entry buttonUp = new EventTrigger.Entry();
			buttonUp.eventID = EventTriggerType.PointerUp;
			buttonUp.callback.AddListener((data) =>
			{
				GameController.Instance.startMachineMovement = false;
			});
			machineTrigger.triggers.Add(buttonUp);
		}
		

		EventTrigger trigger = stitchEventTriggerButton.GetComponent<EventTrigger>();
		
		EventTrigger.Entry entryDown = new EventTrigger.Entry();
		entryDown.eventID = EventTriggerType.PointerDown;
		entryDown.callback.AddListener((data) =>
		{
			stitchButton = true;
			GameController.Instance.startMachineMovement = true;
			GameController.Instance.tutorial.HideTutorial();
			ColorButtonClick();
		});
		trigger.triggers.Add(entryDown);
		
		EventTrigger.Entry entryUp = new EventTrigger.Entry();
		entryUp.eventID = EventTriggerType.PointerUp;
		entryUp.callback.AddListener((data) =>
		{
			stitchButton = false;
			GameController.Instance.startMachineMovement = false;
		});
		trigger.triggers.Add(entryUp);

		colorSelectButton.GetComponent<Button>().onClick.AddListener(() => { ColorButtonClick(); });
		stitchSelectButton.GetComponent<Button>().onClick.AddListener(() => { StitchButtonClick(); });
		machineSelectButton.GetComponent<Button>().onClick.AddListener(() => { MachineButtonClick(); });

		
		SetSelectionPointersColor();
		SetSelectionPointersStitches();
		SetSelectionPointersMachines();
		
		SelectStitch(PlayerPrefs.GetInt("SelectedStitch"));
		SelectMachine(PlayerPrefs.GetInt("SelectedMachine"));
		
		UpdateLocks();
		
		// if (PlayerPrefs.GetInt("Manual") == 1)
		// {
		// 	SelectManualControls();
		// }
		// else
		// {
		// 	SelectAutoControls();
		// }
	}

	private void UpdateLocks()
	{
		if (PlayerPrefs.HasKey("Stitch1"))
		{
			stitchLocks[0].SetActive(false);
		}
		else
		{
			stitchLocks[0].SetActive(true);
		}
		
		if (PlayerPrefs.HasKey("Stitch2"))
		{
			stitchLocks[1].SetActive(false);
		}
		else
		{
			stitchLocks[1].SetActive(true);
		}
		
		if (PlayerPrefs.HasKey("Machine1"))
		{
			machineLocks[0].SetActive(false);
		}
		else
		{
			machineLocks[0].SetActive(true);
		}
		
		if (PlayerPrefs.HasKey("Machine2"))
		{
			machineLocks[1].SetActive(false);
		}
		else
		{
			machineLocks[1].SetActive(true);
		}
		
		if (PlayerPrefs.HasKey("Machine3"))
		{
			machineLocks[2].SetActive(false);
		}
		else
		{
			machineLocks[2].SetActive(true);
		}
		
		
	}

	private void Update()
	{
		playerCashLabel.text = PlayerPrefs.GetInt("Currency").ToString();
	}

	public void ShowLevelReference(int index)
	{
		levelIconImage.sprite = DataCarrier.Instance.levelsData[index].refrenceSprite;
	}

	public void SetLevel(int levelId)
	{
		levelText.text = "LEVEL " + ++levelId + "";
	}

	public void SetProgress(float progress)
	{
		progressFillImage.fillAmount = (progress);
	}
	
	public void SelectColor(int colorId)
	{
		GameController.Instance.selectedColorIndex = colorId;
		GameController.Instance.ChangeSewingMachineThreadColor();
		SetSelectionPointersColor();
	}

	public void SetSelectionPointersColor()
	{
		selectedColorPointer.transform.SetParent(colorButtons[GameController.Instance.selectedColorIndex].transform);
		selectedColorPointer.transform.SetSiblingIndex(4);
		selectedColorPointer.transform.position =
			colorButtons[GameController.Instance.selectedColorIndex].transform.position;
		for (int i = 0; i < stitchIcons.Length; i++)
		{
			stitchIcons[i].color = colorButtons[GameController.Instance.selectedColorIndex].transform.GetChild(2)
				.GetComponent<Image>().color;
		}
	}

	public void SetSelectionPointersStitches()
	{
		selectedStitchPointer.transform.SetParent(stitchButtons[GameController.Instance.selectedStichIndex].transform);
		selectedStitchPointer.transform.SetSiblingIndex(0);
		selectedStitchPointer.transform.position =
			stitchButtons[GameController.Instance.selectedStichIndex].transform.position;
	}
	
	public void SetSelectionPointersMachines()
	{
		selectedMachinePointer.transform.SetParent(machineButtons[GameController.Instance.selectedMachineIndex].transform);
		selectedMachinePointer.transform.SetSiblingIndex(0);
		selectedMachinePointer.transform.position =
			machineButtons[GameController.Instance.selectedMachineIndex].transform.position;
	}

	public void SelectStitch(int stitchId)
	{
		if (stitchId > 0)
		{
			if (PlayerPrefs.HasKey("Stitch" + stitchId))
			{
				GameController.Instance.selectedStichIndex = stitchId;
				SetSelectionPointersStitches();
				PlayerPrefs.SetInt("SelectedStitch",stitchId);
			}
		}
		else
		{
			GameController.Instance.selectedStichIndex = stitchId;
			SetSelectionPointersStitches();
			PlayerPrefs.SetInt("SelectedStitch",stitchId);
		}
		

		// if (GameController.Instance.oldFollower)
		// {
		// 	GameController.Instance.oldFollower.lengthBetweenStitches =
		// 		GameController.Instance.stitchesDistance[stitchId];
		// }
		//
		// if (GameController.Instance.newFollower)
		// {
		// 	GameController.Instance.newFollower.lengthBetweenStitches =
		// 		GameController.Instance.stitchesDistance[stitchId];
		// }
	}

	public void HideGameplayPanels()
	{
		levelProgressbar.SetActive(false);
		toolsPanel.SetActive(false);
		selectedColorPointer.SetActive(false);
		selectedStitchPointer.SetActive(false);
	}

	public void MainMenuButtonClick()
	{
		SceneManager.LoadScene(0);
	}

	public void SelectMachine(int index)
	{
		if (index > 0)
		{
			if (PlayerPrefs.HasKey("Machine" + index))
			{
				GameController.Instance.SelectMachine(index);
				SetSelectionPointersMachines();
				PlayerPrefs.SetInt("SelectedMachine",index);
			}
		}
		else
		{
			GameController.Instance.SelectMachine(index);
			SetSelectionPointersMachines();
			PlayerPrefs.SetInt("SelectedMachine",index);
		}
	}

	public void RefreshUI(int colorsLength)
	{
		// colorsLength = colorsLength + GameController.Instance.currentLevel.extraColors.Length;
		// if (colorsLength > 0 && colorsLength < 7)
		// {
		//     group.padding.left = values[colorsLength - 1];
		// }
	}

	public void ColorButtonClick()
	{
		buttonsSelection.transform.SetParent(colorSelectButton.transform);
		buttonsSelection.localPosition = new Vector3(0, 0, 0);
		buttonsSelection.transform.SetSiblingIndex(0);
		colorPalette.SetActive(true);
		stitchPalette.SetActive(false);
		machinePalette.SetActive(false);
	}

	public void StitchButtonClick()
	{
		buttonsSelection.transform.SetParent(stitchSelectButton.transform);
		buttonsSelection.localPosition = new Vector3(0, 0, 0);
		buttonsSelection.transform.SetSiblingIndex(0);
		colorPalette.SetActive(false);
		stitchPalette.SetActive(true);
		machinePalette.SetActive(false);
	}

	public void MachineButtonClick()
	{
		buttonsSelection.transform.SetParent(machineSelectButton.transform);
		buttonsSelection.localPosition = new Vector3(0, 0, 0);
		buttonsSelection.transform.SetSiblingIndex(0);
		colorPalette.SetActive(false);
		stitchPalette.SetActive(false);
		machinePalette.SetActive(true);
	}

	public void AutoButtonClick()
	{
		if (GameController.Instance.colorFollower.thisMovementMode == ColorControlFollower.MovementMode.Manual)
		{
			SelectAutoControls();
		}
		else
		{
			SelectManualControls();
		}
	}

	public void SelectManualControls()
	{
		GameController.Instance.tckCanvas.SetActive(true);
		GameController.Instance.colorFollower.thisMovementMode = ColorControlFollower.MovementMode.Manual;
		autoSelection.SetActive(true);
		PlayerPrefs.SetInt("Manual",1);
		
		for (int i = 0; i < colorButtons.Length; i++)
		{
			colorButtons[i].GetComponent<Button>().enabled = true;
			colorButtons[i].GetComponent<EventTrigger>().enabled = false;
		}

		for (int j = 0; j < stitchButtons.Length; j++)
		{
			stitchButtons[j].GetComponent<Button>().enabled = true;
			stitchButtons[j].GetComponent<EventTrigger>().enabled = false;
		}
		
		for (int k = 0; k < machineButtons.Length; k++)
		{
			machineButtons[k].GetComponent<Button>().enabled = true;
			machineButtons[k].GetComponent<EventTrigger>().enabled = false;
		}
	}

	public void SelectAutoControls()
	{
		GameController.Instance.tckCanvas.SetActive(false);
		GameController.Instance.colorFollower.thisMovementMode = ColorControlFollower.MovementMode.Automatic;
		autoSelection.SetActive(false);
		PlayerPrefs.SetInt("Manual",0);
		
		for (int i = 0; i < colorButtons.Length; i++)
		{
			colorButtons[i].GetComponent<Button>().enabled = false;
			colorButtons[i].GetComponent<EventTrigger>().enabled = true;
		}

		for (int j = 0; j < stitchButtons.Length; j++)
		{
			stitchButtons[j].GetComponent<Button>().enabled = false;
			stitchButtons[j].GetComponent<EventTrigger>().enabled = true;
		}
		
		for (int k = 0; k < machineButtons.Length; k++)
		{
			machineButtons[k].GetComponent<Button>().enabled = false;
			machineButtons[k].GetComponent<EventTrigger>().enabled = true;
		}
	}

}