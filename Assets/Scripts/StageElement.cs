﻿using UnityEngine;

public class StageElement : MonoBehaviour
{
    public int stageNum;
    public GameObject[] stageElements;

    public void SetStage(int stageNum)
    {
        DeactivateStageElements();
        if(stageNum < this.stageNum)
        {
            stageElements[0].SetActive(true);
        }
        else if(stageNum == this.stageNum)
        {
            stageElements[1].SetActive(true);
        }
        else
        {
            stageElements[2].SetActive(true);
        }
    }

    public void DeactivateStageElements()
    {
        foreach(GameObject stageElement in stageElements)
        {
            stageElement.SetActive(false);
        }
    }
}
