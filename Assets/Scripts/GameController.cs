﻿using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    public GameObject tckCanvas;
    public static GameController Instance;
    public        MeshRenderer[] stichesModels;
    public        Material[]     stichMaterials;
    public        GameObject[]   sewingMachineTypes;
    public        MeshRenderer     extraStitchModel;
    public        MeshRenderer[]     extraIrregularStitchModel;
    public GameObject frameParent;

    public TutorialNew          tutorial;
    public UIController      uiController;
    public ColorControlFollower colorFollower;
    public Transform         sewingFinalPos;
    public GameObject autoManualSwitchButton;
    public FlipMechanics flipMechanics;
    public OverlockMachine overlockMachine;

    public int currentAreaID;

    [HideInInspector] public float correctProgress;
    [HideInInspector] public int totalWaypoints;
    [HideInInspector] public int selectedStichIndex;
    [HideInInspector] public int selectedMachineIndex;
    [HideInInspector] public int selectedColorIndex;
    [HideInInspector] public SewingMachineScript _machineScript;

    public Level currentLevel;
    public Transform referenceHandSprite;
    public UIDetector uiDetector;

    public bool startMachineMovement;
    
    private int _levelId;
    private int _finishPercentage;

    public ParticleSystem machineParticles;

    public ParticleSystem[]  interactiveParticles;
    int                      randomIndex = -1;
    [SerializeField] Vector2 minPos;
    [SerializeField] Vector2 maxPos;
    [SerializeField] Vector2 randomPos;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
        colorFollower.enabled = true;
        if (GameManager.Instance.CurrentLevel > 2)
        {
            colorFollower.SetFlippingMechanics(true);
        }
        else
        {
            colorFollower.SetFlippingMechanics(false);
        }

        colorFollower.thisMovementMode = ColorControlFollower.MovementMode.Automatic;
        autoManualSwitchButton.SetActive(false);
        tckCanvas.SetActive(false);


        if (PlayerPrefs.HasKey("Level"))
        {
            _levelId = GameManager.Instance.CurrentLevel;
        }

        SelectMachine(0);


        currentLevel = SpawnLevel();
        frameParent.transform.SetParent(currentLevel.transform);
        frameParent.transform.localPosition = Vector3.zero;
        flipMechanics.EnableCloth();

        if (GameController.Instance.colorFollower)
        {
            colorFollower.pathContainer = currentLevel.pathContainer;
            colorFollower.stichesContainer = currentLevel.stichesContainer;
        }

        uiController.gamePanel.SetLevel(_levelId);
    }
    
    private void Start()
    {
        randomPos = Vector2.zero;
        
        if (FacebookScript.Instance.isInitialized())
        {
            FacebookScript.Instance.LogLevelStartEvent(_levelId + 1);
        }
        currentAreaID = 0;

        isGameFinished = false;
        //Camera.main.fieldOfView = DataCarrier.Instance.levelsData[GameManager.Instance.CurrentLevel].cameraZoomValue;
    }
    
    public void SelectMachine(int index)
    {
        for (int i = 0; i < sewingMachineTypes.Length; i++)
        {
            sewingMachineTypes[i].SetActive(false);
        }
        sewingMachineTypes[index].SetActive(true);
        _machineScript = sewingMachineTypes[index].GetComponent<SewingMachineScript>();
        selectedMachineIndex = index;
        ChangeSewingMachineThreadColor();
    }

    public void FinishGameComplete()
    {
        FinishGame(correctProgress);
    }

    public void FinishGame(float correctProgress)
    {
        _finishPercentage = (int)((correctProgress / totalWaypoints) * 100);
        if(_finishPercentage >= 98)
        {
            _finishPercentage = 100;
        }

        isGameFinished = true;
        Invoke("DisableSewing",0.25f);
        StartCoroutine(PostFinishGame(_finishPercentage));
    }

    public void CloseGamePanel()
    {
        uiController.gamePanel.HideGameplayPanels();
    }

    public void MoveSewingMachine(Transform tttt)
    {
        tttt.DOMove(sewingFinalPos.position, 0.5f);
    }

    public void DisableSewing()
    {
        if (GameController.Instance.colorFollower)
        {
            colorFollower.enabled = false;
        }
    }

    public bool isGameFinished = false;
    
    public IEnumerator PostFinishGame(int finishPercentage)
    {
        yield return new WaitForSeconds(1f);
        uiController.finishPanelComplete.Initialize(finishPercentage);
    }

    public Level SpawnLevel()
    {
        if (GameManager.Instance.CurrentLevel > 0)
        {
            uiController.gamePanel.ShowLevelReference(GameManager.Instance.CurrentLevel);
            return Instantiate(DataCarrier.Instance.levelsData[GameManager.Instance.CurrentLevel].lvlPrefab, Vector3.zero, Quaternion.identity) as Level;
        }
        else
        {
            uiController.gamePanel.ShowLevelReference(_levelId);
            return Instantiate(DataCarrier.Instance.levelsData[_levelId].lvlPrefab, Vector3.zero, Quaternion.identity) as Level;
        }
    }

    public void HandleFinishGameComplete()
    {
        LogFacebook();
        LogLevel();
    }

    public void LogFacebook()
    {
        FacebookScript.Instance.LogLevelCompleteEvent(_levelId + 1);
    }

    public static int levelCounter;

    public void LogLevel()
    {
        _levelId++;
        if (_levelId == DataCarrier.Instance.levelsData.Count)
        {
            DataCarrier.Instance.AddNewLevel();
            if (_levelId > PlayerPrefs.GetInt("Level"))
            {
                PlayerPrefs.SetInt("Level", _levelId);
            }
            GameManager.Instance.CurrentLevel = _levelId;
        }
        else
        {
            if (_levelId > PlayerPrefs.GetInt("Level"))
            {
                PlayerPrefs.SetInt("Level", _levelId);
            }
            GameManager.Instance.CurrentLevel = _levelId;
        }

        if (_levelId == 1)
        {
            PlayerPrefs.SetInt("TutorialFinished",1);
        }

        levelCounter++;
        if (levelCounter == 6)
        {
            levelCounter = 0;
            SceneManager.LoadScene(0);
        }
        else
        {
            ReloadScene();
        }

        
            if (PlayerPrefs.GetInt("LevelCleared" + PlayerPrefs.GetInt("Level").ToString()) != 1)
            {
                PlayerPrefs.SetInt("LevelCounter", PlayerPrefs.GetInt("LevelCounter") + 1);
                PlayerPrefs.SetInt("LevelCleared" + PlayerPrefs.GetInt("Level").ToString(), 1);
            }

            if (PlayerPrefs.GetInt("LevelCounter") == 5)
            {
                PlayerPrefs.SetInt("LevelCounter", 0);
                if (PlayerPrefs.GetInt("SelectedClothNumber") == flipMechanics.clothDetails.Length - 1)
                {
                    PlayerPrefs.SetInt("SelectedClothNumber", 0);
                }
                else
                {
                    PlayerPrefs.SetInt("SelectedClothNumber", PlayerPrefs.GetInt("SelectedClothNumber") + 1);
                }
            }
    }

    public void HandleFinishGameFailed()
    {
        FacebookScript.Instance.LogFailLevelEvent(_levelId + 1);
        ReloadScene();
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void SetProgress(float progress, int totalProgress)
    {
        uiController.gamePanel.SetProgress(progress / totalProgress);
    }

    public void AddExtraColors(int number)
    {
        uiController.gamePanel.colorButtons[number].SetActive(true);
    }
    
    public void ChangeSewingMachineThreadColor()
    {
        for (int i = 0; i < stichesModels.Length; i++)
        {
            stichesModels[i].sharedMaterial = stichMaterials[selectedColorIndex];
        }
        
        for (int i = 0; i < extraIrregularStitchModel.Length; i++)
        {
            extraIrregularStitchModel[i].sharedMaterial = stichMaterials[selectedColorIndex];
        }

        extraStitchModel.sharedMaterial = stichMaterials[selectedColorIndex];


        for (int j = 0; j < sewingMachineTypes.Length; j++)
        {
            var machine = sewingMachineTypes[j].GetComponent<SewingMachineScript>();
            machine.SetMaterial(stichMaterials[selectedColorIndex]);
            //var threadSkin = machineThreads[j].GetComponent<SkinnedMeshRenderer>();
            //threadSkin.sharedMaterial = stichMaterials[selectedColorIndex];
            //machineThreads[j].RopeMaterial = stichMaterials[selectedColorIndex];
        }
    }

    public void ChangeLevel(string index)
    {
        int lvlIndex = Int32.Parse(index);
        lvlIndex = lvlIndex % (DataCarrier.Instance.levelsData.Count);
        PlayerPrefs.SetInt("Level", lvlIndex );
        GameManager.Instance.CurrentLevel = lvlIndex;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ShowReferenceHand(Vector2 values)
    {
        referenceHandSprite.transform.position = new Vector3(values.x, 1, values.y);
    }

    public void RemoveReferenceHand()
    {
        referenceHandSprite.transform.position = new Vector3(100, 1, 100);
    }

    [Button]
    public void PlayInteracticeParticle()
    {
        int randomValue = Random.Range(0, interactiveParticles.Length);
        interactiveParticles[randomValue].Play();
    }

    public void SpawnParticleAtRandomPos(int randomValue , ParticleSystem particle , bool otherScriptCheck)
    {
        for (int i = 0; i < Random.Range(5, 8); i++)
        {
            Vector2 pos = GetRandomPos();
            StartCoroutine(PlayParticleWithDelay(randomValue,particle,otherScriptCheck));
        }
    }

    Vector2 GetRandomPos()
    {
        Vector2 rnPos = new Vector2(Random.Range(minPos.x, maxPos.x), Random.Range(minPos.y, maxPos.y));
        float   sqr   = Vector3.SqrMagnitude(rnPos - randomPos);
        if (sqr > 0.5f)
        {
            randomPos = rnPos;
            
        }
        else
        {
            GetRandomPos();
        }
        
        return randomPos;
        
    }

    IEnumerator PlayParticleWithDelay(int value , ParticleSystem particle , bool fromOtherScript)
    {
        if (fromOtherScript)
        {
            yield return new WaitForSeconds(Random.Range(0.5f, 1f));
            float localY = particle.transform.position.y - 0.5f;
            particle.transform.position = new Vector3(Random.Range(minPos.x, maxPos.x), localY,
                Random.Range(minPos.y, maxPos.y));
            particle.Emit(1);
        }
        else
        {
            yield return new WaitForSeconds(Random.Range(0.5f, 1f));
            float localY = interactiveParticles[value].transform.position.y;
            interactiveParticles[value].transform.localPosition = new Vector3(Random.Range(minPos.x, maxPos.x), localY,
                Random.Range(minPos.y, maxPos.y));
            interactiveParticles[value].Emit(1);
        }
    }
}
