﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterClothes : MonoBehaviour
{
    public GameObject skirt;
    public GameObject[] clothesVariations;
    public Animation characterAnim;

    public void SetClothes(int clothIndex)
    {
        if (clothIndex > 19)
        {
            skirt.SetActive(true);
        }
        clothesVariations[clothIndex].SetActive(true);
    }

    public void PlayAnimation(string animationName)
    {
        characterAnim.Play(animationName);
    }
}
