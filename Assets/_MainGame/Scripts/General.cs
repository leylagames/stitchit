﻿using System;
using System.Collections;
using DG.Tweening;
using GameAnalyticsSDK;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class General : MonoBehaviour
{
    [Header("UI Controls")] 
    public TextMeshProUGUI playerCashLabel;
    public Image progressFillBar;
    
    [Header("Other Controls")] 
    public GameObject[] toolsImages;
    public int[] prices;
    public int[] toolsCategory;

    public DOTweenAnimation newToolPanel;
    public AudioSource newToolSound;
    
    public static int clothIndex;

    private int earnedCash;
    private int _selfieIndex;
    private bool _goToShop;
    
    public IEnumerator ShowBar(float amount , float duration)
    {
        float counter = 0;
        float startValue = 0;
        float endValue = startValue + amount;
        float newValue = startValue;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            newValue = Mathf.Lerp(startValue, endValue, counter / duration);
            progressFillBar.fillAmount = newValue;
            yield return null;
        }
    }

    public IEnumerator AddCash(int amount , float duration)
    {
        float counter = 0;
        float startValue = GameManager.Instance.GetPlayerCash();
        float endValue = startValue + amount;
        float newValue = startValue;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            newValue = Mathf.Lerp(startValue, endValue, counter / duration);
            playerCashLabel.text = Mathf.FloorToInt(newValue).ToString();
            yield return null;
        }
        
        GameManager.Instance.SetPlayerCash(Mathf.RoundToInt(endValue));
    }

    private IEnumerator ShowIncrement(TextMeshProUGUI textbox, int value, float duration)
    {
        float counter = 0;
        float startValue = 0;
        float endValue = (float)value;
        float newValue = startValue;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            newValue = Mathf.Lerp(startValue, endValue, counter / duration);
            textbox.text = Mathf.FloorToInt(newValue).ToString();
            yield return null;
        }
    }
    
    public void CheckNewTool()
    {
        var currency = GameManager.Instance.GetPlayerCash();
        
        for (int i = 0; i < toolsImages.Length; i++)
        {
            if (currency >= prices[i])
            {
                if (!PlayerPrefs.HasKey("Tools" + i))
                {
                    newToolSound.Play();
                    newToolPanel.DORestartById("ScaleIn");
                    toolsImages[i].SetActive(true);
                    PlayerPrefs.SetInt("InventoryCategory",toolsCategory[i]);
                    PlayerPrefs.SetInt("Tools" + i , 1);
                    break;
                    return;
                }
            }
        }
    }
}
