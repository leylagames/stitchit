﻿using UnityEngine;
using Random = UnityEngine.Random;

public class FrameSelector : MonoBehaviour
{
    public GameObject[] frames;

    private void Awake()
    {
        frames[PlayerPrefs.GetInt("FrameTypeSelected")].SetActive(true);
        Invoke("MakeChild",2f);
    }

    private void MakeChild()
    {
        if (GameController.Instance.colorFollower)
        {
            if (!GameController.Instance.colorFollower.useFlippingMechanics)
            {
                transform.parent = GameController.Instance.colorFollower.stichesContainer.transform.parent;
            }
        }


    }
}
