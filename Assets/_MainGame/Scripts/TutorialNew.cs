﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using UnityEngine;

public class TutorialNew : MonoBehaviour
{
    public GameObject tapAndHoldTutorial;
    public GameObject tutorialStitches, tutorialStitchesPart01_3Button, tutorialStitchesPart01_4Button, tutorialStitchesPart02;
    public GameObject tutorialMachines, tutorialMachinesPart01_3Button, tutorialMachinesPart01_4Button, tutorialMachinesPart02;
    public GameObject tutorialShavers, tutorialShaversIntro, tutorialShaversPart02;
    //public GameObject tutorialClothes, tutorialClothesIntro, tutorialClothesPart02;

    public int tutorialStitchesLevel;
    public int tutorialMachinesLevel;
    public int tutorialShaversLevel;
    public int tutorialClothesLevel;
    

    public void Start()
    {
        if (!PlayerPrefs.HasKey("Tutorial"))
        {
            ShowTutorial();
        }

        if (GameManager.Instance.CurrentLevel == tutorialStitchesLevel && !PlayerPrefs.HasKey("TutorialStitches"))
        {
            StartCoroutine(ShowTutorialStitches());
        }
        
        if (GameManager.Instance.CurrentLevel == tutorialMachinesLevel && !PlayerPrefs.HasKey("TutorialMachines"))
        {
            StartCoroutine(ShowTutorialMachines());
        }

    }

    public void ShowTutorial()
    {
        tapAndHoldTutorial.SetActive(true);
    }

    public void HideTutorial()
    {
        tapAndHoldTutorial.SetActive(false);
    }

    public IEnumerator ShowTutorialStitches()
    {
        yield return new WaitForSeconds(0.5f);
        tutorialStitches.SetActive(true);
        tutorialStitchesPart01_3Button.SetActive(true);
    }

    public void CompleteTutorial02Part01()
    {
        tutorialStitchesPart01_4Button.SetActive(false);
        tutorialStitchesPart01_3Button.SetActive(false);
        tutorialStitchesPart02.SetActive(true);
    }

    public void CompleteTutorial02Part02()
    {
        tutorialStitches.SetActive(false);
        PlayerPrefs.SetInt("TutorialStitches",1);
    }
    
    public IEnumerator ShowTutorialMachines()
    {
        yield return new WaitForSeconds(0.5f);
        tutorialMachines.SetActive(true);
        tutorialMachinesPart01_3Button.SetActive(true);
    }

    public void CompleteTutorial03Part01()
    {
        tutorialMachinesPart01_4Button.SetActive(false);
        tutorialMachinesPart01_3Button.SetActive(false);
        tutorialMachinesPart02.SetActive(true);
    }

    public void CompleteTutorial03Part02()
    {
        tutorialMachines.SetActive(false);
        PlayerPrefs.SetInt("TutorialMachines",1);
    }
    
    public IEnumerator ShowTutorialShavers()
    {
        yield return new WaitForSeconds(0.1f);
        // tutorialShavers.SetActive(true);
        // tutorialShaversPart02.SetActive(true);
    }
    
    public void CompleteTutorialShavers()
    {
        // tutorialShavers.SetActive(false);
        // PlayerPrefs.SetInt("TutorialShavers",1);
    }
    
    // public IEnumerator ShowTutorialClothes()
    // {
    //     yield return new WaitForSeconds(0.5f);
    //     tutorialClothes.SetActive(true);
    //     tutorialClothesPart02.SetActive(true);
    // }
    //
    // public void CompleteTutorialClothes()
    // {
    //     tutorialClothes.SetActive(false);
    //     PlayerPrefs.SetInt("TutorialClothes",1);
    // }
    
    public IEnumerator ShowTutorialShaversIntro()
    {
        yield return new WaitForSeconds(0.1f);
        // tutorialShavers.SetActive(true);
        // tutorialShaversIntro.SetActive(true);
    }
    
    public void CompleteTutorialShaversIntro()
    {
        // tutorialShavers.SetActive(false);
        // PlayerPrefs.SetInt("TutorialShaversIntro",1);
    }
    
    // public IEnumerator ShowTutorialClothesIntro()
    // {
    //     yield return new WaitForSeconds(0.5f);
    //     tutorialClothes.SetActive(true);
    //     tutorialClothesIntro.SetActive(true);
    // }
    //
    // public void CompleteTutorialClothesIntro()
    // {
    //     tutorialClothes.SetActive(false);
    //     PlayerPrefs.SetInt("TutorialClothesIntro",1);
    // }
}
