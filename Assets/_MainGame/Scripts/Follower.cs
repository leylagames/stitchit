﻿using System;
using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MoreMountains.NiceVibrations;
using Sirenix.OdinInspector;
using UnityEngine.UI;


public class Follower : MonoBehaviour
{

	[Serializable]
	public class ActionData
	{

		public int              staringIndex;
		public int              endingIndex;
		public int              currentProgress;
		public int              correctProgress;
		public int              actionAreaIndex;
		public List<GameObject> _stitches   = new List<GameObject>();
		public List<Waypoint>   _donePoints = new List<Waypoint>();

	}

	public float movementSpeed;

	public Transform pathContainer;

	public float lengthBetweenStitches;

	public Transform    stichesContainer;
	public SmoothFollow smoothFollowCam;
	public Transform    cameraPosForCutting;
	public Transform    cameraPositionEnd;
	public AudioSource  sewingFinished;

	public bool StartAtFirstPointOnAwake;

	[HideInInspector] public bool move;
	[HideInInspector] public bool finished;

	[SerializeField] private Waypoint[] _points;


	private List<PointType> stageColors   = new List<PointType>();
	private List<int>       _uniqueColors = new List<int>();
	private List<int>       pointInts     = new List<int>();

	[SerializeField] private List<GameObject> _localStitches   = new List<GameObject>();
	[SerializeField] private List<Waypoint>   _localDonePoints = new List<Waypoint>();
	[SerializeField] private List<ActionData> _actions         = new List<ActionData>();

	public Area[] _areas;

	private  bool  _draw;
	private  bool  _goingNext;
	private  int   _currentTargetIdx;
	public   int   currentColorId;
	internal int   _currentAreaId;
	private  int   _currentStage;
	private  bool  _willActivate = true;
	public   bool  doneStitching;
	private  int   _tempCurrentProgress;
	private  int   _tempCorrectProgress;
	private  int   _previousClosestWaypoint;
	private  float _movementSpeedOriginal;

	[SerializeField] private Transform   stitchesParent;
	[SerializeField] private int         currentProgress;
	[SerializeField] private int         correctProgress;
	private                  AudioSource _machineSound;

	public int CorrectProgress
	{
		get => correctProgress;
	}

	public float completionPercentage;


	#region Mansoor's Variables

	[System.Serializable]
	public class AreaStatus
	{

		public int       areaID;
		public int       pointsFilled;
		public int       totalPoints;
		public float     percentageDone;
		public bool      isFilledCompletely;
		public PointType actualColor = PointType.None;

	}

	[BoxGroup("Auto Fill Properties", true, true), SerializeField] bool autoBtn_isActive = false;

	[BoxGroup("Auto Fill Properties", true, true), SerializeField] float combinedPercentage = 0f;

	[BoxGroup("Auto Fill Properties", true, true), SerializeField] float autofillThresshold = 0.7f;

	[BoxGroup("Auto Fill Properties", true, true)] public GameObject moveButton;

	[BoxGroup("Auto Fill Properties", true, true)] public Button autoFillButton;

	[BoxGroup("Auto Fill Properties", true, true), SerializeField] public List<AreaStatus> areaStatus;

	#endregion


	private bool   onceCheck;
	const   string VibrationKey = "VibrationToggle";

	private Vector3 previousPosition = Vector3.zero;
	public  float   idleTime;
	public  float   inactivityTime;
	private float   _inactivityTimer;
	public  bool    startInactivityCheck;

	private float _idleTimer;
	private bool  _handShown;
	private float _toastTimer;

	[HideInInspector] public bool autofillStarted;

	private bool _actionDataSet;
	private int  _actionIndex;
	private int  _previousActionIndex = 0;

	public void SetActionData(int areaID)
	{
		if (_actionDataSet)
		{
			ActionData action = new ActionData();
			action.staringIndex    = _previousActionIndex;
			action.endingIndex     = _actionIndex;
			action.currentProgress = currentProgress;
			action.correctProgress = correctProgress;
			action.actionAreaIndex = areaID;
			if (_localStitches.Count != 0)
			{
				for (int i = 0; i < _localStitches.Count; i++)
				{
					action._stitches.Add(_localStitches[i]);
				}

				for (int i = 0; i < _localDonePoints.Count; i++)
				{
					action._donePoints.Add(_localDonePoints[i]);
				}

				_localStitches.Clear();
				_localDonePoints.Clear();
				_actions.Add(action);
				_previousActionIndex = _actionIndex;
				_actionDataSet       = false;
			}
		}
	}

	public void UndoAction()
	{
		if (_actions.Count != 0)
		{
			ActionData actionToUndo = _actions[_actions.Count - 1];
			areaStatus[actionToUndo.actionAreaIndex].isFilledCompletely           = false;
			_areas[actionToUndo.actionAreaIndex].GetComponent<Collider>().enabled = true;
			int count = actionToUndo._donePoints.Count;
			for (int i = 0; i < count; i++)
			{
				actionToUndo._donePoints[0].isDrawn = false;
				actionToUndo._donePoints.RemoveAt(0);
				areaStatus[actionToUndo.actionAreaIndex].pointsFilled--;
				if (areaStatus[actionToUndo.actionAreaIndex].pointsFilled < 0)
				{
					areaStatus[actionToUndo.actionAreaIndex].pointsFilled = 0;
				}

				if (_currentTargetIdx > 0)
				{
					_currentTargetIdx--;
				}
			}

			int count2 = actionToUndo._stitches.Count;
			for (int i = 0; i < count2; i++)
			{
				var stitch = actionToUndo._stitches[0];
				actionToUndo._stitches.RemoveAt(0);
				Destroy(stitch.gameObject);
			}

			_actions.RemoveAt(_actions.Count - 1);
			if (_actions.Count != 0)
			{
				_previousActionIndex = _actions[_actions.Count - 1].endingIndex;
				_actionIndex         = _previousActionIndex;
				currentProgress      = _actions[_actions.Count - 1].currentProgress;
				correctProgress      = _actions[_actions.Count - 1].correctProgress;
			}
			else
			{
				_previousActionIndex = 0;
				_actionIndex         = 0;
				currentProgress      = 0;
				correctProgress      = 0;
			}
			
			if (_actions.Count == 0)
			{
				GameController.Instance.uiController.gamePanel.undoButton.SetActive(false);
			}
		}
	}

	private void Start()
	{
		if (GameManager.Instance.CurrentLevel < 2)
		{
			movementSpeed = movementSpeed * 1.75f;
		}

		_movementSpeedOriginal                     = movementSpeed;
		_machineSound                              = GetComponent<AudioSource>();
		_points                                    = pathContainer.GetComponentsInChildren<Waypoint>();
		GameController.Instance.selectedColorIndex = (int) _points[0].colorIndex;
		GameController.Instance.ChangeSewingMachineThreadColor();
		if (PlayerPrefs.HasKey("TutorialFinished"))
		{
			GameController.Instance.uiController.gamePanel.SetSelectionPointersColor();
			GameController.Instance.uiController.gamePanel.SetSelectionPointersStitches();
		}

		currentColorId = (int) _points[0].colorIndex;
		for (int i = 0; i < _points.Length; i++)
		{
			if (_points[i].colorIndex != PointType.None)
			{
				stageColors.Add(_points[i].colorIndex);
				if (!CheckListForColor((int) _points[i].colorIndex))
				{
					_uniqueColors.Add((int) _points[i].colorIndex);
				}

				GameController.Instance.uiController.gamePanel.colorButtons[(int) _points[i].colorIndex].SetActive(true);
			}


			#region Mansoor's Edit For Automation

			SetAreaStatus(i);

			#endregion
		}

		autoFillButton.onClick.AddListener(() =>
		{
			GameController.Instance.isGameFinished = true;
			autofillStarted = true;
			if (moveButton)
			{
				moveButton.SetActive(false);
			}

			autoFillButton.gameObject.SetActive(false);
			//GameController.Instance.machineMover.enabled = false;
			if (!autoBtn_isActive)
			{
				GameController.Instance.selectedColorIndex = (int) areaStatus[_currentAreaId].actualColor;
			}

			if (areaStatus[_currentAreaId].isFilledCompletely)
			{
				for (int i = 0; i < areaStatus.Count; i++)
				{
					if (!areaStatus[i].isFilledCompletely)
					{
						StartCoroutine(WaitForUselessThing(i));
						continue;
					}
				}
			}

			autoBtn_isActive = true;
			move             = true;
			ResetIdleTimer();
		});
		RefreshUI();
		_draw = true;
		if (StartAtFirstPointOnAwake)
		{
			transform.position = _points[0].transform.position;
			currentColorId     = (int) _points[0].colorIndex;
		}

		_areas = FindObjectsOfType<Area>();
		_areas = _areas.OrderBy(x => x.areaId).ToArray();

		// if (GameManager.Instance.CurrentLevel != 0)
		// {
		// 	StartCoroutine(ShowHand());
		// }
	}

	private bool CheckListForColor(int colorId)
	{
		for (int i = 0; i < _uniqueColors.Count; i++)
		{
			if (_uniqueColors[i] == colorId)
			{
				return true;
			}
		}

		return false;
	}

	private void RefreshUI()
	{
		GameController.Instance.uiController.gamePanel.RefreshUI(_uniqueColors.Count);
	}

	private void Update()
	{
		if (_points == null || _points.Length == 0) return;
		if (!areaStatus[_currentAreaId].isFilledCompletely)
		{
			if (_draw && move && !_goingNext)
			{
				timeCheck += Time.deltaTime;
				if (timeCheck >= 0.15f)
				{
					if (PlayerPrefs.GetInt(VibrationKey) == 1)
					{
						timeCheck = 0;
						MMVibrationManager.Haptic(HapticTypes.SoftImpact, false, true, this);
					}
				}
			}

			var distance = Vector3.SqrMagnitude(transform.position - _points[_currentTargetIdx].transform.position);
			if (distance == 0f)
			{
				if (currentColorId == GameController.Instance.selectedColorIndex)
				{
					correctProgress += 1;
				}


				#region Mansoor's Edit For Automation

				int areaIndex = 0;
				for (int i = 0; i < areaStatus.Count; i++)
				{
					if (_points[_currentTargetIdx].areaId == areaStatus[i].areaID)
					{
						areaIndex = i;
						continue;
					}
				}

				FillAreaStatistics(areaIndex);

				#endregion


				if (_points == null)
				{
					return;
				}

				if (!_localDonePoints.Contains(_points[_currentTargetIdx]))
				{
					_localDonePoints.Add(_points[_currentTargetIdx]);
				}

				currentProgress                    += 1;
				_points[_currentTargetIdx].isDrawn =  true;
				_draw                              =  !_points[_currentTargetIdx].stopDrawing;
				if (!_draw)
				{
					if (!SetFirst())
					{
						//GameController.Instance.machineMover.ActivateMover();
						return;
					}
				}

				bool wasGoinNext = _goingNext;
				_goingNext = !_draw;
				if (!_goingNext && wasGoinNext)
				{
					_goingNext = true;
					StartCoroutine(FinishGoingNext());
				}

				_currentTargetIdx++;
				if (_currentTargetIdx >= _points.Length || _points[_currentTargetIdx].isDrawn)
				{
					if (_willActivate && _currentTargetIdx < _points.Length)
					{
						_willActivate = false;
					}
					else
					{
						//GameController.Instance.machineMover.ActivateMover();
						_willActivate = true;
						return;
					}
				}

				if (_previousClosestWaypoint == _currentTargetIdx - 1)
				{
					if (_previousClosestWaypoint >= 0)
					{
						_points[_previousClosestWaypoint].stopDrawing = true;
						_points[_previousClosestWaypoint].isDrawn     = false;
						pointInts.Add(_previousClosestWaypoint);
					}
				}

				if (_goingNext)
				{
					movementSpeed = _movementSpeedOriginal * 5;
				}

				if (wasGoinNext)
				{
					movementSpeed = _movementSpeedOriginal;
				}
			}

			Quaternion lookOnLook = Quaternion.LookRotation(_points[_currentTargetIdx].transform.position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, lookOnLook, Time.deltaTime * 15);
			transform.rotation = Quaternion.Euler(0,transform.eulerAngles.y, 0);
			CheckAllWaypointsDone();
			GameController.Instance._machineScript.SetAnimation(move);

			// if (startInactivityCheck)
			// {
			// 	if (!move)
			// 	{
			// 		_inactivityTimer += Time.deltaTime;
			// 		if (_inactivityTimer >= 2f)
			// 		{
			// 			if (!showToastOnce)
			// 			{
			// 				showToastOnce = true;
			// 				if (GameManager.Instance.CurrentLevel != 0)
			// 				{
			// 					GameController.Instance.uiController.gamePanel.ShowToastMessage("Tap and hold the area to stitch!");
			// 					GameController.Instance.uiController.gamePanel.helpingArrow.SetActive(true);
			// 				}
			// 			}
			// 		}
			//
			// 		if ((_inactivityTimer >= inactivityTime) && !autoFillButton.gameObject.activeInHierarchy)
			// 		{
			// 			autoFillButton.gameObject.SetActive(true);
			// 		}
			// 	}
			// 	else
			// 	{
			// 		_inactivityTimer = 0;
			// 	}
			// }
		}
		else
		{
			//GameController.Instance.machineMover.enabled = true;
			// _idleTimer += Time.deltaTime;
			// if (_idleTimer >= idleTime)
			// {
			// 	if (!_handShown)
			// 	{
			// 		ShowHelperHand();
			// 		GameController.Instance.uiController.gamePanel.ShowToastMessage("Tap the area to select it!");
			// 		_handShown = true;
			// 	}
			// }
		}

		if (showToastOnce)
		{
			_toastTimer += Time.deltaTime;
			if (_toastTimer >= 5)
			{
				_toastTimer   = 0;
				showToastOnce = false;
			}
		}
		
	}

	private float _dragTimer;
	private float _handleMoveTimer;

	public void ResetIdleTimer()
	{
		_idleTimer = 0;
		_handShown = false;
		GameController.Instance.RemoveReferenceHand();
		StopCoroutine(ShowHand());
	}

	public IEnumerator ShowHand()
	{
		yield return new WaitForSeconds(idleTime);
		if (!startInactivityCheck)
		{
			ShowHelperHand();
		}
	}

	void ShowHelperHand()
	{
		bool allAreasCompleted = CheckAllAreaCompletion();
		if (!allAreasCompleted)
		{
			for (int i = 0; i < _areas.Length; i++)
			{
				if (!areaStatus[_areas[i].areaId].isFilledCompletely)
				{
					// GameController.Instance.ShowReferenceHand(DataCarrier.Instance.levelsData[GameManager.Instance.CurrentLevel].values[i]);
					return;
				}
			}
		}
	}

	private float timeCheck;

	private void FixedUpdate()
	{
		float sqrDistance = Vector3.SqrMagnitude(previousPosition - transform.position);
		if (sqrDistance >= lengthBetweenStitches)
		{
			if (_draw && move && !_goingNext)
			{
				var currentStich = Instantiate(GameController.Instance.stichesModels[GameController.Instance.selectedStichIndex], transform.position, Quaternion.Euler(0, 0, 0));
				_actionDataSet = true;
				_actionIndex++;
				_localStitches.Add(currentStich.gameObject);
				currentStich.transform.SetParent(stichesContainer.transform.GetChild(GameController.Instance.currentAreaID).transform, true);
				previousPosition = transform.position;
				if (PlayerPrefs.GetInt(VibrationKey) == 1)
				{
					if (!_machineSound.isPlaying)
					{
						_machineSound.Play();
					}
				}
			}
		}

		if (((move && !_goingNext) || !_draw) && !doneStitching)
		{
			float plusValue = movementSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, _points[_currentTargetIdx].transform.position, plusValue);
			GameController.Instance.SetProgress(currentProgress, _points.Length);
		}
	}

	[SerializeField] Transform nextTarget;

	private IEnumerator Finish(float completionPercentage)
	{
		if (!finished)
		{
			GameController.Instance.isGameFinished = true;
			autoFillButton.gameObject.SetActive(false);
			doneStitching = true;
			sewingFinished.Play();
			GameController.Instance.MoveSewingMachine(transform);
			smoothFollowCam.enabled = false;
			smoothFollowCam.transform.DOMove(cameraPosForCutting.position, 1f);
			smoothFollowCam.transform.DORotate(cameraPosForCutting.transform.eulerAngles, 1f);
			finished = true;
			GameController.Instance.CloseGamePanel();
			StartCoroutine(FillRoutineCamera(55f, 2f));
			yield return new WaitForSeconds(2f);
			GameController.Instance.FinishGame(correctProgress);
		}
	}

	public IEnumerator FillRoutineCamera(float end, float duration)
	{
		float counter    = 0;
		float startValue = Camera.main.fieldOfView;
		float endValue   = end;
		float newValue   = startValue;
		while (counter < duration)
		{
			counter                 += Time.deltaTime;
			newValue                =  Mathf.Lerp(startValue, endValue, counter / duration);
			Camera.main.fieldOfView =  newValue;
			yield return null;
		}
	}

	public void FinishGame()
	{
		//GameController.Instance.tutorial.RemoveTutorials();
		int totalWaypoints = _points.Length;
		_points                                 = null;
		GameController.Instance.correctProgress = correctProgress;
		GameController.Instance.totalWaypoints  = totalWaypoints;
		completionPercentage                    = (((float) correctProgress / (float) currentProgress)) * 100;
		StartCoroutine(Finish(completionPercentage));
	}

	public bool SetFirst()
	{
		for (int i = 0; i < _points.Length; i++)
		{
			if (!_points[i].isDrawn && _points[i].areaId == _currentAreaId)
			{
				_currentTargetIdx = i - 1;
				return true;
			}
		}

		return false;
	}

	public bool SetClosest()
	{
		float dist    = 0;
		float minDist = Mathf.Infinity;
		if (IsAreaFilled(_currentAreaId))
		{
			return false;
		}
		else
		{
			for (int i = 0; i < _points.Length; i++)
			{
				if (_points[i].areaId == _currentAreaId && !_points[i].isDrawn)
				{
					dist = Vector3.Distance(transform.position, _points[i].transform.position);
					if (dist < minDist)
					{
						minDist                  = dist;
						_currentTargetIdx        = i - 1;
						_previousClosestWaypoint = i - 1;
					}
				}
			}

			return true;
		}
	}

	public Waypoint GetClosestWaypoint(int areaId)
	{
		float dist    = 0;
		float minDist = Mathf.Infinity;
		_currentAreaId = areaId;
		Waypoint closestWaypoint = null;
		if (IsAreaFilled(areaId))
		{
			return null;
		}

		for (int i = 0; i < _points.Length; i++)
		{
			if (_points[i].areaId == areaId)
			{
				dist = Vector3.Distance(transform.position, _points[i].transform.position);
				if (dist < minDist && !_points[i].isDrawn)
				{
					closestWaypoint          = _points[i];
					minDist                  = dist;
					_draw                    = false;
					_currentTargetIdx        = i;
					_previousClosestWaypoint = i;
				}
			}
		}

		currentColorId = (int) stageColors[areaId];
		return closestWaypoint;
	}

	public Waypoint GetFirstWaypoint(int areaId)
	{
		Waypoint closestWaypoint = null;
		for (int i = 0; i < _points.Length; i++)
		{
			if (_points[i].areaId == areaId)
			{
				closestWaypoint   = _points[i];
				_currentTargetIdx = i;
				break;
			}
		}

		for (int j = 0; j < pointInts.Count; j++)
		{
			_points[pointInts[j]].stopDrawing = false;
		}

		return closestWaypoint;
	}

	public void ClearAreaPoints(int areaID)
	{
		_currentAreaId = areaID;
		for (int i = 0; i < _points.Length; i++)
		{
			if (_points[i].areaId == areaID)
			{
				_points[i].isDrawn = false;
			}
		}
	}
	

	public bool IsAreaFilled(int areaId)
	{
		for (int i = 0; i < _points.Length; i++)
		{
			if (_points[i].areaId == areaId && !_points[i].isDrawn)
			{
				return false;
			}
		}

		return true;
	}

	private bool showToastOnce;
	private bool _handleMove;

	public void HandleMove(bool start)
	{
		move = start;
		// if (GameController.Instance.machineMover.areaSelected)
		// {
		// 	GameController.Instance.uiController.gamePanel.undoButton.SetActive(!start);
		// 	GameController.Instance.uiController.gamePanel.levelProgressbar.SetActive(start);
		// 	// GameController.Instance.uiController.gamePanel.toolsPanel.SetActive(!start);
		// }
		// else
		{
			// if (!showToastOnce)
			// {
			// 	GameController.Instance.uiController.gamePanel.ShowToastMessage("Please select an area first !");
			// 	showToastOnce = true;
			// }
		}
	}

	private void _ShowAndroidToastMessage(string message)
	{
		AndroidJavaClass  unityPlayer   = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		if (unityActivity != null)
		{
			AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
			unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
			{
				AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
				toastObject.Call("show");
			}));
		}
	}

	public IEnumerator FinishGoingNext()
	{
		yield return new WaitForSeconds(.3f);
		_goingNext = false;
	}

	private bool once;

	public void CheckAllWaypointsDone()
	{
		for (int i = 0; i < _points.Length; i++)
		{
			if (!_points[i].isDrawn)
			{
				return;
			}
		}

		if (!once)
		{
			FinishGame();
			GameController.Instance.uiController.gamePanel.undoButton.SetActive(false);
			once = true;
		}
	}

	public void MakeStopper()
	{
		if (!IsAreaFilled(_currentAreaId))
		{
			_points[_currentTargetIdx].isDrawn = false;
			//_points[_currentTargetIdx].stopDrawing = true;
		}
	}


	#region Mansoor's Edit For Automation

	void SetAreaStatus(int index)
	{
		// for very first entry
		if (areaStatus.Count == 0)
		{
			AreaStatus status = new AreaStatus();
			status.areaID      = _points[index].areaId;
			status.actualColor = _points[index].colorIndex;
			status.totalPoints++;
			areaStatus.Add(status);
		}
		else
		{
			// Finding current point in areaStatus
			bool existingFound = false;
			for (int j = 0; j < areaStatus.Count; j++)
			{
				if (areaStatus[j].areaID == _points[index].areaId)
				{
					existingFound = true;
					areaStatus[j].totalPoints++;
				}
			}

			// if not found, add new to the list :) 
			if (!existingFound)
			{
				AreaStatus status = new AreaStatus();
				status.areaID      = _points[index].areaId;
				status.actualColor = _points[index].colorIndex;
				status.totalPoints++;
				areaStatus.Add(status);
			}
		}
	}

	
	void FillAreaStatistics(int index)
	{
		if (areaStatus[index].isFilledCompletely) return;
		areaStatus[index].pointsFilled++;
		areaStatus[index].percentageDone = Mathf.Clamp01((float) areaStatus[index].pointsFilled / (float) areaStatus[index].totalPoints);
		areaStatus[index].actualColor    = (PointType) GameController.Instance.selectedColorIndex;
		if (areaStatus[index].percentageDone >= 1f)
		{
			areaStatus[index].isFilledCompletely           = true;
			_areas[index].GetComponent<Collider>().enabled = false;
		}

		bool allAreasCompleted = CheckAllAreaCompletion();
		if (allAreasCompleted)
		{
			move = false;
			FinishGame();
			GameController.Instance.uiController.gamePanel.undoButton.SetActive(false);
			once          = true;
			doneStitching = true;
		}
		else
		{
			if (!autoBtn_isActive)
			{
				combinedPercentage = (float) currentProgress / _points.Length;
				if (combinedPercentage >= autofillThresshold)
				{
					autoFillButton.gameObject.SetActive(true);
				}
			}
			else
			{
				if (areaStatus[index].isFilledCompletely)
				{
					for (int i = 0; i < areaStatus.Count; i++)
					{
						if (!areaStatus[i].isFilledCompletely)
						{
							StartCoroutine(WaitForUselessThing(i));
							break;
						}
					}
				}
			}
		}
	}

	IEnumerator WaitForUselessThing(int i)
	{
		yield return new WaitForSeconds(0.3f);
		GameController.Instance.selectedColorIndex = (int) areaStatus[i].actualColor;
		GameController.Instance.uiController.gamePanel.SelectColor((int) areaStatus[i].actualColor);
		currentColorId = (int) areaStatus[i].actualColor;
		_currentAreaId = GameController.Instance.currentAreaID = areaStatus[i].areaID;
		move           = true;
	}

	bool CheckAllAreaCompletion()
	{
		int counter = 0;
		for (int i = 0; i < areaStatus.Count; i++)
		{
			counter += areaStatus[i].isFilledCompletely ? 1 : 0;
		}

		return counter == areaStatus.Count;
	}

	void UndoAllAreaStats()
	{
		int index = 0;
		for (int i = 0; i < areaStatus.Count; i++)
		{
			if (GameController.Instance.currentAreaID == areaStatus[i].areaID && !areaStatus[i].isFilledCompletely)
			{
				autoFillButton.gameObject.SetActive(false);
				areaStatus[i].pointsFilled   = 0;
				areaStatus[i].percentageDone = 0f;
				continue;
			}
		}
	}

	#endregion

}