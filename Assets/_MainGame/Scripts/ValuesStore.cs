﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor.Experimental;
using UnityEngine;

public class ValuesStore : MonoBehaviour
{
    public int counter;
    public List<Vector2> values;
    public DataCarrier dataCarrier;
    
#if UNITY_EDITOR
    [Button]
    public void StoreCurrentValue()
    {
        // dataCarrier.levelsData[counter].values.Add(new Vector2(transform.position.x,transform.position.z));
    }
    
    [Button]
    public void NextLevel()
    {
        counter++;
    }

    [Button]
    public void ClearList()
    {
        // dataCarrier.levelsData[counter].values.RemoveRange(0,dataCarrier.levelsData[counter].values.Count);
    }

    [Button]
    public void CheckHands()
    {
        StartCoroutine(CheckHandsRoutine(counter));
    }

    public IEnumerator CheckHandsRoutine(int counter)
    {
        // for (int i = 0; i < dataCarrier.levelsData[counter].values.Count; i++)
        // {
        //     transform.position = new Vector3(dataCarrier.levelsData[counter].values[i].x, 1,
        //         dataCarrier.levelsData[counter].values[i].y);
        yield return new WaitForSeconds(1);
        // }
    }
    
    #endif
}
