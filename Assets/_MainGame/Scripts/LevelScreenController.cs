﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelScreenController : MonoBehaviour
{

    public Transform levelsParent;

    [Header("Level Buttons")]
    public List<Transform> LevelButtons = new List<Transform>();

    [Header("UI Panels")]
    public GameObject LoadingScreen;
    public Slider FillBar;

    AsyncOperation async = null;

    void Start()
    {
        LoadingScreen.SetActive(false);
        LevelsInit();
    }
    void LevelsInit()
    {
        for (int i = 0; i < levelsParent.childCount; i++)
        {
            LevelButtons.Add(levelsParent.GetChild(i));
        }
        //lock levels
        for (int i = 0; i < LevelButtons.Count; i++)
        {
            LevelButtons[i].Find("Complete").gameObject.SetActive(false);
        }
        //check levels to unlock
        for (int i = 0; i < LevelButtons.Count; i++)
        {
            if (i < GameManager.Instance.LevelsPlayed)
            {
                LevelButtons[i].GetComponent<Button>().interactable = true;
                LevelButtons[i].Find("Complete").gameObject.SetActive(true);
            }
            else if (i > GameManager.Instance.LevelsPlayed)
            {
                LevelButtons[i].GetComponent<Button>().interactable = false;
                LevelButtons[i].Find("Complete").gameObject.SetActive(false);
            }
        }
    }
    private void Update()
    {
        if (async != null)
        {
            FillBar.value = async.progress;
            if (async.progress >= 0.9f)
            {
                FillBar.value = 1.0f;
            }
        }
    }
    public void PlayLevel(int level)
    {
        GameManager.Instance.CurrentLevel = level;
        LoadingScreen.SetActive(true);
        StartCoroutine(LevelStart());
    }


    IEnumerator LevelStart()
    {
        async = SceneManager.LoadSceneAsync(1);
        LoadingScreen.SetActive(true);
        yield return async;
    }

}
