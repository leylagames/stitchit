﻿using UnityEngine;
using System.Collections;

public class GameManager 
{
    private static GameManager instance;
	private GameManager() { }
	public static GameManager Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new GameManager();
			}
			return instance;
		}
	}
	public int CurrentLevel = -1;

	public int playerCash;


	public int TotalCoins
	{
		set => PlayerPrefs.SetInt("Currency", value);
		get => PlayerPrefs.GetInt("Currency", 0);
	}

	public int LevelsPlayed
	{
		set => PlayerPrefs.SetInt("Level", value);
		get => PlayerPrefs.GetInt("Level", 0);
	}

	public int SoundToggle
	{
		set => PlayerPrefs.SetInt("SoundToggle", value);
		get => PlayerPrefs.GetInt("SoundToggle", 1);
	}

	public int MusicToggle
	{
		set => PlayerPrefs.SetInt("MusicToggle", value);
		get => PlayerPrefs.GetInt("MusicToggle", 1);
	}

	public int VibrationToggle
	{
		set => PlayerPrefs.SetInt("VibrationToggle", value);
		get => PlayerPrefs.GetInt("VibrationToggle", 1);
	}

	public void SetPlayerCash(int cash)
	{
		playerCash = cash;
		PlayerPrefs.SetInt("Currency",cash);
	}

	public int GetPlayerCash()
	{
		playerCash = PlayerPrefs.GetInt("Currency");
		return playerCash;
	}

}