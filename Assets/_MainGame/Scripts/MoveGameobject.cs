﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveGameobject : MonoBehaviour
{
    public static MoveGameobject Instance;
    
    public string menuSceneName;
    public GameObject stitchedPatch;

    private void Awake()
    {
        Instance = this;
    }

    public void StartMove()
    {
        StartCoroutine(LoadYourAsyncScene());
    }

    IEnumerator LoadYourAsyncScene()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(menuSceneName, LoadSceneMode.Additive);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        SceneManager.MoveGameObjectToScene(stitchedPatch, SceneManager.GetSceneByName(menuSceneName));
        SceneManager.UnloadSceneAsync(currentScene);
    }
}
