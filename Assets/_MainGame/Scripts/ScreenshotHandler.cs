﻿using UnityEngine;
using UnityEngine.UI;

public class ScreenshotHandler : MonoBehaviour
{
    public static ScreenshotHandler Instance;
    private Camera _myCamera;
    private bool _takeScreenshotOnNextFrame;
    private int _width;
    private int _height;
    private Sprite _capturedImage;
    private Image _picture;
    

    private void Awake() 
    {
        Instance = this;
        _myCamera = gameObject.GetComponent<Camera>();
    }

    private void OnPostRender() 
    {
        if (_takeScreenshotOnNextFrame) 
        {
            _takeScreenshotOnNextFrame = false;
            RenderTexture renderTexture = _myCamera.targetTexture;

            Texture2D renderResult = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
            Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
            renderResult.ReadPixels(rect, 0, 0);
            renderResult.Apply();

            // byte[] byteArray = renderResult.EncodeToPNG();
            // System.IO.File.WriteAllBytes(Application.dataPath + "/CameraScreenshot.png", byteArray);
            // Debug.Log("Saved CameraScreenshot.png");

            Texture2D tex;
            tex = renderResult;
            //tex.LoadImage(byteArray);
            _capturedImage = Sprite.Create(tex,rect,new Vector2(0.5f,0.5f) );
            _picture.sprite = _capturedImage;

            RenderTexture.ReleaseTemporary(renderTexture);
            _myCamera.targetTexture = null;
        }
    }

    public void TakeScreenshot(Image picture, int width , int height)
    {
        _picture = picture;
        _myCamera.targetTexture = RenderTexture.GetTemporary(width, height, 16);
        _takeScreenshotOnNextFrame = true;
    }
}
