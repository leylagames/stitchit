﻿using System;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.UI;

public class IronScript : MonoBehaviour
{
    [System.Serializable]
    public class Range
    {
        public float minValue;
        public float maxValue;
    }

    public GameObject[] dots;
    public GameObject[] ironModels;
    public float[] ironTimers;
    public Range[] modelsRange;
    public GameObject[] ironUI;
    
    public Transform ironTouchedPosition;
    public Transform particleSystemwWhite , particleSystemBlack;
    public GameObject tapAndHoldButton;

    public RectTransform arrow;
    
    private bool _touchStart;
    private Vector3 _mOffset;
    private float _mZCoord;
    private float _progress;
    private int _selectedIronIndex;
    public static bool halfCash;

    private void Awake()
    {
        _selectedIronIndex = PlayerPrefs.GetInt("IronTypeSelected");
        ironModels[_selectedIronIndex].SetActive(true);
        ironUI[_selectedIronIndex].SetActive(true);
    }

    private void OnEnable()
    {
        //GameController.Instance.uiController.gamePanel.ironBar.SetActive(true);
        halfCash = false;
        dots[0].SetActive(false);
        dots[1].SetActive(true);
    }

    void Update()
    {
        if (!_touchStart)
        {
            if (transform.localPosition != ironTouchedPosition.position)
            {
                transform.position = Vector3.Lerp(transform.position,ironTouchedPosition.position,Time.deltaTime * 10f);
                transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles,ironTouchedPosition.localEulerAngles,Time.deltaTime * 10);
            }
        }
        arrow.transform.localPosition = new Vector3(Map(_progress,0f,1f,-270f,270f),arrow.transform.localPosition.y,arrow.transform.localPosition.z);
    }
    
    void OnMouseDown()
    {
        _touchStart = true;
        transform.position = ironTouchedPosition.position;
        transform.localEulerAngles = ironTouchedPosition.localEulerAngles;
        _mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        _mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
        particleSystemwWhite.parent = null;
        particleSystemBlack.parent = null;
        tapAndHoldButton.SetActive(false);
    }

    private void OnMouseUp()
    {
        _touchStart = false;
        if (_progress < modelsRange[_selectedIronIndex].maxValue &&
            _progress >= modelsRange[_selectedIronIndex].minValue)
        {
            particleSystemwWhite.gameObject.SetActive(false);
            GameController.Instance.FinishGameComplete();
        }
        else if (_progress >= modelsRange[_selectedIronIndex].maxValue)
        {
            halfCash = true;
            GameController.Instance.FinishGameComplete();
        }
        else
        {
            tapAndHoldButton.SetActive(true);
        }
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = _mZCoord;
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDrag()
    {
        if (_progress >= modelsRange[_selectedIronIndex].maxValue)
        {
            particleSystemwWhite.gameObject.SetActive(false);
            particleSystemBlack.gameObject.SetActive(true);
        }
        //_progress = GameController.Instance.IncreaseIronBar(ironTimers[_selectedIronIndex]);
        var pos = GetMouseAsWorldPoint() + _mOffset;
        transform.position = new Vector3(pos.x,transform.position.y,pos.z);
        if (PlayerPrefs.GetInt("VibrationToggle") == 1)
        {
            MMVibrationManager.Haptic(HapticTypes.LightImpact, false, true, this);
        }
    }
    
    public float Map (float x, float x1, float x2, float y1,  float y2)
    {
        var m = (y2 - y1) / (x2 - x1);
        var c = y1 - m * x1; 
 
        return m * x + c;
    }
}
