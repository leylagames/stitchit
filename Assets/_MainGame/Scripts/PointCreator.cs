﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCreator : MonoBehaviour
{
    public Transform                      cam;
    GameObject                            pointParent;
    [Range(0f, 1f), SerializeField] float size = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        //cam = Camera.main.transform;

        pointParent = new GameObject();
        Instantiate(pointParent, transform.position, Quaternion.identity);
        pointParent.tag = "PointParent";
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 250.0f))
            {
                Debug.DrawRay(cam.position, cam.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                Debug.Log("Did Hit: " + hit.transform.name);
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.AddComponent<Waypoint>();
                //sphere.transform.position = new Vector3(0, 1.5f, 0);
                sphere.transform.position                      = hit.point;
                sphere.transform.localScale                    = Vector3.one * size;
                sphere.GetComponent<Renderer>().material.color = Color.blue;
                sphere.transform.SetParent(pointParent.transform);
            }
            else
            {
                //Debug.DrawRay(cam.position, cam.TransformDirection(Vector3.forward) * 1000, Color.white);
                Debug.Log("Did not Hit");
            }
        }
    }
}
