﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameAnalyticsSDK;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class DataCarrier : MonoBehaviour
{
	public static DataCarrier Instance;

	[System.Serializable]
	public class Data
	{

		[HorizontalGroup("Group1")] [BoxGroup("Group1/Gameplay"), LabelWidth(75)]
		public Level lvlPrefab;

		[BoxGroup("Group1/Gameplay"), LabelWidth(75)]
		public Sprite refrenceSprite;

		[BoxGroup("Group1/Gameplay"), LabelWidth(75)]
		public int lvlIndex;

		[BoxGroup("Group1/LevelFinish"), LabelWidth(75)]
		public Sprite matchSprite;

		public float cameraZoomValue;

		public string imagePath;
		public Texture2D imageCreated;
		public Sprite imageSprite;

	}

	[BoxGroup("Game Control Variables")] public bool useObstacles;

	[BoxGroup("Free Movement Controller")] public List<Data> levelsData;

	public int originalLevels;
	public int totalLevels;

	public string controlVariable = "A";
	public string myCustomConfig = "False";


	private bool _setRemoteConfigs;

	private bool uselessVariable;

	#region EditorButtons

	#if UNITY_EDITOR
    
    	[Button]
    	public void SortList()
    	{
    		levelsData = levelsData.OrderBy(x => x.lvlIndex).ToList();
    	}
    	
    	[Button]
    	public void VerifyLevels()
    	{
    
    		var query = levelsData.GroupBy(x => x.lvlIndex)
    			.Where(g => g.Count() > 1)
    			.Select(y => new {Element = y.Key, Counter = y.Count()})
    			.ToList();
    
    		Debug.Log(query.Count);
    		foreach (var VARIABLE in query)
    		{
    			Debug.Log(VARIABLE.Element + " = " + VARIABLE.Counter);
    		}
    
    	}
    
    	[Button]
    	public void AddMoreLevels()
    	{
    		var startingIndex = levelsData.Count + 1;
    		for (int i = startingIndex; i <= totalLevels; i++)
    		{
    			var random = Random.Range(2, originalLevels);
    			
    			var temp = CopyListNodes(levelsData[random]);
    			temp.lvlIndex = i;
    			levelsData.Add(temp);
    			
    		}
    	}
    
    	[Button]
    	public void ChangeLevelIndex()
    	{
    		for (int i = 0; i < levelsData.Count; i++)
    		{
    			levelsData[i].lvlIndex = i + 1;
    		}
    	}
    
    	[Button]
    	public void RemoveExtraLevels()
    	{
    		for (int i = originalLevels ; i < totalLevels; i++)
    		{
    			levelsData.RemoveAt(levelsData.Count - 1);
    		}
    	}
    
    	[Button]
    	public void CopyDataToOtherDataArray()
    	{
    		//CopyDataToOtherArray(levelsData,levelsData2);
    	}
    
    	[Button]
    	public void RemoveQuickRepetition(int number)
    	{
    		List<Level> tempList = new List<Level>();
    		
    		for (int i = 0; i < levelsData.Count; i++)
    		{
    			if (tempList.Count < number)
    			{
    				tempList.Add(levelsData[i].lvlPrefab);
    			}
    			else
    			{
    				if (Check(tempList, levelsData[i].lvlPrefab))
    				{
    					levelsData.RemoveAt(i);
    				}
    				else
    				{
    					tempList.RemoveAt(0);
    					tempList.Add(levelsData[i].lvlPrefab);
    				}
    			}
    		}
    	}
    
    	public bool Check(List<Level> temp, Level levelsDataLevel)
    	{
    		for (int i = 0; i < temp.Count; i++)
    		{
    			if (levelsDataLevel == temp[i])
    			{
    				return true;
    			}
    		}
    
    		return false;
    	}

        void CopyDataToOtherArray(List<Data> copyTo, List<Data> copyFrom)
    	{
    		for (int i = 0; i < copyFrom.Count; i++)
    		{
    			copyTo.Add(CopyListNodes(copyFrom[i]));
    		}
    	}
    
    
    #endif

	#endregion

	
	public Data CopyListNodes(Data from)
	{
		var temp = new Data();
		temp.lvlPrefab = from.lvlPrefab;
		temp.refrenceSprite = from.refrenceSprite;
		temp.matchSprite = from.matchSprite;
		temp.lvlIndex = from.lvlIndex;
		temp.cameraZoomValue = from.cameraZoomValue;
		temp.imagePath = from.imagePath;
		temp.imageCreated = from.imageCreated;
		temp.imageSprite = from.imageSprite;
		return temp;
	}

	private void Awake()
	{
		if (Instance != null)
		{
			Destroy(this.gameObject);
		}
		else
		{
			Instance = this;
			DontDestroyOnLoad(this.gameObject);
		}

		GameAnalytics.Initialize();
		Application.targetFrameRate = 60;
	}

	private void Start()
	{
		for (int i = 0; i < PlayerPrefs.GetInt("Level"); i++)
		{
			LoadSprites(i);
		}
	}

	private void Update()
	{
		if (!_setRemoteConfigs)
		{
			if (GameAnalytics.IsRemoteConfigsReady())
			{
				controlVariable = GameAnalytics.GetRemoteConfigsValueAsString("Settings", "A");
				myCustomConfig = GameAnalytics.GetRemoteConfigsValueAsString("MyConfig", "False");
				SetCustomDimension(controlVariable);
				_setRemoteConfigs = true;
			}
		}
	}

	void SetCustomDimension(string value)
	{
		GameAnalytics.SetCustomDimension01(value);
	}

	public void AddNewLevel()
	{
		var random = Random.Range(2, originalLevels);
		var temp = CopyListNodes(levelsData[random]);
		temp.lvlIndex = levelsData.Count;;
		levelsData.Add(temp);
	}

	public static Sprite ToSprite (Texture2D t, Vector2? v = null)
	{
		var vector = v ?? new Vector2(.5f, .5f);
		return Sprite.Create(t, new Rect(0, 0, t.width,       t.height),       vector);
	}
	
	public void LoadSprites(int index)
	{
		string path = PlayerPrefs.GetString("Level" + index);
		if (File.Exists(path))
		{
			Texture2D texture = new Texture2D(1, 1);
			texture.LoadImage(File.ReadAllBytes(path));
			DataCarrier.Instance.levelsData[index].imagePath = path;
			DataCarrier.Instance.levelsData[index].imageCreated = texture;
			DataCarrier.Instance.levelsData[index].imageSprite  = ToSprite(texture);
		}
	}
}