﻿using System;
using UnityEngine;

public class MusicScript : MonoBehaviour
{
    public static MusicScript Instance;
    private AudioSource musicSource;
    
    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        musicSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (GameManager.Instance.MusicToggle == 1)
        {
            musicSource.volume = 1;
        }
        else
        {
            musicSource.volume = 0;
        }
    }
}
