﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScreenShot : MonoBehaviour
{
    [SerializeField] private RectTransform targetRect;
    public Camera _camera;

    public void TakeScreenshot(string fileName)
    {
        Debug.Log("Here");
        StartCoroutine(CutSpriteFromScreen(fileName));
    }
 
    private IEnumerator CutSpriteFromScreen(string fileName)
    {
        yield return new WaitForEndOfFrame();
 
        var corners = new Vector3[4];
        targetRect.GetWorldCorners(corners);
        var bl = RectTransformUtility.WorldToScreenPoint(_camera, corners[0]);
        var tl = RectTransformUtility.WorldToScreenPoint(_camera, corners[1]);
        var tr = RectTransformUtility.WorldToScreenPoint(_camera, corners[2]);
 
        var height = tl.y - bl.y;
        var width = tr.x - bl.x;
 
        Texture2D tex = new Texture2D((int)width, (int)height, TextureFormat.RGB24, false);
        Rect rex = new Rect(bl.x,bl.y,width,height);
        tex.ReadPixels(rex, 0, 0);
        tex.Apply();
        var bytes = tex.EncodeToPNG();
        Destroy(tex);
 
        File.WriteAllBytes(Application.dataPath + fileName, bytes);
    }
}
