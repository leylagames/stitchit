﻿using UnityEngine;

public class StichScript : MonoBehaviour
{
    public MeshRenderer _meshRenderer;

    void Start()
    {
        if (_meshRenderer)
        {
            _meshRenderer.material = GameController.Instance.stichMaterials[GameController.Instance.selectedColorIndex];
        }
    }
}
