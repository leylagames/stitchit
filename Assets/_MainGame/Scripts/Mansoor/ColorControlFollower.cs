﻿using System;
using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EZCameraShake;
using MoreMountains.NiceVibrations;
using Sirenix.OdinInspector;
using TouchControlsKit;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class ColorControlFollower : MonoBehaviour
{
	public enum MovementMode {Automatic, Manual};
	
	[System.Serializable]
	public class AreaStatus
	{

		public int areaID;
		public int pointsFilled;
		public int totalPoints;
		public float percentageDone;
		public bool isFilledCompletely;
		public PointType actualColor = PointType.None;
		public PointType lastSelectedColor = PointType.None;

	}

	[System.Serializable]
	public class ActionData
	{
		public int staringIndex;
		public int endingIndex;
		public int currentProgress;
		public int correctProgress;
		public int actionAreaIndex;
		public List<GameObject> _stitches = new List<GameObject>();
		public List<Waypoint> _donePoints = new List<Waypoint>();

	}
	
	[System.Serializable]
	public class PercentageChecker
	{
		public float value;
		public bool isCalled = false;
	}
	
	[BoxGroup("Movement Type", true, true), SerializeField]
	public MovementMode thisMovementMode;
	
	[BoxGroup("Free Movement Properties", true, true), SerializeField]
	public float touchSensitivity;
	[BoxGroup("Free Movement Properties", true, true), SerializeField]
	public float minX, maxX, minY, maxY;
	
	[BoxGroup("Auto Movement Properties", true, true), SerializeField]
	public float machineSpeedMax;
	[BoxGroup("Auto Movement Properties", true, true), SerializeField]
	public float machineSpeedMin;
	[BoxGroup("Auto Movement Properties", true, true), SerializeField]
	public float machineSpeedCurrent;
	[BoxGroup("Auto Movement Properties", true, true), SerializeField]
	private int _positionCounter = 0;
	[BoxGroup("Auto Movement Properties", true, true), SerializeField]
	private int _areaCounter = 0;
	
	[BoxGroup("General Properties", true, true), SerializeField]
	public float maxHeight;
	[BoxGroup("General Properties", true, true), SerializeField]
	public float minHeight;
	[BoxGroup("General Properties", true, true), SerializeField]
	public float heightMovementSpeed = 10f;
	[BoxGroup("General Properties", true, true), SerializeField]
	public Transform pathContainer;
	[BoxGroup("General Properties", true, true), SerializeField]
	public Transform stichesContainer;
	[BoxGroup("General Properties", true, true), SerializeField]
	public Transform smoothFollowCam;
	[BoxGroup("General Properties", true, true), SerializeField]
	public Transform cameraPosForCutting;
	[BoxGroup("General Properties", true, true), SerializeField]
	public AudioSource sewingFinished;
	[BoxGroup("General Properties", true, true), SerializeField]
	public bool startAtFirstPointOnAwake;
	[BoxGroup("General Properties", true, true), SerializeField]
	private Waypoint[] _points;
	[BoxGroup("General Properties", true, true), SerializeField]
	private int currentProgress;
	[BoxGroup("General Properties", true, true), SerializeField]
	private int correctProgress;
	[BoxGroup("General Properties", true, true), SerializeField]
	private AudioSource _machineSound;
	[BoxGroup("General Properties", true, true), SerializeField]
	public float completionPercentage;

	[BoxGroup("General Properties", true, true), SerializeField]
	public Camera screenshotCamera;
	[BoxGroup("General Properties", true, true), SerializeField]
	public Transform cameraTransform;

	[BoxGroup("General Properties", true, true), SerializeField]
	public Vector3 cameraOffset;
	
	
	[BoxGroup("Trail Properties", true, true), SerializeField]
	public GameObject trailParticle;
	[BoxGroup("Trail Properties", true, true), SerializeField]
	public int trailLength = 10;
	[BoxGroup("Trail Properties", true, true), SerializeField]
	public List<GameObject> trailPoints;

	[BoxGroup("AutoComplete Properties", true, true), SerializeField]
	private List<GameObject> _localStitches = new List<GameObject>();
	[BoxGroup("AutoComplete Properties", true, true), SerializeField]
	private List<Waypoint> _localDonePoints = new List<Waypoint>();
	[BoxGroup("AutoComplete Properties", true, true), SerializeField]
	bool autoBtn_isActive = false;
	[BoxGroup("AutoComplete Properties", true, true), SerializeField]
	float combinedPercentage = 0f;
	[BoxGroup("AutoComplete Properties", true, true), SerializeField]
	float autofillThresshold = 0.7f;
	[BoxGroup("AutoComplete Properties", true, true), SerializeField]
	float immidiateFillThreshold = 0.95f;
	[BoxGroup("AutoComplete Properties", true, true)]
	public GameObject moveButton;
	[BoxGroup("AutoComplete Properties", true, true)]
	public Button autoFillButton;
	[BoxGroup("AutoComplete Properties", true, true), SerializeField]
	public List<AreaStatus> areaStatus;
	
	
	[BoxGroup("Undo Properties", true, true), SerializeField]
	private List<ActionData> _actions = new List<ActionData>();
	[BoxGroup("Undo Properties", true, true), SerializeField]
	private ParticleSystem undoParticles;
	
	[BoxGroup("Area Properties", true, true), SerializeField]
	public Area[] _areas;
	
	[HideInInspector] public bool finished;
	
	private List<PointType> stageColors = new List<PointType>();
	private List<int> _uniqueColors = new List<int>();
	private bool _goingNext;
	private int _currentStage;
	private bool _willActivate = true;
	private int _tempCurrentProgress;
	private int _tempCorrectProgress;
	private int _previousClosestWaypoint;
	private float _movementSpeedOriginal;
	private bool onceCheck;
	private float _inactivityTimer;
	private bool _runOnce;
	private bool _showOnce = false;
	private bool _immidiateComplete;
	private bool _onceAgain;
	private bool _collisionDetected;
	private float _idleTimer;
	private bool _handShown;
	private float _toastTimer;
	private bool _once;
	private float _trailTimer;
	private bool _startTrail;
	private float _machineHeightValue;
	private bool _actionDataSet;
	private bool isAuto = false;
	private int _actionIndex;
	private int _previousActionIndex = 0;
	private float _clickTimer;
	private int _trailCounter;
	private bool _areaChanged;
	private bool _stitchingFinished;
	private bool _machineIsSewing;
	private bool _undoAction;
	
	public int currentColorId;
	public int _currentAreaId;

	public float idleTimer;
	
	const string VibrationKey = "VibrationToggle";

	[HideInInspector] public Collider collider;
	public PercentageChecker[] percentageChecker;
	
	
	private void Start()
	{
		collider = GetComponent<Collider>();
		_machineSound = GetComponent<AudioSource>();
		_points = pathContainer.GetComponentsInChildren<Waypoint>();
		GameController.Instance.selectedColorIndex = (int) _points[0].colorIndex;
		GameController.Instance.ChangeSewingMachineThreadColor();

		GameController.Instance.uiController.gamePanel.undoButton.GetComponent<Button>().onClick
			.AddListener(() => { UndoAction(); });

		if (GameManager.Instance.CurrentLevel < 2)
		{
			machineSpeedMax = 2;
		}

		//if (PlayerPrefs.HasKey("TutorialFinished"))
		{
			GameController.Instance.uiController.gamePanel.SetSelectionPointersColor();
			GameController.Instance.uiController.gamePanel.SetSelectionPointersStitches();
			GameController.Instance.uiController.gamePanel.SetSelectionPointersMachines();
		}

		currentColorId = (int) _points[0].colorIndex;

		machineSpeedCurrent = machineSpeedMin;

		for (int i = 0; i < _points.Length; i++)
		{
			_points[i].gameObject.tag = "point";
			if (_points[i].colorIndex != PointType.None)
			{
				stageColors.Add(_points[i].colorIndex);
				if (!CheckListForColor((int) _points[i].colorIndex))
				{
					_uniqueColors.Add((int) _points[i].colorIndex);
				}

				GameController.Instance.uiController.gamePanel.colorButtons[(int) _points[i].colorIndex]
					.SetActive(true);
			}

			SetAreaStatus(i);
		}

		RefreshUI();

		if (startAtFirstPointOnAwake)
		{
			var nextPos = _points[0].transform.position + cameraOffset;
			cameraTransform.position = nextPos;
			transform.position = _points[0].transform.position;
			currentColorId = (int) _points[0].colorIndex;
			_currentAreaId = 0;
			GameController.Instance.currentAreaID = 0;
		}

		_areas = FindObjectsOfType<Area>();
		_areas = _areas.OrderBy(x => x.areaId).ToArray();

		HighlightCurrentArea();

		autoFillButton.onClick.AddListener(() => { AutoCompleteFunction(); });


		for (int i = 0; i < trailLength; i++)
		{
			trailPoints.Add(_points[i].gameObject);
		}

		GameController.Instance._machineScript.SetAnimation(false);
		
		CheckRemoteConfig();
	}
	
	private void CheckRemoteConfig()
	{
		if (DataCarrier.Instance.controlVariable == "A")
		{
			cameraOffset = new Vector3(0, 4.3f, -1.5f);
			machineSpeedMin = 1f;
			machineSpeedMax = 1.5f;
		}
		else if (DataCarrier.Instance.controlVariable == "B")
		{
			cameraOffset = new Vector3(0, 4.3f, -1.5f);
			machineSpeedMin = 1.5f;
			machineSpeedMax = 1.8f;
		}
		else if (DataCarrier.Instance.controlVariable == "C")
		{
			cameraOffset = new Vector3(0, 3.3f, -1.4f);
			machineSpeedMin = 1.5f;
			machineSpeedMax = 1.8f;
		}
		else if (DataCarrier.Instance.controlVariable == "D")
		{
			cameraOffset = new Vector3(0, 3f, -1f);
			machineSpeedMin = 1.2f;
			machineSpeedMax = 1.2f;
		}
	}
	
	private void Update()
	{
		if (!_stitchingFinished)
		{
			var nextPos = transform.position + cameraOffset;
			var velocity = Vector3.zero;
			cameraTransform.position = Vector3.SmoothDamp(cameraTransform.position, nextPos, ref velocity, 0.1f);
			//cameraTransform.position = nextPos;
		}

		if (_machineIsSewing)
		{
			machineSpeedCurrent = Mathf.Lerp(machineSpeedCurrent, machineSpeedMax, Time.deltaTime);
		}
		else
		{
			machineSpeedCurrent = machineSpeedMin;
		}


		if (thisMovementMode == MovementMode.Automatic)
		{
			GameController.Instance.uiController.gamePanel.stitchEventTriggerButton.SetActive(true);
			
			if (!GameController.Instance.isGameFinished && !_goingNext && !_collisionDetected && !_undoAction)
			{
				transform.GetChild(0).transform.localPosition = new Vector3(0, _machineHeightValue, 0);
				if (_positionCounter < _points.Length)
				{
					if (_points[_positionCounter].isDrawn)
					{
						_positionCounter++;
					}
					else
					{
						transform.position = Vector3.MoveTowards(transform.position,
							_points[_positionCounter].transform.position,
							Time.deltaTime * machineSpeedCurrent);
					}
				}
			}

			if (GameController.Instance.startMachineMovement && !_goingNext &&
			    !GameController.Instance.isGameFinished &&
			    !_collisionDetected && !_undoAction)
			{
				if (DataCarrier.Instance.useObstacles)
				{
					_startTrail = false;
					_trailCounter = 0;
					trailParticle.SetActive(false);
					trailParticle.GetComponent<TrailRenderer>().emitting = false;
				}

				_machineHeightValue = Mathf.Lerp(_machineHeightValue, minHeight, Time.deltaTime * heightMovementSpeed);
				_clickTimer += Time.deltaTime;
				if (_clickTimer >= 0.1f)
				{
					if (GameController.Instance.uiController.gamePanel.stitchButton)
					{
						GameController.Instance.uiController.gamePanel.toolsPanel.SetActive(false);
						GameController.Instance.uiController.gamePanel.undoButton.SetActive(false);
						_machineIsSewing = true;
					}
					
					GameController.Instance._machineScript.SetAnimation(true);
					collider.enabled = true;
					if (_positionCounter < _points.Length)
					{
						if (Vector3.Distance(transform.position, _points[_positionCounter].transform.position) < 0.01f)
						{
							if (_positionCounter < _points.Length - 1 && _points[_positionCounter].isDrawn)
							{
								_positionCounter++;
								_areaCounter++;
							}
						}
					}
				}
			}
			else
			{
				// if (Input.GetMouseButtonUp(0))
				// {
				// 	_collisionDetected = false;
				// }

				if (!_stitchingFinished)
				{
					GameController.Instance.uiController.gamePanel.toolsPanel.SetActive(true);
					GameController.Instance.uiController.gamePanel.undoButton.SetActive(true);
					_machineIsSewing = false;
				}
				
				collider.enabled = false;
				GameController.Instance._machineScript.SetAnimation(false);
				_machineHeightValue = Mathf.Lerp(_machineHeightValue, maxHeight, Time.deltaTime * heightMovementSpeed);
				_clickTimer = 0;
				SetActionData(_currentAreaId);
				if (DataCarrier.Instance.useObstacles)
				{
					UpdateTrailPoints(_positionCounter);
					_trailTimer += Time.deltaTime;
					if (_trailTimer >= 1)
					{
						if (!_startTrail)
						{
							StartTrail();
							_startTrail = true;
						}
					}
				}
			}
			

			if (DataCarrier.Instance.useObstacles)
			{
				if (_startTrail && _positionCounter != _points.Length - 1)
				{
					if (trailPoints[_trailCounter] != null)
					{
						trailParticle.transform.position = Vector3.MoveTowards(trailParticle.transform.position,
							trailPoints[_trailCounter].transform.position, Time.deltaTime * machineSpeedCurrent);


						if (Vector3.Distance(trailParticle.transform.position,
							    trailPoints[_trailCounter].transform.position) <
						    0.05f)
						{
							if (_trailCounter < trailLength - 1)
							{
								if (trailPoints[_trailCounter + 1] != null)
								{
									_trailCounter++;
								}
								else
								{
									StartCoroutine(ParticlePositionChange());
								}
							}
							else
							{
								StartCoroutine(ParticlePositionChange());
							}
						}
					}
				}
			}

		}
		else if(thisMovementMode == MovementMode.Manual)
		{
			GameController.Instance.uiController.gamePanel.stitchEventTriggerButton.SetActive(false);

			if (_currentAreaId < areaStatus.Count)
			{
				if (areaStatus[_currentAreaId].percentageDone >= autofillThresshold)
				{
					AutoCompleteArea(_currentAreaId);
				}
			}


			if (!GameController.Instance.isGameFinished)
			{
				transform.GetChild(0).transform.localPosition = new Vector3(0, _machineHeightValue, 0);
				
				if (Input.GetMouseButton(0) && !_goingNext && !GameController.Instance.uiDetector.isOverUI && !_areaChanged &&!_collisionDetected)
				{
					_machineHeightValue = Mathf.Lerp(_machineHeightValue, minHeight, Time.deltaTime * heightMovementSpeed);
					_clickTimer += Time.deltaTime;
					
					if (_clickTimer >= 0.1f)
					{
						collider.enabled = true;

						var zAxis = TCKInput.GetAxis("Touch", "Vertical");
						var xAxis = TCKInput.GetAxis("Touch", "Horizontal");
						
						var _newPos = new Vector3(xAxis, 0, zAxis);
						
						_newPos *= touchSensitivity;
						_newPos += transform.position;
						_newPos.x = Mathf.Clamp(_newPos.x, minX, maxX);
						_newPos.z = Mathf.Clamp(_newPos.z, minY, maxY);
						transform.position = _newPos;
					}
				}
				else if(Input.GetMouseButtonUp(0))
				{
					_collisionDetected = false;
					_areaChanged = false;
					_clickTimer = 0;
					collider.enabled = false;
					GameController.Instance._machineScript.SetAnimation(false);
					_machineHeightValue = Mathf.Lerp(_machineHeightValue, maxHeight, Time.deltaTime * heightMovementSpeed);
					SetActionData(_currentAreaId);
					if (combinedPercentage > immidiateFillThreshold)
					{
						if (!_onceAgain)
						{
							_onceAgain = true;
							StartCoroutine(AddDelay());
						}
					}
				}
			}
		}

		if (!GameController.Instance.isGameFinished && !GameController.Instance.overlockMachine.gameObject.activeInHierarchy)
		{
			_idleTimer += Time.deltaTime;
			if (_idleTimer >= idleTimer)
			{
				if (!GameController.Instance.tutorial.tutorialStitches.activeInHierarchy &&
				    !GameController.Instance.tutorial.tutorialMachines.activeInHierarchy)
				{
					GameController.Instance.tutorial.ShowTutorial();
				}
			}
		}
	}

	void UpdateTrailPoints(int currentIndex)
	{
		for (int i = 0; i < trailLength; i++)
		{
			if ((_positionCounter + i) < _points.Length - 1)
			{
				trailPoints[i] = _points[_positionCounter + i].gameObject;
			}
			else
			{
				trailPoints[i] = null;
			}
		}
	}

	void HighlightCurrentArea()
	{
		// for (int i = 0; i < _areas.Length; i++)
		// {
		// 	_areas[i].HightLight(false);
		// }

		_areas[_currentAreaId].HightLight(true);
	}

	private bool CheckListForColor(int colorId)
	{
		for (int i = 0; i < _uniqueColors.Count; i++)
		{
			if (_uniqueColors[i] == colorId)
			{
				return true;
			}
		}

		return false;
	}

	private void RefreshUI()
	{
		GameController.Instance.uiController.gamePanel.RefreshUI(_uniqueColors.Count);
	}

	private void AutoCompleteArea(int areaID)
	{
		if (_currentAreaId < areaStatus.Count)
		{
			for (int i = 0; i < areaStatus[_currentAreaId].totalPoints; i++)
			{
				if (!_points[i].isDrawn)
				{
					NewMethodSpawnStitch(_points[i].gameObject, false);
				}
			}
		}
	}

	public void AutoCompleteFunction()
	{
		GameController.Instance.isGameFinished = true;
		autoFillButton.gameObject.SetActive(false);
		isAuto = true;
		for (int j = 0; j < _areas.Length; j++)
		{
			GameController.Instance.currentAreaID = j;
			_currentAreaId = j;
			for (int i = 0; i < _points.Length; i++)
			{
				if (_points[i].areaId == GameController.Instance.currentAreaID && !_points[i].isDrawn)
				{
					GameController.Instance.selectedColorIndex = (int) areaStatus[_currentAreaId].lastSelectedColor;
					currentColorId = (int) areaStatus[_currentAreaId].lastSelectedColor;
					GameController.Instance.ChangeSewingMachineThreadColor();
					if (_immidiateComplete)
					{
						NewMethodSpawnStitch(_points[i].gameObject, false);
					}
					else
					{
						NewMethodSpawnStitch(_points[i].gameObject, true);
					}

					_actionIndex++;
				}
			}
		}

		GameController.Instance.uiController.gamePanel.undoButton.SetActive(false);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("point"))
		{
			GameController.Instance._machineScript.SetAnimation(true);
			currentColorId = (int) areaStatus[GameController.Instance.currentAreaID].actualColor;
			GameController.Instance.uiController.gamePanel.undoButton.SetActive(true);
			GameObject detectedObj = other.gameObject;
			NewMethodSpawnStitch(detectedObj, false);
			_actionIndex++;
			CheckAllWaypointsDone();
		}

		if (other.gameObject.CompareTag("Obstacle"))
		{
			collider.enabled = false;
			_collisionDetected = true;
			SetActionData(_currentAreaId);
			CameraShaker.Instance.ShakeOnce(2f, 2f, 0.1f, 1f);
			MMVibrationManager.Haptic(HapticTypes.Warning, false, true, this);
			UndoAction();
		}
	}

	public IEnumerator MoveMachineBack(int positionIndex)
	{
		yield return new WaitForSeconds(0.5f);
		if (_points[positionIndex].isDrawn)
		{
			_positionCounter++;
			positionIndex++;
		}
		
		transform.DOMove(_points[positionIndex].transform.position, 1f).OnComplete(() =>
		{
			_collisionDetected = false;
			_undoAction = false;
		});
	}

	public void SetActionData(int areaID)
	{
		if (_actionDataSet)
		{
			ActionData action = new ActionData();
			action.staringIndex = _previousActionIndex;
			action.endingIndex = _actionIndex;
			action.currentProgress = currentProgress;
			action.correctProgress = correctProgress;
			action.actionAreaIndex = areaID;
			
			if (_localStitches.Count != 0)
			{
				for (int i = 0; i < _localStitches.Count; i++)
				{
					action._stitches.Add(_localStitches[i]);
				}

				for (int i = 0; i < _localDonePoints.Count; i++)
				{
					action._donePoints.Add(_localDonePoints[i]);
				}

				_localStitches.Clear();
				_localDonePoints.Clear();
				_actions.Add(action);
				_previousActionIndex = _actionIndex;
				_actionDataSet = false;
			}
		}
	}

	public void UndoAction()
	{
		if (_actions.Count != 0)
		{
			_undoAction = true;
			
			ActionData actionToUndo = _actions[_actions.Count - 1];
			
			areaStatus[actionToUndo.actionAreaIndex].isFilledCompletely = false;
			
			_areas[actionToUndo.actionAreaIndex].GetComponent<Collider>().enabled = true;
			
			int count = actionToUndo._stitches.Count;

			//_positionCounter = actionToUndo.staringIndex;
			
			_positionCounter -= count + 1;
			
			if (_positionCounter < 0)
			{
				_positionCounter = 0;
			}

			// StartCoroutine(WaitRoutine(_positionCounter - count));
			
			for (int i = 0; i < count; i++)
			{
				var stitch = actionToUndo._stitches[0];
				actionToUndo._stitches.RemoveAt(0);
				actionToUndo._donePoints[0].isDrawn = false;
				actionToUndo._donePoints[0].GetComponent<SphereCollider>().enabled = true;
				actionToUndo._donePoints.RemoveAt(0);
				areaStatus[actionToUndo.actionAreaIndex].pointsFilled--;
				DestroyStitches(stitch.gameObject);
			}

			_currentAreaId = actionToUndo.actionAreaIndex;
			GameController.Instance.currentAreaID = actionToUndo.actionAreaIndex;
			HighlightCurrentArea();

			_actions.RemoveAt(_actions.Count - 1);
			if (_actions.Count != 0)
			{
				_previousActionIndex = _actions[_actions.Count - 1].endingIndex;
				_actionIndex = _previousActionIndex;
				currentProgress = _actions[_actions.Count - 1].currentProgress;
				correctProgress = _actions[_actions.Count - 1].correctProgress;
			}
			else
			{
				_previousActionIndex = 0;
				_actionIndex = 0;
				currentProgress = 0;
				correctProgress = 0;
			}

			if (_actions.Count == 0)
			{
				GameController.Instance.uiController.gamePanel.undoButton.SetActive(false);
			}

			combinedPercentage = (float) currentProgress / _points.Length;
		}
		
		StartCoroutine(MoveMachineBack(_positionCounter));
	}

	IEnumerator WaitRoutine(int index)
	{
		if (index < 0)
		{
			index = 0;
		}
		
		Invoke("CollisionDetected",3);
		yield return new WaitUntil(
			() => Vector3.Distance(transform.position, _points[index].transform.position) < 0.01f);
		_collisionDetected = false;
	}

	void CollisionDetected()
	{
		_collisionDetected = false;
	}

	public void DestroyStitches(GameObject stitch)
	{
		var offset = stitch.transform.localScale * 1.1f;
		stitch.transform.DOScale(offset, 0.25f).SetEase(Ease.OutBounce).OnComplete(() =>
		{
			stitch.transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InElastic).OnComplete(() =>
			{
				var particle = Instantiate(undoParticles, stitch.transform.position, Quaternion.identity);
				particle.startColor = stitch.GetComponent<MeshRenderer>().material.color;
				particle.Play();
				Destroy(particle.gameObject, 1);
				Destroy(stitch.gameObject);
				
				// while (!_points[_positionCounter].isDrawn && _positionCounter > 0)
				// {
				// 	_positionCounter--;
				// }

				if (_positionCounter != 0)
				{
					_areaCounter--;
				}
			});
		});
	}

	private void NewMethodSpawnStitch(GameObject detectedObj, bool isAuto)
	{
		var wayPoint = detectedObj.GetComponent<Waypoint>();
		if (wayPoint.areaId != _currentAreaId)
		{
			return;
		}

		if (currentColorId == GameController.Instance.selectedColorIndex)
		{
			correctProgress += 1;
		}

		currentProgress += 1;

		detectedObj.GetComponent<Collider>().enabled = false;
		wayPoint.isDrawn = true;
		_localDonePoints.Add(wayPoint);
		GameController.Instance.SetProgress(currentProgress, _points.Length);
		Vector3 currentScale = Vector3.zero;
		Vector3 increasedScale = Vector3.zero;
		var currentStich =
			Instantiate(GameController.Instance.stichesModels[GameController.Instance.selectedStichIndex],
				detectedObj.transform.position, Quaternion.Euler(0, 0, 0));
		currentStich.transform.SetParent(
			stichesContainer.transform.GetChild(GameController.Instance.currentAreaID).transform, true);

		var position = currentStich.transform.localPosition;
		currentStich.transform.localPosition = new Vector3(currentStich.transform.localPosition.x,
			currentStich.transform.localPosition.y - 0.5f,
			currentStich.transform.localPosition.z);
		
		currentStich.transform.DOLocalMove(position, 0.1f);
		
		
		if (isAuto)
		{
			currentScale = currentStich.transform.localScale;
			increasedScale = currentScale * 1.2f;
		}

		if (isAuto)
		{
			currentStich.transform.localScale = Vector3.zero;
			currentStich.transform.DOScale(increasedScale, 0.5f).SetEase(Ease.OutBounce).OnComplete(() =>
			{
				currentStich.transform.DOScale(currentScale, 0.25f);
			});
		}

		_localStitches.Add(currentStich.gameObject);
		
		if (PlayerPrefs.GetInt(VibrationKey) == 1 && !isAuto)
		{
			if (!_machineSound.isPlaying)
			{
				_machineSound.Play();
			}

			MMVibrationManager.Haptic(HapticTypes.Selection, false, true, this);
		}

		FillAreaStatistics(GameController.Instance.currentAreaID);
		
		if (areaStatus[_currentAreaId].isFilledCompletely)
		{
			MoveMachineToNextArea();
		}
		
		_actionDataSet = true;
		_idleTimer = 0;
		GameController.Instance.tutorial.HideTutorial();
		if (!PlayerPrefs.HasKey("Tutorial"))
		{
			PlayerPrefs.SetInt("Tutorial",1);
		}
		
		
				
		// -> Start Flipping Mechanic

		if (useFlippingMechanics)
		{
			//
			// var extracurrentStich = Instantiate(GameController.Instance.extraStitchModel, detectedObj.transform.position, Quaternion.Euler(0, 0, 0));
			// extracurrentStich.transform.SetParent(flipMechanic.stitchesParent.transform, true);
			// Vector3 ext_LocalScale = extracurrentStich.transform.localScale;
			// ext_LocalScale.x                                                     = ext_LocalScale.x * Random.Range(1.2f, 1.5f);
			// ext_LocalScale.z                                                     = ext_LocalScale.x;
			// ext_LocalScale.y                                                     = ext_LocalScale.y * ( 0.1f * Random.Range(4, 9));
			// extracurrentStich.transform.localScale                               = ext_LocalScale;
			// extracurrentStich.transform.localRotation                            = Quaternion.Euler(0f, 180f, 180f);
			// extracurrentStich.gameObject.tag                                     = "ExtraStitch";
			// extracurrentStich.gameObject.AddComponent<MeshCollider>().sharedMesh = extracurrentStich.GetComponent<MeshFilter>().sharedMesh;
			//
			//
			// Vector3 ext_position = extracurrentStich.transform.localPosition;
			// ext_position.y                            = -0.05f;
			// extracurrentStich.transform.localPosition = ext_position;
			//
			// // flipMechanic.spawnStitches.Add(extracurrentStich.gameObject);
			//
			// var irregularData = new FlipMechanics.irregularStitch();
			// irregularData.stitches = new List<GameObject>();
			// irregularData.spawnStitches        = extracurrentStich.gameObject;
			// int randomLoop = Random.Range(1, 3);
			// for (int i = 0; i < randomLoop; i++)
			// {
			// 	var IrregularStitch = Instantiate(GameController.Instance.extraIrregularStitchModel[Random.Range(0,                                                                        GameController.Instance.extraIrregularStitchModel.Length)],
			// 		detectedObj.transform.position + new Vector3(Random.Range(0f, 0.15f), 0 , Random.Range(0f, 0.15f)), Quaternion.Euler(0f, 180f, 180f) * Quaternion.Euler(Random.Range(-30, 31), 0, Random.Range(-30, 31)));
			// 	IrregularStitch.transform.SetParent(flipMechanic.IrregularstitchesParent.transform, true);
			// 	ext_LocalScale   = IrregularStitch.transform.localScale;
			// 	ext_LocalScale.x = ext_LocalScale.x * Random.Range(0.4f, 0.6f);
			// 	ext_LocalScale.z = ext_LocalScale.x;
			// 	ext_LocalScale.y = ext_LocalScale.y * ( 0.1f * Random.Range(6, 11));
			// 	
			// 	ext_position = IrregularStitch.transform.localPosition;
			// 	ext_position.y                          = -0.05f;
			// 	IrregularStitch.transform.localPosition = ext_position;
			// 	
			// 	irregularData.stitches.Add(IrregularStitch.gameObject);
			// 	
			// }
			//
			// flipMechanic.irregularStitches.Add(irregularData);
			//
			//
			
		}
		
		// -> Ends Flipping Mechanic
	}

	public void MoveMachineToNextArea()
	{
		SetActionData(_currentAreaId);
		
		GameController.Instance.startMachineMovement = false;
		GameController.Instance.currentAreaID++;
		_currentAreaId++;
		_areaCounter = 0;
		_goingNext = true;
		collider.enabled = false;

		_positionCounter++;
		
		Transform firstpoint = _points[0].transform;
		for (int i = 0; i < _points.Length; i++)
		{
			if (_points[i].areaId == _currentAreaId)
			{
				firstpoint = _points[i].transform;
				break;
			}
		}
		transform.DOMove(firstpoint.transform.position, 0.5f).OnComplete(() =>
		{
			_goingNext = false;
			HighlightCurrentArea();
		});

		_areaChanged = true;
	}
	
	IEnumerator ParticlePositionChange()
	{
		trailParticle.SetActive(false);
		_trailCounter = 0;
		trailParticle.transform.position = trailPoints[0].transform.position;
		yield return new WaitForSeconds(0.1f);
		trailParticle.SetActive(true);
	}

	public void StartTrail()
	{
		if (trailPoints[0] != null)
		{
			trailParticle.transform.position = trailPoints[0].transform.position;
			trailParticle.SetActive(true);
			trailParticle.GetComponent<TrailRenderer>().emitting = true;
		}
	}

	IEnumerator AddDelay()
	{
		yield return new WaitForSeconds(0.1f);
		AutoCompleteFunction();
	}

	private IEnumerator Finish(float completionPercentage)
	{
		if (!finished)
		{
			GameController.Instance.isGameFinished = true;
			yield return new WaitForSeconds(0.25f);
			transform.GetChild(0).gameObject.SetActive(false);
			autoFillButton.gameObject.SetActive(false);
			sewingFinished.Play();
			finished = true;
			GameController.Instance.CloseGamePanel();
			GameController.Instance.FinishGame(correctProgress);
		}
	}

	public IEnumerator FillRoutineCamera(float end, float duration)
	{
		float counter = 0;
		float startValue = Camera.main.fieldOfView;
		float endValue = end;
		float newValue = startValue;
		while (counter < duration)
		{
			counter += Time.deltaTime;
			newValue = Mathf.Lerp(startValue, endValue, counter / duration);
			Camera.main.fieldOfView = newValue;
			yield return null;
		}
	}

	public void FinishGame()
	{
		int totalWaypoints = _points.Length;
		GameController.Instance.correctProgress = correctProgress;
		GameController.Instance.totalWaypoints = totalWaypoints;
		completionPercentage = (((float) correctProgress / (float) currentProgress)) * 100;
		StartCoroutine(Finish(completionPercentage));
	}

	public void CheckAllWaypointsDone()
	{
		if (!_once)
		{
			for (int i = 0; i < _points.Length; i++)
			{
				if (!_points[i].isDrawn)
				{
					return;
				}
			}
		}

		if (!_once)
		{
			//FinishGame();
			GameController.Instance.uiController.gamePanel.undoButton.SetActive(false);
			_once = true;
		}
	}

	void SetAreaStatus(int index)
	{
		if (areaStatus.Count == 0)
		{
			AreaStatus status = new AreaStatus();
			status.areaID = _points[index].areaId;
			status.actualColor = _points[index].colorIndex;
			status.lastSelectedColor = status.actualColor;
			status.totalPoints++;
			areaStatus.Add(status);
		}
		else
		{

			bool existingFound = false;
			for (int j = 0; j < areaStatus.Count; j++)
			{
				if (areaStatus[j].areaID == _points[index].areaId)
				{
					existingFound = true;
					areaStatus[j].totalPoints++;
				}
			}

			if (!existingFound)
			{
				AreaStatus status = new AreaStatus();
				status.areaID = _points[index].areaId;
				status.actualColor = _points[index].colorIndex;
				status.lastSelectedColor = status.actualColor;
				status.totalPoints++;
				areaStatus.Add(status);
			}
		}
	}

	void FillAreaStatistics(int index)
	{
		if (areaStatus[index].isFilledCompletely) return;
		areaStatus[index].pointsFilled++;
		areaStatus[index].percentageDone =
			Mathf.Clamp01((float) areaStatus[index].pointsFilled / (float) areaStatus[index].totalPoints);
		areaStatus[index].lastSelectedColor = (PointType) GameController.Instance.selectedColorIndex;
		float correctProgPercentage = (float) correctProgress / (float) _points.Length;

		if (!isAuto)
		{
			for (int i = 0; i < percentageChecker.Length; i++)
			{
				if (correctProgPercentage >= percentageChecker[i].value && percentageChecker[i].isCalled == false)
				{
					GameController.Instance.PlayInteracticeParticle();
					percentageChecker[i].isCalled = true;
				}
			}
		}

		if (areaStatus[index].percentageDone >= 1f)
		{
			areaStatus[index].isFilledCompletely = true;
		}

		bool allAreasCompleted = CheckAllAreaCompletion();
		
		if (allAreasCompleted)
		{
			if (!_once)
			{
				_stitchingFinished = true;
				GameController.Instance.uiController.gamePanel.undoButton.SetActive(false);
				GameController.Instance.uiController.gamePanel.referencePicture.SetActive(false);
				GameController.Instance.tckCanvas.SetActive(false);

				transform.GetChild(0).gameObject.SetActive(false);
				GameController.Instance.flipMechanics.RemoveCanvas();
				GameController.Instance.currentLevel.obstaclesContainer.gameObject.SetActive(false);
				
				// if (GameController.Instance.currentLevel.hasOverlocking)
				// {
				// 	GameController.Instance.overlockMachine.Initialize();
				// }
				// else
				// {
				// 	transform.GetChild(0).gameObject.SetActive(false);
				// 	GameController.Instance.flipMechanics.RemoveCanvas();
				// }

				// 	StartCoroutine(WaitForConvert()); // for enabling the flip mechanics
				
			}
			_once = true;
		}
		else
		{
			if (!autoBtn_isActive)
			{
				combinedPercentage = (float) currentProgress / _points.Length;
				if (combinedPercentage >= autofillThresshold)
				{
					if (!_runOnce)
					{
						//autoFillButton.gameObject.SetActive(true);
						_runOnce = true;
					}

				}
			}
			else
			{
				if (areaStatus[index].isFilledCompletely)
				{
					for (int i = 0; i < areaStatus.Count; i++)
					{
						if (!areaStatus[i].isFilledCompletely)
						{
							StartCoroutine(WaitForUselessThing(i));
							continue;
						}
					}
				}
			}
		}
	}
	
	IEnumerator WaitForConvert()
	{
		yield return new WaitForSeconds(0.1f);
		GameController.Instance.uiController.finishPanelComplete.ConvertRendererToTexture();
		EnableFlipFunctionality();
	}

	IEnumerator WaitForUselessThing(int i)
	{
		yield return new WaitForSeconds(0.3f);
		GameController.Instance.selectedColorIndex = (int) areaStatus[i].actualColor;
		GameController.Instance.uiController.gamePanel.SelectColor((int) areaStatus[i].actualColor);
		currentColorId = (int) areaStatus[i].actualColor;
		_currentAreaId = areaStatus[i].areaID;
	}

	bool CheckAllAreaCompletion()
	{
		int counter = 0;
		for (int i = 0; i < areaStatus.Count; i++)
		{
			counter += areaStatus[i].isFilledCompletely ? 1 : 0;
		}

		return counter == areaStatus.Count;
	}
	
	[BoxGroup("FlippingMechanics"),SerializeField] public FlipMechanics flipMechanic;
	[BoxGroup("FlippingMechanics"),SerializeField]public bool useFlippingMechanics = false;


	public void SetFlippingMechanics(bool value)
	{
		useFlippingMechanics = value;
	}

	public void EnableFlipFunctionality()
	{
		GameController.Instance.CloseGamePanel();
		GameController.Instance.currentLevel.gameObject.transform.GetChild(2).gameObject.SetActive(false);

		flipMechanic.enabled = true;
		flipMechanic.Initialize();
		enabled = false;
	}
}