﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using TouchControlsKit;
using UnityEngine.UI;

public class ColorControlMover : MonoBehaviour
{
	public Slider sensitivitySlider;
	public Text sliderValue;
	public LayerMask zoneMask;
	public LayerMask layerMask;

	private Area   _hitObject;
	private Area[] _allAreas;


	public ParticleSystem undoBurst;
	bool                  _autoMove = false;
	bool                  once;


	private Vector3 mOffset;
	private float   mZCoord;
	const   float   minHeight = 0.2f;

	public bool restrictAreaMovement = false;
	public bool canMove              = false;

	private int selectedAreaID;


	float minX;
	float maxX;
	float minY;
	float maxY;

	[SerializeField] internal Area  selectedArea;
	[SerializeField] internal Area  detectedArea;
	[SerializeField]          float detectionTime  = 2;
	[SerializeField]          float _DetectionTime = 0;

	public GameObject worldCanvas;
	public Vector3    canvasOffset;
	public Image      fillbars;

	private                           Vector3 previousPosition;
	[Range(0f, 100f), SerializeField] float   sensitivity = 20f;


	[SerializeField] private Vector3 angleOne;
	[SerializeField] private Vector3 angleTwo;


	float particlesSpawnTime = 0.5f;
	float _particlesSpawnTime = 0f;
	

	// private void Start()
	// {
	// 	_particlesSpawnTime = 0f;
	// 	worldCanvas.SetActive(false);
	// 	fillbars.fillAmount = 0f;
	// 	_DetectionTime      = 0f;
	// 	_allAreas           = FindObjectsOfType<Area>();
	// 	GameController.Instance.uiController.gamePanel.levelProgressbar.SetActive(false);
	// 	canMove = true;
	// 	GameController.Instance.uiController.gamePanel.undoButton.GetComponent<Button>().onClick.AddListener(() => { DeleteSelectedAreaStitches(); });
	// 	var emission = GameController.Instance.machineParticles.emission;
	// 	emission.enabled                                                     = false;
	// 	//GameController.Instance.colorFollower.GetComponent<Collider>().enabled = false;
	// }
	//
	// public void EnableAutoMovement()
	// {
	// 	_autoMove = true;
	// }
	//
	// const string tutKey = "Tutorial";
	//
	// void Update()
	// {
	// 	sliderValue.text = sensitivitySlider.value.ToString();
	// 	
	// 	if (PlayerPrefs.GetFloat(tutKey) == 0)
	// 	{
	// 		return;
	// 	}
	// 	
	// 	UpdatePosition(Vector3.zero);
	// 	
	// 	fillbars.color                 = GameController.Instance.stichMaterials[GameController.Instance.selectedColorIndex].color;
	// 	worldCanvas.transform.position = transform.position + canvasOffset;
	// 	if (!GameController.Instance.colorFollower.doneStitching && GameController.Instance.uiController.settingsPanel.localScale == Vector3.zero)
	// 	{
	// 		if (canMove)
	// 		{
	// 			if (Input.GetMouseButtonDown(0))
	// 			{
	// 				var mainModule = GameController.Instance.machineParticles.main;
	// 				var startColor = mainModule.startColor;
	// 				startColor.color      = fillbars.color;
	// 				mainModule.startColor = startColor;
	// 				GetOffsetFromInput();
	// 				//GameController.Instance.colorFollower.GetComponent<Collider>().enabled = true;
	// 				GameController.Instance.uiController.gamePanel.levelProgressbar.SetActive(true);
	// 				selectedAreaID = GameController.Instance.currentAreaID;
	// 			}
	//
	// 			if (Input.GetMouseButton(0) && GameController.Instance.colorFollower.move == false)
	// 			{
	// 				Vector3 difference = Input.mousePosition - previousPosition;
	// 				difference = new Vector3(difference.x, 0f, difference.y);
	// 				difference = difference.normalized;
	// 				Vector3 nextPosition = GetMouseAsWorldPoint() + mOffset; 
	// 				
	// 				Vector3      upWardPos = nextPosition + Vector3.up;
	// 				RaycastHit[] hitInfo;
	// 				hitInfo = Physics.RaycastAll(_newPos, Vector3.down, 10f,layerMask);
	// 				if (hitInfo.Length > 0)
	// 				{
	// 					if (!selectedArea)
	// 					{
	// 						//GameController.Instance.colorFollower.GetComponent<Collider>().enabled = false;
	// 						int counter = 0;
	// 						for (int i = 0; i < hitInfo.Length; i++)
	// 						{
	// 							if (hitInfo[i].collider.gameObject.layer == LayerMask.NameToLayer("Zone"))
	// 							{
	// 								var area = hitInfo[i].collider.GetComponent<Area>();
	// 								if (GameController.Instance.colorFollower.IsAreaFilled(area.areaId))
	// 								{
	// 									return;
	// 								}
	// 								
	// 								counter = 1;
	// 								if (!detectedArea)
	// 								{
	// 									detectedArea = hitInfo[i].collider.GetComponent<Area>();
	// 								}
	// 								else if (detectedArea != hitInfo[i].collider.GetComponent<Area>())
	// 								{
	// 									_DetectionTime = 0;
	// 									detectedArea   = hitInfo[i].collider.GetComponent<Area>();
	// 								}
	//
	// 								for (int j = 0; j < _allAreas.Length; j++)
	// 								{
	// 									if (_allAreas[j] != detectedArea)
	// 									{
	// 										_allAreas[j].HightLight(false);
	// 									}
	// 									else
	// 									{
	// 										_allAreas[j].HightLight(true);
	// 									}
	// 								}
	//
	// 								_DetectionTime += Time.deltaTime;
	// 								worldCanvas.SetActive(true);
	// 								fillbars.fillAmount = _DetectionTime / detectionTime;
	// 								if (_DetectionTime >= detectionTime)
	// 								{
	// 									selectedArea   = hitInfo[i].collider.GetComponent<Area>();
	// 									detectedArea   = selectedArea;
	// 									_DetectionTime = 0;
	// 									selectedAreaID = GameController.Instance.currentAreaID = GameController.Instance.colorFollower._currentAreaId = selectedArea.areaId;
	// 									for (int j = 0; j < _allAreas.Length; j++)
	// 									{
	// 										if (_allAreas[j] != selectedArea)
	// 										{
	// 											_allAreas[j].HightLight(false);
	// 										}
	// 										else
	// 										{
	// 											_allAreas[j].HightLight(true);
	// 										}
	// 									}
	// 								}
	//
	// 								nextPosition.y = minHeight;
	// 								// transform.position = nextPosition;
	// 								//UpdatePosition(nextPosition);
	// 							}
	// 							else
	// 							{
	// 								nextPosition.y = minHeight;
	// 								// transform.position = nextPosition;
	// 								//UpdatePosition(nextPosition);
	// 							}
	// 						}
	//
	// 						if (counter == 0)
	// 						{
	// 							if (detectedArea)
	// 							{
	// 								detectedArea.HightLight(false);
	// 							}
	//
	// 							_DetectionTime = 0f;
	// 							worldCanvas.SetActive(false);
	// 						}
	// 					}
	// 					else
	// 					{
	// 						worldCanvas.SetActive(false);
	// 						nextPosition.y = minHeight;
	// 						//UpdatePosition(nextPosition);
	// 						//GameController.Instance.colorFollower.GetComponent<Collider>().enabled = true;
	//
	// 						int counter = 0;
	// 						for (int i = 0; i < hitInfo.Length; i++)
	// 						{
	// 							if (hitInfo[i].collider.gameObject.layer == LayerMask.NameToLayer("Zone"))
	// 							{
	// 								if (hitInfo[i].collider.GetComponent<Area>().areaId == GameController.Instance.colorFollower._currentAreaId)
	// 								{
	// 									counter++;
	// 								}
	// 							}
	// 						}
	//
	// 						if (counter ==  0)
	// 						{
	// 							if (_particlesSpawnTime >= particlesSpawnTime)
	// 							{
	// 								var emission = GameController.Instance.machineParticles.emission;
	// 								emission.enabled = true;
	// 								MMVibrationManager.Haptic(HapticTypes.Warning, true, false);
	// 							}
	// 							else
	// 							{
	// 								_particlesSpawnTime += Time.deltaTime;
	// 							}
	// 						}
	// 						else
	// 						{
	// 							_particlesSpawnTime = 0f;
	// 							var emission = GameController.Instance.machineParticles.emission;
	// 							emission.enabled = false;
	// 						}
	// 					}
	//
	// 					if (hitInfo[0].collider.gameObject.layer == LayerMask.NameToLayer("Zone"))
	// 					{
	// 						sensitivitySlider.value = 0.3f;
	// 					}
	// 					else
	// 					{
	// 						GameController.Instance._machineScript.SetAnimation(false);
	// 						sensitivitySlider.value = 0.03f;
	// 					}
	// 				}
	// 				else
	// 				{
	// 					GetOffsetFromInput();
	// 				}
	//
	// 				Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
	// 				pos.x = Mathf.Clamp(pos.x, 0.05f, 0.95f);
	// 				if (pos.x > 0)
	// 				{
	// 					targetAngle = Vector3.Lerp(angleOne, angleTwo, pos.x);
	// 				}
	// 				else if (pos.x < 0)
	// 				{
	// 					targetAngle = Vector3.Lerp(angleOne, angleTwo, pos.x);
	// 				}
	//
	// 				transform.GetChild(0).localRotation = Quaternion.Slerp(transform.GetChild(0).localRotation, Quaternion.Euler(targetAngle), Time.deltaTime * 5f);
	// 			}
	// 		}
	// 		else
	// 		{
	// 			GetOffsetFromInput();
	// 		}
	//
	// 		previousPosition = Input.mousePosition;
	// 		if (Input.GetKeyUp(KeyCode.Mouse0))
	// 		{
	// 			var emission = GameController.Instance.machineParticles.emission;
	// 			emission.enabled    = false;
	// 			_particlesSpawnTime = 0f;
	// 			worldCanvas.SetActive(false);
	// 			_DetectionTime = 0;
	// 			detectedArea   = selectedArea = null;
	// 			GameController.Instance.colorFollower.SetActionData(selectedAreaID);
	// 			if (!GameController.Instance.uiDetector.isOverUI)
	// 			{
	// 				GameController.Instance.uiController.gamePanel.levelProgressbar.SetActive(false);
	// 				//GameController.Instance.colorFollower.GetComponent<Collider>().enabled = false;
	// 				if (!GameController.Instance.colorFollower.finished)
	// 				{
	// 					GameController.Instance.colorFollower.CheckAllWaypointsDone();
	// 				}
	// 			}
	// 		}
	// 	}
	// }
	//
	// [SerializeField] Vector3 targetAngle;
	// [SerializeField] private float _minX, _maxX, _minY, _maxY;
	// private Vector3 _newPos;
	// private void UpdatePosition(Vector3 nextPos)
	// {
	// 	// Vector3 pos = Camera.main.WorldToViewportPoint(nextPos);
	// 	// pos.x   = Mathf.Clamp(pos.x, 0.05f, 0.95f);
	// 	// pos.y   = Mathf.Clamp(pos.y, 0.15f, 0.85f);
	// 	// nextPos = Camera.main.ViewportToWorldPoint(pos);
	// 	//transform.position = nextPos;
	//
	// 	// var zAxis = TCKInput.GetAxis("Touch", "Vertical");
	// 	// var xAxis = TCKInput.GetAxis("Touch", "Horizontal");
	// 	// _newPos = new Vector3(xAxis, 0, zAxis);
	// 	// _newPos *= sensitivitySlider.value;
	// 	// _newPos += transform.position;
	// 	// _newPos.x = Mathf.Clamp(_newPos.x, _minX, _maxX);
	// 	// _newPos.z = Mathf.Clamp(_newPos.z, _minY, _maxY);
	// 	// transform.position = _newPos;
	// }
	//
	// private void GetOffsetFromInput()
	// {
	// 	mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
	//
	// 	// Store offset = gameobject world pos - mouse world pos
	// 	mOffset          = gameObject.transform.position - GetMouseAsWorldPoint();
	// 	previousPosition = Input.mousePosition;
	// }
	//
	// private Vector3 GetMouseAsWorldPoint()
	// {
	// 	// Pixel coordinates of mouse (x,y)
	// 	Vector3 mousePoint = Input.mousePosition;
	//
	// 	// z coordinate of game object on screen
	// 	mousePoint.z = mZCoord;
	//
	// 	// Convert it to world points
	// 	return Camera.main.ScreenToWorldPoint(mousePoint);
	// }
	

}