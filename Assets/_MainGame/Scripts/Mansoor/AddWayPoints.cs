﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWayPoints : MonoBehaviour
{

	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
			obj.transform.parent     = transform;
			obj.transform.localScale = Vector3.one * 0.07336711f;

			Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			position.y                  = 0.1f;
			obj.transform.localPosition = position;
		}
	}

}
