﻿
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour
{
    [SerializeField] LoadingScript loader;
    [SerializeField] List<Sprite> LevelIcons;
    [SerializeField] Sprite lockImage;
    [SerializeField] Sprite makeImage;
    [SerializeField] GameObject Parent;
    [SerializeField] GameObject LevelPrefab;
    [SerializeField] RectTransform contentRect;
    [SerializeField, Range(0f, 2f)] float elementHeight;
    [SerializeField, Range(100, 250)] int elementDistance = 233;
    [SerializeField] float heightDifference;
    [SerializeField] float floorHeight;
    [SerializeField] float contentHeight;


[Button]
    void Start()
    {
        int maxLevel = PlayerPrefs.GetInt("Level");
        int counter = -1;


        LevelIcons.Clear();

        for (int i = 0; i < DataCarrier.Instance.levelsData.Count; i++)
        {
            if (DataCarrier.Instance.levelsData[i].imageSprite)
            {
                LevelIcons.Add(DataCarrier.Instance.levelsData[i].imageSprite);
            }
            else
            {
                LevelIcons.Add(DataCarrier.Instance.levelsData[i].matchSprite);
            }
        }

        for (int i = 0; i < LevelIcons.Count; i++)
        {

            int index = i;

            GameObject Go = Instantiate(LevelPrefab, Parent.transform);
            Go.transform.localScale = Vector3.one * elementHeight;
            GameObject desiredChild = Go.transform.GetChild(2).gameObject;

            desiredChild.GetComponent<Image>().sprite = LevelIcons[i];
            desiredChild.GetComponent<Button>().onClick.AddListener(() => StartLevel(index));


            int value = i % 3;
            
            
            
            if (value == 0)
            {
                counter++;
                //Go.transform.GetChild(0).gameObject.SetActive(true);
            
            
            }
            else
            {
                Go.transform.GetChild(0).gameObject.SetActive(false);
            }

            Vector3 pos = Go.GetComponent<RectTransform>().localPosition;
            pos.x = (value - 1) * elementDistance;
            pos.y = (-counter * floorHeight) - heightDifference;
            Go.GetComponent<RectTransform>().localPosition = pos;

            if (i == maxLevel)
            {
                desiredChild.GetComponent<Image>().sprite = makeImage;
            }
            else
            {
                desiredChild.GetComponent<Image>().sprite = LevelIcons[i];
            }

            if (i > maxLevel)
            {
                desiredChild.GetComponent<Image>().sprite = lockImage;
                desiredChild.GetComponent<Button>().enabled = false;
            }


        }

        Vector3 contentPos = contentRect.sizeDelta;
        contentPos.y = counter * contentHeight;
        contentRect.sizeDelta = contentPos;

        var scrollValue = PlayerPrefs.GetInt("Level");
        scrollValue = scrollValue / 12;
        scrollValue = scrollValue * 860;
        contentRect.anchoredPosition = new Vector3(0, scrollValue, 0);
    }

    void StartLevel(int levelIndex)
    {
        gameObject.SetActive(false);
        loader.PlayButtonClick(levelIndex);
    }

}
