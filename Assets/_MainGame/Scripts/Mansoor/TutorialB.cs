﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TutorialB : MonoBehaviour
{

	public GameObject panelParents;
	public GameObject dullImage;

	public GameObject chooseColor;
	public GameObject chooseStitch;
	public GameObject dragMachine;
	public GameObject Hand;
	public GameObject[] extraSprites;

	public GameObject stitchPanel;
	public GameObject colorpanel;

	public GameObject lightGreenImg;
	public GameObject greenImg;
	public GameObject ghostMachine;

	bool stepOneDone = false;
	bool stepTwoDone = false;

	bool                 useTutorial;
	public RectTransform rect;
	public Vector2       actualSize;
	public Vector2       targetSize;

	[SerializeField] bool  showHand;
	[SerializeField] float waitTime  = 3f;
	[SerializeField] float _wTime = 0f;
	

	void Awake()
	{
		lightGreenImg.SetActive(false);
		showHand   = true;
		actualSize = rect.sizeDelta;
		targetSize = actualSize * 0.8f;
		CheckTutorialStatus();
	}

	const string tutkey = "Tutorial";
	
	void CheckTutorialStatus()
	{
		if (PlayerPrefs.GetFloat(tutkey, 0) == 1)
		{
			DisableTutorial();
		}
		else
		{
			useTutorial = true;
		}
	}

	void Start()
	{
		if (useTutorial)
		{
			InitTutorial();
		}
	}

	void Update()
	{
		if (PlayerPrefs.GetFloat(tutkey) == 0)
		{
			return;
		}
		
		if (GameController.Instance.isGameFinished)
		{
			Hand.SetActive(false);
			lightGreenImg.SetActive(false);
			return;
		}
		else
		{
			if (!GameController.Instance.startMachineMovement)
			{
				if (showHand)
				{
					_wTime += Time.deltaTime;

					if (_wTime > waitTime)
					{
						_wTime = 0;
						PerformStep1();
					}
				
				}

			}
			else
			{
				_wTime = 0;
			}
		}
		
	}


	void DisableTutorial()
	{
		chooseColor.SetActive(false);
		chooseStitch.SetActive(false);
		dragMachine.SetActive(false);
		Hand.SetActive(false);
		lightGreenImg.SetActive(false);
		greenImg.SetActive(false);
		dullImage.SetActive(false);
	}

	void InitTutorial()
	{
		PerformStep2();
	}

	[Button]
	void PerformStep1()
	{
		// for (int i = 0; i < extraSprites.Length; i++)
		// {
		// 	extraSprites[i].SetActive(true);
		// }
		dragMachine.SetActive(false);
		chooseStitch.SetActive(false);
		chooseColor.SetActive(true);
		// dullImage.transform.SetParent(panelParents.transform);
		chooseColor.transform.SetParent(panelParents.transform);
		// dullImage.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		chooseColor.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		colorpanel.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		stitchPanel.transform.SetSiblingIndex(0);
		for (int i = 0; i < GameController.Instance.uiController.gamePanel.colorButtons.Length; i++)
		{
			if (i == GameController.Instance.selectedColorIndex)
			{
				Hand.SetActive(true);
				rect.SetParent(GameController.Instance.uiController.gamePanel.colorButtons[i].transform);
				rect.localPosition = new Vector3(-150, -180, 0);
				actualSize         = rect.sizeDelta;
				rect.sizeDelta     = targetSize;
				rect.DOSizeDelta(actualSize, 0.5f).SetLoops(-1, LoopType.Yoyo);
			}

			int myButtonIndex = i;
			
		}
	}

	public void RemoveTutorial()
	{
		DOTween.Kill(rect);
		rect.sizeDelta = actualSize;
		chooseColor.SetActive(false);
		Hand.SetActive(false);
	}


	[Button]
	public void PerformStep2()
	{
		_wTime = 0;
		dragMachine.SetActive(false);
		chooseColor.SetActive(false);
		chooseStitch.SetActive(true);
		chooseStitch.transform.SetParent(panelParents.transform);
		// dullImage.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		chooseStitch.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		stitchPanel.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		colorpanel.transform.SetSiblingIndex(0);
		for (int i = 0; i < GameController.Instance.uiController.gamePanel.stitchButtons.Length; i++)
		{
			if (i == GameController.Instance.selectedStichIndex)
			{
				Hand.SetActive(true);
				rect.SetParent(GameController.Instance.uiController.gamePanel.stitchButtons[i].transform);
				rect.localPosition = new Vector3(-200, -360, 0);
				actualSize         = rect.sizeDelta;
				rect.sizeDelta     = targetSize;
				rect.DOSizeDelta(actualSize, 0.5f).SetLoops(-1, LoopType.Yoyo);
			}

			int myButtonIndex = i;
			GameController.Instance.uiController.gamePanel.stitchButtons[i].GetComponent<Button>().onClick.AddListener(() =>
			{
				if (!stepTwoDone)
				{
					DOTween.Kill(rect);
					rect.DOSizeDelta(actualSize, 0.5f);
					rect.transform.SetParent(transform);
					
					
						lightGreenImg.SetActive(true);
						lightGreenImg.transform.SetParent(transform);
						lightGreenImg.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);
						lightGreenImg.transform.SetSiblingIndex(Hand.transform.GetSiblingIndex() - 1);
						
						lightGreenImg.GetComponent<Image>().enabled = false;
						greenImg.GetComponent<Image>().enabled      = false;
						
						rect.transform.SetParent(lightGreenImg.transform);
						rect.anchoredPosition = new Vector2(-150, -200);
						//ghostMachine.SetActive(true);
						StartCoroutine(CheckForMouseUp());
					
					
					// rect.transform.DOLocalMove(Vector3.zero, 1f).SetDelay(0.25f).OnComplete(() =>
					// {
					// 	lightGreenImg.SetActive(true);
					// 	lightGreenImg.transform.parent = transform;
					// 	lightGreenImg.transform.SetSiblingIndex(Hand.transform.GetSiblingIndex() - 1);
					// 	greenImg.GetComponent<RectTransform>().DOSizeDelta(Vector3.zero, 1f).SetLoops(-1, LoopType.Restart);
					// 	rect.GetComponent<RectTransform>().DOSizeDelta(targetSize, 1f).SetLoops(-1, LoopType.Restart);
					// 	StartCoroutine(CheckForMouseUp());
					// });
					stepTwoDone = true;
					PerformStep1();
				}
			});
		}
	}

	IEnumerator CheckForMouseUp()
	{
		yield return new WaitUntil(() => (Input.GetMouseButtonDown(0)));
		PlayerPrefs.SetFloat("Tutorial", 1);
		DOTween.Kill(rect);
		DOTween.Kill(greenImg);
		greenImg.GetComponent<RectTransform>().sizeDelta = new Vector2(128f, 128f);
		DisableTutorial();
		showHand = true;
		ghostMachine.SetActive(false);
		for (int i = 0; i < extraSprites.Length; i++)
		{
			extraSprites[i].SetActive(false);
		}
	}


	[Button]
	public void PerformStep3()
	{
		chooseColor.SetActive(false);
		chooseStitch.SetActive(false);
		dragMachine.SetActive(true);
		// dullImage.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		chooseStitch.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		stitchPanel.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		colorpanel.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
	}


	[Button]
	public void PerformStep3ForInActivity()
	{
		showHand = false;

		chooseColor.SetActive(false);
		chooseStitch.SetActive(false);
		dragMachine.SetActive(true);

		rect.transform.localPosition = Vector3.zero;
		Hand.SetActive(true);


		// dullImage.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		chooseStitch.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		stitchPanel.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);
		colorpanel.transform.SetSiblingIndex(stitchPanel.transform.parent.childCount);


		lightGreenImg.SetActive(true);
		lightGreenImg.transform.SetParent(transform);
		lightGreenImg.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
		lightGreenImg.transform.SetSiblingIndex(Hand.transform.GetSiblingIndex() - 1);

		lightGreenImg.GetComponent<Image>().enabled = false;
		greenImg.GetComponent<Image>().enabled = false;

		rect.transform.SetParent(lightGreenImg.transform);
		rect.anchoredPosition = new Vector2(-150, -200);
		ghostMachine.SetActive(true);
		StartCoroutine(CheckForMouseUp());

	}


}