﻿
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class WaypointMover : MonoBehaviour
{

    public float        speed = 10f;
    
    public GameObject[] wayPoint;
    public int          length;
    public int          currIndex;
    bool                stop;
    
    public GameObject   uiImage;
    public GameObject   uiImage2;
    Camera              mainCmaera;
    // Start is called before the first frame update


    // Update is called once per frame

    void Start()
    {
        mainCmaera         = Camera.main;
        length             = wayPoint.Length;
        currIndex          = 0;
        transform.position = wayPoint[currIndex].transform.position;
    }

    void Update()
    {
        if(stop)
            return;
        
        transform.position = Vector3.MoveTowards(transform.position, wayPoint[currIndex].transform.position, Time.deltaTime * speed);
        
        
        Vector2 viewportPoint = Camera.main.WorldToViewportPoint(transform.position); //convert game object position to VievportPoint
 
        // set MIN and MAX Anchor values(positions) to the same position (ViewportPoint)
        uiImage.GetComponent<RectTransform>().anchorMin = viewportPoint;  
        uiImage.GetComponent<RectTransform>().anchorMax  = viewportPoint; 
        
        
        // Vector3 pos = mainCmaera.WorldToViewportPoint(transform.position);
        //
        // uiImage.transform.position = mainCmaera.ViewportToWorldPoint(pos);
        
        
        if (Vector3.SqrMagnitude(transform.position - wayPoint[currIndex].transform.position) == 0)
        {
            currIndex++;
            if (currIndex == length)
            {
               
                if (!stop)
                {
                    stop                                  = true;
                    uiImage.GetComponent<Image>().enabled = true;
                    uiImage2.gameObject.SetActive(true);

                    uiImage.GetComponent<RectTransform>().DOSizeDelta(new Vector2(156f, 156f), 1f).From(new Vector2(128f, 128f));
                    
                    uiImage2.GetComponent<RectTransform>().DOSizeDelta(Vector3.zero, 1f).From(new Vector2(128f, 128f)).OnComplete(() =>
                    {
                        stop                                                        = false;
                        currIndex                                                   = 0;
                        uiImage.GetComponent<Image>().enabled                       = false;
                        uiImage2.gameObject.SetActive(false);
                        
                    });

                }
            }
        }
    }

}
