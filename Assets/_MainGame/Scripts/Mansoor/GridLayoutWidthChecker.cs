﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridLayoutWidthChecker : MonoBehaviour
{

    [SerializeField] GridLayoutGroup LayoutGroup;
    [SerializeField] float constantSize = 240;
    [SerializeField] float           constantWidth = 1080;
    [SerializeField] float           screenWidth;

    [SerializeField] float difference;
    [SerializeField] float percentage;
    // Start is called before the first frame update
    void Start()
    {
        screenWidth = Screen.width;
    }

    // Update is called once per frame
    void Update()
    {
        screenWidth = Screen.width;
        difference  = constantWidth - screenWidth;
        difference  = difference / 1000;
        Vector2 size = LayoutGroup.cellSize;
        size.x               = constantSize - (constantSize * difference);
        LayoutGroup.cellSize = size;
    }
}
