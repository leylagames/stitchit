﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotion : MonoBehaviour
{

	[SerializeField] TouchInput touchData;
	[SerializeField] float      posSmoothness;
	[SerializeField] float      rotSmoothness;

	[SerializeField] Vector2 posXRange;
	[SerializeField] Vector2 posYRange;
	[SerializeField] Vector2 posZRange;
	[SerializeField] Vector2 rotXRange;
	[SerializeField] Vector2 rotYRange;
	[SerializeField] Vector2 rotZRange;


	[SerializeField] bool UsePosX;
	[SerializeField] bool UsePosY;
	[SerializeField] bool UsePosZ;
	[SerializeField] bool UseRotX;
	[SerializeField] bool UseRotY;
	[SerializeField] bool UseRotZ;


	Action PosX;
	Action PosY;
	Action PosZ;
	Action RotX;
	Action RotY;
	Action RotZ;


	Vector3 myPosition;
	Vector3 myRotation;

	// Start is called before the first frame update
	void Start()
	{
		if (UsePosX)
		{
			PosX += HandlePosX;
		}

		if (UsePosY)
		{
			PosY += HandlePosY;
		}

		if (UsePosZ)
		{
			PosZ += HandlePosZ;
		}

		if (UseRotX)
		{
			RotX += HandleRotX;
		}

		if (UseRotY)
		{
			RotY += HandleRotY;
		}

		if (UseRotZ)
		{
			RotZ += HandleRotZ;
		}
	}

	// Update is called once per frame
	void LateUpdate()
	{
		myPosition = transform.position;
		
		PosX?.Invoke();
		PosY?.Invoke();
		PosZ?.Invoke();

		// clamping here
		
		myPosition.x       = Mathf.Clamp(myPosition.x, posXRange.x, posXRange.y);
		myPosition.y       = posYRange.y;
		myPosition.z       = Mathf.Clamp(myPosition.z, posZRange.x, posZRange.y);
		transform.position = myPosition;
		
		RotX?.Invoke();
		RotY?.Invoke();
		RotZ?.Invoke();
	}


	void HandlePosX()
	{
		myPosition         -= (transform.right * (touchData.DeltaChange.x * Time.deltaTime * posSmoothness));
	}

	void HandlePosY()
	{
	
	}

	void HandlePosZ()
	{
		Vector3 value = transform.up * (touchData.DeltaChange.y * Time.deltaTime * posSmoothness);
		myPosition         -= value;
	}

	void HandleRotX()
	{
		Vector3 deltaChange          =  transform.TransformDirection(touchData.DeltaChange);
		myRotation   =  transform.eulerAngles;
		myRotation.x += touchData.DeltaChange.x * Time.deltaTime * rotSmoothness;
		
		if (myRotation.x > 180f)
		{
			myRotation.x = myRotation.x - 360f;
		}

		myRotation.x = Mathf.Clamp(myRotation.x, rotXRange.x, rotXRange.y);
		float currentRot = myRotation.x;
		if (currentRot < 0)
		{
			currentRot += 360;
		}

		myRotation.x       = currentRot;
		transform.rotation = Quaternion.Euler(myRotation);
	}

	void HandleRotY()
	{
		myRotation   =  transform.eulerAngles;
		myRotation.y += touchData.DeltaChange.y * Time.deltaTime * rotSmoothness;
		if (myRotation.y > 180f)
		{
			myRotation.y = myRotation.y - 360f;
		}

		myRotation.y = Mathf.Clamp(myRotation.y, rotYRange.x, rotYRange.y);
		float currentRot = myRotation.y;
		if (currentRot < 0)
		{
			currentRot += 360;
		}

		myRotation.y          = currentRot;
		transform.eulerAngles = myRotation;
	}

	void HandleRotZ()
	{
		// copy paste and use DeltaChange X or Y according to your needs
	}


	void OnDisable()
	{
		PosX -= HandlePosX;
		PosY -= HandlePosY;
		PosZ -= HandlePosZ;
		RotX -= HandleRotX;
		RotY -= HandleRotY;
		RotZ -= HandleRotZ;
	}

	private void OnEnable()
	{
		Start();
	}
}