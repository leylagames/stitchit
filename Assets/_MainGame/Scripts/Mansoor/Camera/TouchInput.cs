﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using UnityEngine;


public class TouchInput : MonoBehaviour
{


	// <- Private Variables ->
	Vector2 prevPosition;
	Vector2 deltaChange;

	public Vector2 DeltaChange => deltaChange;

	public bool canDrag = false;

	const int Zero = 0;

	void Update()
	{
		if (canDrag)
		{
			HandleDrag();
		}
	}
	
	void HandleDrag()
	{
		if (Input.GetMouseButtonDown(Zero))
		{
			prevPosition = Input.mousePosition;
		}


		if (Input.GetMouseButton(Zero))
		{
			Vector2 currentPos = Input.mousePosition;
			deltaChange  = currentPos - prevPosition;
			prevPosition = currentPos;

		}

		if (Input.GetMouseButtonUp(Zero))
		{
			prevPosition = Input.mousePosition;
			deltaChange  = Vector2.zero;
		}
	}

	public void CanDrag(bool value)
	{
		canDrag = value;
	}
}