﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class ClothMechanics : MonoBehaviour
{
	
	public enum Position
	{

		Top,
		Bottom,
		purse
	}

	[System.Serializable]
   public class ClothData
   {

	   public Transform targetCloth;
	   public Position  position;
	   public Vector2   minPos;
	   public Vector2   maxPos;
	   public GameObject   Quad;
	   
	   [Button]
	   public void SetMinPosition()
	   {
		   minPos = Quad.transform.localPosition;
	   }
	   
	   [Button]
	   public void SetMaxPosition()
	   {
		   maxPos = Quad.transform.localPosition;
	   }

   }

   public DragAndPinch dragAndPinch;
   public ClothData[]  clothData;
   
   public Position targetPosition;
   [SerializeField,ReadOnly]ClothData       targetCloth;
   
   // public MeshDecal   meshDecal;

    // Start is called before the first frame update
    public void Init()
    {
	    for (int i = 0; i < clothData.Length; i++)
	    {
		    clothData[i].Quad.SetActive(false);
	    }
	    
	    
	    for (int i = 0; i < clothData.Length; i++)
	    {
		    if (targetPosition == clothData[i].position)
		    {
			    targetCloth = clothData[i];
			    targetCloth.Quad.SetActive(true);
			    GameController.Instance.uiController.finishPanelComplete._secondaryCamera = targetCloth.Quad.transform.GetChild(0).GetComponent<Camera>();
		    }
	    }
    }

    // Update is called once per frame
    void Update()
    {
	    Vector3 pos = targetCloth.Quad.transform.localPosition;
	    pos.x                                        = Mathf.Lerp(targetCloth.minPos.x, targetCloth.maxPos.x, dragAndPinch.lerpedValue.x);
	    pos.y                                        = Mathf.Lerp(targetCloth.minPos.y, targetCloth.maxPos.y, dragAndPinch.lerpedValue.y);
	    targetCloth.Quad.transform.localPosition = pos;
    }
}
