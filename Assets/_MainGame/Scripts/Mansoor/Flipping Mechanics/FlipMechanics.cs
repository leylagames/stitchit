﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class FlipMechanics : MonoBehaviour
{

	public enum States
	{
		None,
		Shaver,
		Glue,
		Finished
	}

	[System.Serializable]
	public struct irregularStitch
	{
		public GameObject spawnStitches;
		public List<GameObject> stitches;
	}


	[System.Serializable]
	public class ClothDetails
	{
		public GameObject clothObject;
		public GameObject[] patchPositions;
		public SpriteRenderer[] dullSprites;
		public Vector3 cameraPosition;
	}

	Vector3 previousPos;
	float _clickTimer = 0f;
	public float touchSensitivity = 0.15f;
	public DOTweenAnimation woodenBG;
	public LayerMask cameraNewLayerMask;
	[Header("State")] public States currentState = States.None;
	public ColorControlFollower colorControlFollower;
	public DragAndPinch dragAndPinch;
	[Header("Gameplay")] public GameObject camera;
	public GameObject canvas;
	public GameObject level;
	public GameObject stitchesParent;
	public GameObject IrregularstitchesParent;
	public List<irregularStitch> irregularStitches;
	public Transform shaverMachine;
	public Transform[] shaverMachineModels;
	public Transform stitchMachine;
	public int machineIndex;
	public Transform cameraPositionForShaving;
	public Transform cameraPositionForCloth;
	public ClothDetails[] clothDetails;
	public Transform screenShotCamera;
	public AudioSource shaverSound;
	public Animation shaverAnimation;
	public ParticleSystem shaverParticle;

	[Header("Particles")] public ParticleSystem undoParticles;
	public MeshRenderer undoParticlesRenderer;
	public ParticleSystem undoBurst;

	[Header("UI")] public GameObject abovePanel;
	[Header("UI")] public GameObject toolsPanel;
	public GameObject machinePanel;
	public GameObject machineButton;
	public GameObject[] shaverLocks, clothLocks;


	[Header("Glue Stuff")] public Transform glueMachine;
	public Transform glueObject;
	public SkinnedMeshRenderer skinnedMeshRenderer;
	[SerializeField] Vector3 initalPos;
	[SerializeField] Vector3 targetPos;
	[SerializeField] Vector3 currPos;
	[SerializeField] float blenshapeInitialValue;
	[SerializeField] float blenshapeFinalValue;
	[SerializeField] float blendShapeValue;
	[SerializeField, Range(-0.25f, 1)] float transition = 0f;
	[SerializeField, Range(0, 10)] float increaseSpeed = 0f;

	[Header("Movement")] [SerializeField] bool canMove = false;
	[SerializeField] float moveSpeed = 10f;
	[SerializeField] float sqrMag = 0f;
	[SerializeField] float sqrMagThreshold = 0.01f;
	[SerializeField] float rotSpeed = 10f;
	[SerializeField] float heightAddition = 0.2f;

	[SerializeField] float _spawnTime = 0f;
	[SerializeField] float spawnTime = 0.5f;
	[SerializeField] float dropSpeed = 2;

	public float firstValue, secondValue;

	public GameObject[] frames;

	int _counter = 0;
	bool _isDone = false;

	void Start()
	{
		UpdateLocks();
		PlayerPrefs.SetInt("Cloth0", 1);
	}

	public void Initialize()
	{
		GetComponent<BoxCollider>().enabled = true;
		level = GameController.Instance.currentLevel.gameObject;
		canvas = frames[PlayerPrefs.GetInt("FrameTypeSelected")];
		stitchesParent.transform.parent = canvas.transform;
		IrregularstitchesParent.transform.parent = canvas.transform;
		level.transform.parent = canvas.transform;

		transform.DOMoveZ(10f, 1f).SetDelay(1f).OnComplete(() =>
		{
			camera.transform.DOMove(cameraPositionForShaving.position, 0.5f);
			camera.transform.DOLocalRotate(new Vector3(50, 0, 0), 0.5f);
			canvas.transform.DOLocalRotate(new Vector3(1.75f, 0, 180), 1.5f).SetDelay(0.25f).OnComplete(() =>
			{
				stitchMachine.gameObject.SetActive(false);
				shaverMachine.gameObject.SetActive(true);
				Vector3 desirePos = irregularStitches[0].spawnStitches.transform.position;
				desirePos.y = desirePos.y + heightAddition;

				transform.DOMove(desirePos, 0.75f).SetDelay(0.5f).OnComplete(() =>
				{
					SetMachineButton();
					Vector3 dir = transform.position - desirePos;
					dir = dir.normalized;
					Quaternion lookAt = Quaternion.LookRotation(dir);
					transform.rotation = lookAt;
					currentState = States.Shaver;
					transition = 0;
					GameController.Instance.colorFollower.thisMovementMode = ColorControlFollower.MovementMode.Manual;
					firstValue = irregularStitches.Count * 0.4f;
					secondValue = irregularStitches.Count * 0.8f;
				});
			});
		});

		undoParticles = Instantiate(undoBurst);
		undoParticles.gameObject.transform.localScale = Vector3.one * 0.65f;
		ParticleSystem.MainModule undoParticlesMain = undoParticles.main;
		undoParticlesMain.playOnAwake = false;
		undoParticlesMain.simulationSpace = ParticleSystemSimulationSpace.World;
	}

	private void SetMachineButton()
	{
		machinePanel.SetActive(true);
		if (!PlayerPrefs.HasKey("TutorialShaversIntro"))
		{
			GameController.Instance.tutorial.StartCoroutine("ShowTutorialShaversIntro");
		}
		else if (GameManager.Instance.CurrentLevel == GameController.Instance.tutorial.tutorialShaversLevel &&
		         !PlayerPrefs.HasKey("TutorialShavers"))
		{
			GameController.Instance.tutorial.StartCoroutine("ShowTutorialShavers");
		}

		EventTrigger eventTrigger = machineButton.AddComponent<EventTrigger>();
		EventTrigger.Entry entry = new EventTrigger.Entry();
		entry.eventID = EventTriggerType.PointerEnter;
		entry.callback.AddListener(arg0 => { canMove = true; });
		eventTrigger.triggers.Add(entry);
		entry = new EventTrigger.Entry();
		entry.eventID = EventTriggerType.PointerExit;
		entry.callback.AddListener(arg0 => { canMove = false; });
		eventTrigger.triggers.Add(entry);
	}

	void Update()
	{
		switch (currentState)
		{
			case States.None:
			{
				break;
			}

			case States.Shaver:
			{
				switch (colorControlFollower.thisMovementMode)
				{
					case ColorControlFollower.MovementMode.Automatic:
					{
						AutoHandleShaver();
						break;
					}

					case ColorControlFollower.MovementMode.Manual:
					{
						if (!_isDone)
						{
							ManualHandleShaver();
						}

						break;
					}
				}

				break;
			}

			case States.Glue:
			{
				HandleGlue();
				break;
			}

			case States.Finished:
			{
				break;
			}
		}
	}

	void AutoHandleShaver()
	{
		if (canMove && !_isDone)
		{
			Vector3 desirePos = irregularStitches[_counter].spawnStitches.transform.position;
			desirePos.y = desirePos.y + heightAddition;
			transform.position = Vector3.MoveTowards(transform.position, desirePos, Time.deltaTime * moveSpeed);
			sqrMag = Vector3.SqrMagnitude(transform.position - desirePos);
			Vector3 dir = transform.position - desirePos;
			dir = dir.normalized;
			Quaternion lookAt = Quaternion.LookRotation(dir);
			transform.rotation = Quaternion.Slerp(transform.rotation, lookAt, Time.deltaTime * rotSpeed);
			if (sqrMag < sqrMagThreshold)
			{
				undoParticles.transform.position = transform.position - new Vector3(0, heightAddition, 0);
				undoParticles.startColor =
					irregularStitches[_counter].spawnStitches.GetComponent<MeshRenderer>().material.color * 0.8f;
				undoParticles.Emit(Random.Range(7, 11));
				irregularStitches[_counter].spawnStitches.transform.localScale = new Vector3(
					irregularStitches[_counter].spawnStitches.transform.localScale.x, 0.25f,
					irregularStitches[_counter].spawnStitches.transform.localScale.z);

				for (int i = 0; i < irregularStitches[_counter].stitches.Count; i++)
				{
					irregularStitches[_counter].stitches[i].gameObject.SetActive(false);
				}

				_counter++;
				if (_counter == irregularStitches.Count)
				{
					_isDone = true;
					machineButton.SetActive(false);


					toolsPanel.SetActive(false);
					abovePanel.SetActive(false);
					machinePanel.SetActive(false);


					transform.DOMoveZ(10f, 1f).SetDelay(0.15f).OnComplete(() => { FlipCanvasBack(); });
				}
			}
		}
	}

	public float minX, maxX, minY, maxY;

	void ManualHandleShaver()
	{
		if (Input.GetMouseButtonDown(0))
		{
			previousPos = Input.mousePosition;
			if (!PlayerPrefs.HasKey("TutorialShaversIntro"))
			{
				GameController.Instance.tutorial.CompleteTutorialShaversIntro();
			}

			shaverSound.Play();
			if (!shaverAnimation.isPlaying)
			{
				shaverAnimation.Play();
			}
		}

		if (Input.GetMouseButton(0))
		{
			MMVibrationManager.Haptic(HapticTypes.SoftImpact);
			_clickTimer += Time.deltaTime;
			if (_clickTimer >= 0.1f)
			{
				Vector3 newPos = (Input.mousePosition - previousPos) * Time.deltaTime;


				var zAxis = newPos.y;
				var xAxis = newPos.x;
				var _newPos = new Vector3(xAxis, 0f, zAxis);
				_newPos *= touchSensitivity;
				_newPos += transform.position;
				newPos.y = transform.position.y + heightAddition;
				_newPos.x = Mathf.Clamp(_newPos.x, minX, maxX);
				_newPos.z = Mathf.Clamp(_newPos.z, minY, maxY);
				transform.position = _newPos;

				previousPos = Input.mousePosition;
			}

		}

		if (Input.GetMouseButtonUp(0))
		{
			previousPos = Input.mousePosition;
			_clickTimer = 0f;
			shaverSound.Stop();
			shaverAnimation.Stop();
		}
	}


	void OnTriggerEnter(Collider other)
	{
		if (!enabled)
		{
			return;
		}

		if (other.gameObject.CompareTag("ExtraStitch") && Input.GetMouseButton(0))
		{
			if (colorControlFollower.thisMovementMode == ColorControlFollower.MovementMode.Manual)
			{
				other.gameObject.gameObject.GetComponent<MeshCollider>().enabled = false;

				irregularStitch data = new irregularStitch();
				int length = irregularStitches.Count;
				int index = 0;

				for (int i = 0; i < length; i++)
				{
					if (irregularStitches[i].spawnStitches == other.gameObject)
					{
						index = i;
						break;
					}
				}

				undoParticles.transform.position = transform.position - new Vector3(0, heightAddition, 0);
				undoParticles.startColor =
					irregularStitches[index].spawnStitches.GetComponent<MeshRenderer>().material.color * 0.8f;
				undoParticles.Emit(Random.Range(7, 11));

				MMVibrationManager.Haptic(HapticTypes.LightImpact, false, true, this);

				irregularStitches[index].spawnStitches.transform.localScale = new Vector3(
					irregularStitches[index].spawnStitches.transform.localScale.x, 0.25f,
					irregularStitches[index].spawnStitches.transform.localScale.z);

				for (int i = 0; i < irregularStitches[index].stitches.Count; i++)
				{
					irregularStitches[index].stitches[i].gameObject.SetActive(false);
				}

				_counter++;

				if (_counter == (int) firstValue || _counter == (int) secondValue)
				{
					GameController.Instance.SpawnParticleAtRandomPos(0, shaverParticle, true);
				}

				if (_counter == irregularStitches.Count)
				{
					_isDone = true;
					machineButton.SetActive(false);

					toolsPanel.SetActive(false);
					abovePanel.SetActive(false);
					machinePanel.SetActive(false);

					transform.DOMoveZ(10f, 1f).SetDelay(0.15f).OnComplete(() =>
					{
						shaverSound.Stop();
						FlipCanvasBack();
					});
				}

			}


		}
	}

	public void OnControlTypeChanged()
	{
		_counter = 0;
		int length = irregularStitches.Count;
		for (int i = 0; i < length; i++)
		{
			if (!irregularStitches[i].stitches[0].activeInHierarchy)
			{
				irregularStitches.Remove(irregularStitches[i]);
				i--;
				length = irregularStitches.Count;
			}
		}
	}

	void HandleGlue()
	{
		if (Input.GetMouseButton(0))
		{
			_spawnTime += Time.deltaTime;
			if (_spawnTime >= spawnTime)
			{
				GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				plane.GetComponent<Collider>().enabled = false;
				plane.transform.position = transform.position;
				plane.transform.localScale = Vector3.one * 0.1f;
				plane.AddComponent<Rigidbody>().AddRelativeForce(Vector3.down * dropSpeed, ForceMode.Force);

				_spawnTime = 0;
			}

			transition += Time.deltaTime * increaseSpeed;
			float value = skinnedMeshRenderer.GetBlendShapeWeight(0);
			value = Mathf.Lerp(blenshapeInitialValue, blenshapeFinalValue, transition);
			blendShapeValue = value;
			skinnedMeshRenderer.SetBlendShapeWeight(0, value);
			glueObject.position = currPos = Vector3.Lerp(initalPos, targetPos, transition);
		}


	}

	public void FlipCanvasBack()
	{
		camera.transform.DOMove(cameraPositionForCloth.position, 1f);
		camera.transform.DOLocalRotate(new Vector3(90, 0, 0), 1f).OnComplete(() =>
		{
			Vector3 currentCamPos = camera.transform.position;
			float camTargetY = currentCamPos.y + 8f;
			canvas.transform.DOLocalRotate(new Vector3(1.75f, 0, 0), 1.5f).SetDelay(0.25f).OnComplete(() =>
			{
				Invoke("RemoveCanvas", 0.1f);
			});
		});
	}

	public void RemoveCanvas()
	{
		GameController.Instance.tutorial.gameObject.SetActive(false);
		GameController.Instance.uiController.gamePanel.toolsPanel.SetActive(false);
		GameController.Instance.currentLevel.gameObject.transform.parent = null;
		canvas.transform.parent = null;
		// if (GameManager.Instance.CurrentLevel < 5)
		// {
		// 	colorControlFollower.FinishGame();
		// }
		
		canvas.transform.DOMoveZ(-30, 0.5f);
		StartCoroutine(ShowCloth());
	}

	public void ShowModel(int index)
	{

	}

	public void SelectMachine(int index)
	{
		for (int i = 0; i < shaverMachineModels.Length; i++)
		{
			shaverMachineModels[i].gameObject.SetActive(false);
		}

		if (index > 0)
		{
			if (PlayerPrefs.GetInt("Shaver" + index.ToString()) == 1)
			{
				shaverMachineModels[index].gameObject.SetActive(true);
			}
			else
			{
				shaverMachineModels[0].gameObject.SetActive(true);
			}
		}
		else
		{
			shaverMachineModels[0].gameObject.SetActive(true);
		}
	}

	public void UpdateLocks()
	{
		if (PlayerPrefs.HasKey("Shaver1"))
		{
			shaverLocks[0].SetActive(false);
		}

		if (PlayerPrefs.HasKey("Shaver2"))
		{
			shaverLocks[1].SetActive(false);
		}

		if (PlayerPrefs.HasKey("Shaver3"))
		{
			shaverLocks[2].SetActive(false);
		}

		if (PlayerPrefs.HasKey("Cloth1"))
		{
			clothLocks[0].SetActive(false);
		}

		if (PlayerPrefs.HasKey("Cloth2"))
		{
			clothLocks[1].SetActive(false);
		}

		if (PlayerPrefs.HasKey("Cloth3"))
		{
			clothLocks[2].SetActive(false);
		}

		if (PlayerPrefs.HasKey("Cloth4"))
		{
			clothLocks[3].SetActive(false);
		}

		if (PlayerPrefs.HasKey("Cloth5"))
		{
			clothLocks[4].SetActive(false);
		}
	}

	public IEnumerator ShowCloth()
	{
		Camera.main.cullingMask = cameraNewLayerMask;
		
		int clothNumber = PlayerPrefs.GetInt("SelectedClothNumber");
		int levelRange = GameManager.Instance.CurrentLevel / 5;
		int currentLevel = GameManager.Instance.CurrentLevel % 5;
		int startLevel = levelRange * 5;
		levelRange = levelRange * 5;

		for (int i = 0; i < 5; i++)
		{
			clothDetails[clothNumber].patchPositions[i].SetActive(true);
		}

		for (int j = 0; j < currentLevel; j++)
		{
			var levelImage = Instantiate(DataCarrier.Instance.levelsData[startLevel].lvlPrefab,
				clothDetails[clothNumber].patchPositions[j].transform);
			levelImage.EnableStitches();
			levelImage.transform.localPosition = levelImage.levelTransform.pos;
			levelImage.transform.localRotation = levelImage.levelTransform.rot;
			levelImage.transform.localScale = levelImage.levelTransform.scale;

			startLevel++;
		}

		//clothDetails[clothNumber].clothObject.SetActive(true);

		for (int i = 0; i < 5; i++)
		{
			clothDetails[clothNumber].dullSprites[i].sprite =
				DataCarrier.Instance.levelsData[levelRange].refrenceSprite;
			levelRange++;
		}

		 GameController.Instance.currentLevel.gameObject.transform.SetParent(clothDetails[clothNumber]
			.patchPositions[currentLevel].transform);
		
		clothDetails[clothNumber].dullSprites[currentLevel].gameObject.SetActive(false);
		
		

		// GameController.Instance.currentLevel.gameObject.transform.localPosition =
		// 	GameController.Instance.currentLevel.levelTransform.pos;
		GameController.Instance.currentLevel.gameObject.transform.localScale =
			GameController.Instance.currentLevel.levelTransform.scale;
		// GameController.Instance.currentLevel.gameObject.transform.localEulerAngles = 
		// 	GameController.Instance.currentLevel.levelTransform.rot.eulerAngles;

		var cameraPos = clothDetails[clothNumber].patchPositions[currentLevel].transform.GetChild(1).transform;
		cameraPos.SetParent(null);
		Camera.main.transform.parent.transform.DOMove(cameraPos.position, 1f);
		//Camera.main.transform.parent.transform.localEulerAngles = cameraPos.rotation.eulerAngles;

		yield return new WaitForSeconds(1);
		
		//Camera.main.transform.parent.transform.DOLocalRotate(new Vector3(70, 0, 0), 1f);
		Camera.main.transform.parent.transform.DOMove(
			clothDetails[clothNumber].cameraPosition, 2f);

		// GameController.Instance.currentLevel.gameObject.transform.DOLocalMove(
		// 	GameController.Instance.currentLevel.levelTransform.pos, 0.25f);
		// GameController.Instance.currentLevel.gameObject.transform.DOScale(
		// 	GameController.Instance.currentLevel.levelTransform.scale, 0.25f);
		// GameController.Instance.currentLevel.gameObject.transform.DOLocalRotate(
		// 	GameController.Instance.currentLevel.levelTransform.rot.eulerAngles, 0.25f);

		screenShotCamera.transform.SetParent(clothDetails[clothNumber].patchPositions[currentLevel].transform);
		screenShotCamera.transform.localPosition = Vector3.zero + new Vector3(0,1.5f,0);
		screenShotCamera.transform.localEulerAngles = Vector3.zero + new Vector3(90,0,0);

		yield return new WaitForSeconds(2f);
		colorControlFollower.FinishGame();
	}

	public void EnableCloth()
	{
		int clothNumber = PlayerPrefs.GetInt("SelectedClothNumber");
		int currentLevel = GameManager.Instance.CurrentLevel % 5;
		
		clothDetails[clothNumber].clothObject.SetActive(true);
		clothDetails[clothNumber].patchPositions[currentLevel].SetActive(true);
		clothDetails[clothNumber].dullSprites[currentLevel].gameObject.SetActive(false);
			
		GameController.Instance.currentLevel.transform.SetParent(clothDetails[clothNumber]
			.patchPositions[currentLevel].transform);
		GameController.Instance.currentLevel.transform.localPosition =
			GameController.Instance.currentLevel.levelTransform.pos;
		GameController.Instance.currentLevel.transform.localEulerAngles = 
			GameController.Instance.currentLevel.levelTransform.rot.eulerAngles;
		
	}

}