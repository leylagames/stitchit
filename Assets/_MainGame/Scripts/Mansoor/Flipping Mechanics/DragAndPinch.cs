﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragAndPinch : MonoBehaviour
{

	public GameObject[] UIFNotorTHis;

	public GameObject objToMove;

	private Vector3 mOffset;
	private float   mZCoord;

	// [SerializeField] Vector3 deltaChange;
	[SerializeField] float deltaDistance;

	// Vector3 previousPos;
	float  previousDistance;
	Camera Camera;

	[SerializeField] float         inverseLerp;
	[SerializeField] Vector2       minMax;
	[SerializeField]public Vector2       lerpedValue;
	[SerializeField] RectTransform image;
	public           GameObject    sliderObj;
	// Start is called before the first frame update
	void Start()
	{
		for (int i = 0; i < UIFNotorTHis.Length; i++)
		{
			UIFNotorTHis[i].SetActive(false);
		}
	
		Camera = Camera.main;
		objToMove.SetActive(true);
		// objToMove.transform.parent        = transform;
		// objToMove.transform.localPosition = new Vector3(0, 0, 4.25f);
		objToMove.transform.localScale    = new Vector3(0.25f,0.25f,0.25f);
		
		Vector3 localPos = image.localPosition;

		inverseLerp         = Mathf.InverseLerp(0f, 0.25f, objToMove.transform.localScale.x);
		localPos.x          = Mathf.Lerp(minMax.x, minMax.y, inverseLerp);
		image.localPosition = localPos;
	}

	// Update is called once per frame
	void Update()
	{
		Vector3 localPos = image.localPosition;

		inverseLerp         = Mathf.InverseLerp(0f, 0.25f, objToMove.transform.localScale.x);
		localPos.x          = Mathf.Lerp(minMax.x, minMax.y, inverseLerp);
		image.localPosition = localPos;
		
		lerpedValue   = Camera.WorldToViewportPoint(objToMove.transform.position);
		lerpedValue.x = Mathf.Clamp(lerpedValue.x, 0.05f, 0.95f);
		lerpedValue.y = Mathf.Clamp(lerpedValue.y, 0.15f, 0.85f);
		
		if (Input.touchCount == 1)
		{
			if (Input.GetMouseButtonDown(0))
			{
				mZCoord = Camera.main.WorldToScreenPoint(objToMove.transform.position).z;
				// Store offset = gameobject world pos - mouse world pos
				mOffset = objToMove.transform.position - GetMouseAsWorldPoint();
			}

			if (Input.GetMouseButton(0))
			{
				 objToMove.transform.position = GetMouseAsWorldPoint() + mOffset;
				
				 Vector3 pos = Camera.WorldToViewportPoint(objToMove.transform.position);
				 pos.x                        = Mathf.Clamp(pos.x, 0.05f, 0.95f);
				 pos.y                        = Mathf.Clamp(pos.y, 0.15f, 0.85f);
				 Vector3 nextPos                      = Camera.ViewportToWorldPoint(pos);
				 nextPos.y                    = 1.125f;
				 objToMove.transform.position = nextPos;
				
			}
			
			if (Input.GetMouseButtonUp(0))
			{
				mZCoord = Camera.main.WorldToScreenPoint(objToMove.transform.position).z;

				// Store offset = gameobject world pos - mouse world pos
				mOffset = objToMove.transform.position - GetMouseAsWorldPoint();
			}

		}
		
		if (Input.touchCount == 2)
		{
			Touch touch1 = Input.GetTouch(0);
			Touch touch2 = Input.GetTouch(1);
 
			if (touch1.phase == TouchPhase.Began || touch2.phase == TouchPhase.Began)
			{
				previousDistance = Vector2.Distance(touch1.position, touch2.position);
			}
 
			if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved)
			{
				float newDist = Vector2.Distance(touch1.position, touch2.position);
				deltaDistance = (previousDistance - newDist) * Time.deltaTime;
				
 
				// Your Code Here
				Vector3 scale = objToMove.transform.localScale;
				scale.x                        -= Time.deltaTime * deltaDistance;
				scale.x                        =  Mathf.Clamp(scale.x, 0f, 0.25f);
				scale.z                        =  scale.x;
				scale.y                        =  0.5f;
				
				objToMove.transform.localScale =  scale;
				
				
				previousDistance = newDist;
				
			}
		}
	}


	private Vector3 GetMouseAsWorldPoint()

	{
		// Pixel coordinates of mouse (x,y)
		Vector3 mousePoint = Input.mousePosition;

		// z coordinate of game object on screen
		mousePoint.z = mZCoord;

		// Convert it to world points
		return Camera.main.ScreenToWorldPoint(mousePoint);
	}

}