﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public GameObject abovePanel;
    public GameObject tools;
    public GameObject colorSelection;
    public GameObject undoButton;
    public GameObject tut02Text;
    public GameObject tut02Hand;

    public GameObject tut03Text;
    public GameObject tut03Hand;
 
    public GameObject tut01, tut02, tut03;
    public GameObject headingBG;

    [HideInInspector] public bool stitchSelected;

    private void Start()
    {
        // if (!PlayerPrefs.HasKey("TutorialFinished"))
        // {
        //     undoButton.SetActive(false);
        //     FirstTutorialShow();
        //     tools.SetActive(false);
        // }
    }

    public void FirstTutorialShow()
    {
        StartCoroutine(ShowTutorial01());
    }
    

    public IEnumerator ShowTutorial01()
    {
        yield return new WaitForSeconds(0.5f);
        headingBG.SetActive(true);
        if (PlayerPrefs.GetInt("Level") == 0)
        {
            tut01.SetActive(true);
        }
    }

    public void FinishTutorial01()
    {
        tut01.SetActive(false);
        StartCoroutine(SecondTutorialShow());
    }
    

    public IEnumerator SecondTutorialShow()
    {
        yield return new WaitForSeconds(0.25f);
        abovePanel.SetActive(true);
        tools.SetActive(true);
        if (PlayerPrefs.GetInt("Level") == 0)
        {
            tut02.SetActive(true);
        }
    }

    public void FinishTutorial02()
    {
        colorSelection.SetActive(true);
        tut02Text.SetActive(false);
        tut02Hand.SetActive(false);
        stitchSelected = true;
        StartCoroutine(ThirdTutorial());
        once = true;
    }

    public IEnumerator ThirdTutorial()
    {
        yield return new WaitForSeconds(0.25f);
        tut03.SetActive(true);
    }

    [HideInInspector]
    public bool once;
    
    public void FinishThirdTutorial()
    {
        tut03Text.SetActive(false);
        tut03Hand.SetActive(false);
        headingBG.SetActive(false);
        StartCoroutine(FourthTutorial());
    }

    public void RemoveTutorials()
    {
        this.gameObject.SetActive(false);
    }

    public IEnumerator FourthTutorial()
    {
        yield return new WaitForSeconds(0.25f);
        // tut04.SetActive(true);
    }

    public void FinishTutorials()
    {
        // tut04.SetActive(false);
        // headingBG.SetActive(false);
        // StartCoroutine(ShowLastText());
        // once = true;
    }

    public IEnumerator ShowLastText()
    {
        yield return new WaitForSeconds(0.5f);
        // tut05.SetActive(true);
        // yield return new WaitForSeconds(5f);
        // tut05.SetActive(false);
    }
}
