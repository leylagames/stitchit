﻿using System;
using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using UnityEngine;

public class ShineControllerUI : MonoBehaviour
{
    public float shineDuration;
    public float shineInterval;

    public ShinyEffectForUGUI effectMain;


    private void Awake()
    {
        effectMain = GetComponent<ShinyEffectForUGUI>();
    }

    private void OnEnable()
    {
        InvokeRepeating("StartShine",0,shineInterval);
    }

    private void StartShine()
    {
        StartCoroutine(ShineController(shineDuration));
    }

    private IEnumerator ShineController(float duration)
    {
        float counter = 0;
        float startValue = 0;
        float endValue = 1;
        float newValue = startValue;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            newValue = Mathf.Lerp(startValue, endValue, counter / duration);
            effectMain.location = newValue;
            yield return null;
        }
    }

    private void OnDisable()
    {
        CancelInvoke();
        StopAllCoroutines();
    }
}
