﻿
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class TapAndHoldScript : MonoBehaviour
{
    public TextMeshProUGUI text;
    
    private void OnEnable()
    {
        StartCoroutine(Show());
    }
    
    private IEnumerator Show()
    {
        float counter = 0;
        float startValue = 0;
        float endValue = 1;
        float duration = 1;
        float newValue = startValue;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            newValue = Mathf.Lerp(startValue, endValue, counter / duration);
            text.color = new Color(0, 0, 0, newValue);
            yield return null;
        }
    }
    
}
