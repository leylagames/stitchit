﻿using UnityEngine;

public class SewingMachineScript : MonoBehaviour
{
    public MeshRenderer threadColorMesh;
    public MeshRenderer otherMesh;
    public Animation machineAnimation;
    public int machineNumber;

    public void SetMaterial(Material colorMaterial)
    {
        threadColorMesh.material = colorMaterial;
        if (otherMesh)
        {
            if (machineNumber == 2)
            {
                otherMesh.sharedMaterials[2] = colorMaterial;
            }
            else
            {
                otherMesh.material = colorMaterial;
            }
        }

    }

    public void SetAnimation(bool check)
    {
        if (check)
        {
            if (!machineAnimation.isPlaying)
            {
                machineAnimation.Play();
            }
        }
        else
        {
            if (machineAnimation.isPlaying)
            {
                machineAnimation.Stop();
            }
        }
        
    }
}
