﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryScript : MonoBehaviour
{
    [System.Serializable] public class Item
    {
        public int[] price;
        public TextMeshProUGUI[] priceLabels;
        public GameObject[] locks;
        public GameObject[] selection;
    }
    
    public Image[] mainIcons;
    public GameObject[] mainItems;
    public TextMeshProUGUI cashLabel;
    
    public Color selected;
    public Color deselected;

    public Item stitchTypes;
    public Item machineTypes;
    public Item threadTypes;
    public Item frameTypes;
    public Item scissorTypes;
    public Item ironTypes;
    
    
    private int _currency;
    
    void Start()
    {
        if (!PlayerPrefs.HasKey("FirstTime"))
        {
            PlayerPrefs.SetInt("FirstTime",1);
            PurchaseStitchType(0);
            PurchaseMachineType(0);
            PurchaseFrameType(0);
            PurchaseScissorType(0);
            PurchaseIronType(0);
        }
        
        SelectCategory(PlayerPrefs.GetInt("InventoryCategory"));
        StartCoroutine(UpdateLabels());
        StartCoroutine(UpdateLocks());
        UpdateStats();
    }

    private void Update()
    {
        _currency = GameManager.Instance.GetPlayerCash();
        cashLabel.text = _currency.ToString("000");
    }

    private IEnumerator UpdateLabels()
    {
        for (int i = 0; i < stitchTypes.priceLabels.Length; i++)
        {
            stitchTypes.priceLabels[i].text = stitchTypes.price[i].ToString();
        }
        
        for (int i = 0; i < machineTypes.priceLabels.Length; i++)
        {
            machineTypes.priceLabels[i].text = machineTypes.price[i].ToString();
        }
        
        for (int i = 0; i < threadTypes.priceLabels.Length; i++)
        {
            threadTypes.priceLabels[i].text = threadTypes.price[i].ToString();
        }
        
        for (int i = 0; i < frameTypes.priceLabels.Length; i++)
        {
            frameTypes.priceLabels[i].text = frameTypes.price[i].ToString();
        }
        
        for (int i = 0; i < scissorTypes.priceLabels.Length; i++)
        {
            scissorTypes.priceLabels[i].text = scissorTypes.price[i].ToString();
        }
        
        for (int i = 0; i < ironTypes.priceLabels.Length; i++)
        {
            ironTypes.priceLabels[i].text = ironTypes.price[i].ToString();
        }

        yield return null;
    }

    private IEnumerator UpdateLocks()
    {
        for (int i = 0; i < stitchTypes.price.Length; i++)
        {
            if (PlayerPrefs.GetInt("StitchType" + i.ToString()) == 1)
            {
                stitchTypes.locks[i].SetActive(false);
            }
        }
        
        for (int i = 0; i < machineTypes.price.Length; i++)
        {
            if (PlayerPrefs.GetInt("MachineType" + i.ToString()) == 1)
            {
                machineTypes.locks[i].SetActive(false);
            }
        }
        
        for (int i = 0; i < threadTypes.price.Length; i++)
        {
            if (PlayerPrefs.GetInt("ThreadType" + i.ToString()) == 1)
            {
                threadTypes.locks[i].SetActive(false);
            }
        }
        
        for (int i = 0; i < frameTypes.price.Length; i++)
        {
            if (PlayerPrefs.GetInt("FrameType" + i.ToString()) == 1)
            {
                frameTypes.locks[i].SetActive(false);
            }
        }
        
        for (int i = 0; i < scissorTypes.price.Length; i++)
        {
            if (PlayerPrefs.GetInt("ScissorType" + i.ToString()) == 1)
            {
                scissorTypes.locks[i].SetActive(false);
            }
        }
        
        for (int i = 0; i < ironTypes.price.Length; i++)
        {
            if (PlayerPrefs.GetInt("IronType" + i.ToString()) == 1)
            {
                ironTypes.locks[i].SetActive(false);
            }
        }

        yield return null;
    }

    private void UpdateStats()
    {
        _currency = GameManager.Instance.GetPlayerCash();
        cashLabel.text = _currency.ToString("000");
        //SelectStitchType(PlayerPrefs.GetInt("StitchTypeSelected"));
        SelectMachineType(PlayerPrefs.GetInt("MachineTypeSelected"));
        //SelectThreadType(PlayerPrefs.GetInt("ThreadTypeSelected"));
        SelectFrameType(PlayerPrefs.GetInt("FrameTypeSelected"));
        SelectScissorType(PlayerPrefs.GetInt("ScissorTypeSelected"));
        SelectIronType(PlayerPrefs.GetInt("IronTypeSelected"));
    }

    public void SelectCategory(int index)
    {
        for (int i = 0; i < mainIcons.Length; i++)
        {
            mainIcons[i].color = deselected;
            mainItems[i].SetActive(false);
        }

        mainIcons[index].color = selected;
        mainItems[index].SetActive(true);
        PlayerPrefs.SetInt("InventoryCategory",0);
    }

    public void PurchaseStitchType(int index)
    {
        if (PlayerPrefs.GetInt("StitchType" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= stitchTypes.price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - stitchTypes.price[index]);
                PlayerPrefs.SetInt("StitchType" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                //PlayerPrefs.SetInt("StitchTypeSelected",index);
                UpdateStats();
            }
        }
        else
        {
            //PlayerPrefs.SetInt("StitchTypeSelected",index);
            UpdateStats();
        }
    }
    
    public void PurchaseMachineType(int index)
    {
        if (PlayerPrefs.GetInt("MachineType" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= machineTypes.price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - machineTypes.price[index]);
                PlayerPrefs.SetInt("MachineType" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                PlayerPrefs.SetInt("MachineTypeSelected",index);
                UpdateStats();
            }
        }
        else
        {
            PlayerPrefs.SetInt("MachineTypeSelected",index);
            UpdateStats();
        }
    }
    
    public void PurchaseThreadType(int index)
    {
        if (PlayerPrefs.GetInt("ThreadType" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= threadTypes.price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - threadTypes.price[index]);
                PlayerPrefs.SetInt("ThreadType" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                //PlayerPrefs.SetInt("ThreadTypeSelected",index);
                UpdateStats();
            }
        }
        else
        {
            //PlayerPrefs.SetInt("ThreadTypeSelected",index);
            UpdateStats();
        }
    }
    
    public void PurchaseFrameType(int index)
    {
        if (PlayerPrefs.GetInt("FrameType" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= frameTypes.price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - frameTypes.price[index]);
                PlayerPrefs.SetInt("FrameType" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                PlayerPrefs.SetInt("FrameTypeSelected",index);
                UpdateStats();
            }
        }
        else
        {
            PlayerPrefs.SetInt("FrameTypeSelected",index);
            UpdateStats();
        }
    }
    
    public void PurchaseScissorType(int index)
    {
        if (PlayerPrefs.GetInt("ScissorType" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= scissorTypes.price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - scissorTypes.price[index]);
                PlayerPrefs.SetInt("ScissorType" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                PlayerPrefs.SetInt("ScissorTypeSelected",index);
                UpdateStats();
            }
        }
        else
        {
            PlayerPrefs.SetInt("ScissorTypeSelected",index);
            UpdateStats();
        }
    }
    
    public void PurchaseIronType(int index)
    {
        if (PlayerPrefs.GetInt("IronType" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= ironTypes.price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - ironTypes.price[index]);
                PlayerPrefs.SetInt("IronType" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                PlayerPrefs.SetInt("IronTypeSelected",index);
                UpdateStats();
            }
        }
        else
        {
            PlayerPrefs.SetInt("IronTypeSelected",index);
            UpdateStats();
        }
    }

    private void SelectStitchType(int index)
    {
        for (int i = 0; i < stitchTypes.selection.Length; i++)
        {
            stitchTypes.selection[i].SetActive(false);
        }
        stitchTypes.selection[index].SetActive(true);
    }
    
    private void SelectMachineType(int index)
    {
        for (int i = 0; i < machineTypes.selection.Length; i++)
        {
            machineTypes.selection[i].SetActive(false);
        }
        machineTypes.selection[index].SetActive(true);
    }
    
    private void SelectThreadType(int index)
    {
        for (int i = 0; i < threadTypes.selection.Length; i++)
        {
            threadTypes.selection[i].SetActive(false);
        }
        threadTypes.selection[index].SetActive(true);
    }
    
    private void SelectFrameType(int index)
    {
        for (int i = 0; i < frameTypes.selection.Length; i++)
        {
            frameTypes.selection[i].SetActive(false);
        }
        frameTypes.selection[index].SetActive(true);
    }
    
    private void SelectScissorType(int index)
    {
        for (int i = 0; i < scissorTypes.selection.Length; i++)
        {
            scissorTypes.selection[i].SetActive(false);
        }
        scissorTypes.selection[index].SetActive(true);
    }
    
    private void SelectIronType(int index)
    {
        for (int i = 0; i < ironTypes.selection.Length; i++)
        {
            ironTypes.selection[i].SetActive(false);
        }
        ironTypes.selection[index].SetActive(true);
    }

    public void ExtraCashButton()
    {
        GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() + 1000);
        UpdateStats();
    }
}
