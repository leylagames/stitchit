﻿using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class ShopScript : MonoBehaviour
{
    public TextMeshProUGUI cashLabel;
    public FurnitureScript[] furnitures;
    public GameObject[] furnitureUI;
    public GameObject[] positionMarkers;
    public GameObject[] notificationIcons;
    public GameObject menuNotificationIcon;
    
    public Transform furnitureCamera;

    public ParticleSystem particle;
    
    private int _currency;
    private int _counter;

    void Start()
    {
        _counter = PlayerPrefs.GetInt("StoreCounter");
        PlayerPrefs.SetInt("StoreCounter",0);
        RePositionCamera();
        UpdateStats();
        StartCoroutine(UpdateLocks());
        PositionMarkers(false);
        UpdateLockImages();
    }

    private void Update()
    {
        _currency = GameManager.Instance.GetPlayerCash();
        cashLabel.text = _currency.ToString("000");
    }

    private void UpdateStats()
    {
        _currency = GameManager.Instance.GetPlayerCash();
        cashLabel.text = _currency.ToString("000");
        SelectFurniture01(PlayerPrefs.GetInt("FurnitureTypeSelected0"));
        SelectFurniture02(PlayerPrefs.GetInt("FurnitureTypeSelected1"));
        SelectFurniture03(PlayerPrefs.GetInt("FurnitureTypeSelected2"));
        SelectFurniture04(PlayerPrefs.GetInt("FurnitureTypeSelected3"));
    }

    private IEnumerator UpdateLocks()
    {
        for (int i = 0; i < furnitures.Length; i++)
        {
            for (int j = 0; j < furnitures[i].priceLabels.Length; j++)
            {
                if (PlayerPrefs.GetInt("Furniture" + i.ToString() + j.ToString()) == 1)
                {
                    furnitures[i].cover.SetActive(false);
                    furnitures[i].locks[j].SetActive(false);
                }
                else
                {
                    furnitures[i].priceLabels[j].text = furnitures[i].price[j].ToString();
                }
            }
        }

        yield return null;
    }

    public void NextButton()
    {
        if (_counter == furnitures.Length - 1)
        {
            _counter = 0;
        }
        else
        {
            _counter++;
        }
        RePositionCamera();
    }

    public void EnableUI(int index)
    {
        for (int i = 0; i < furnitures.Length; i++)
        {
            furnitureUI[i].SetActive(false);
        }
        furnitureUI[index].SetActive(true);
    }

    public void RePositionCamera()
    {
        furnitureCamera.DOMove(furnitures[_counter].cameraPosition.position, 1f);
        furnitureCamera.DORotate(furnitures[_counter].cameraPosition.eulerAngles, 1f);
        EnableUI(_counter);
    }
    
    public void PreviousButton()
    {
        if (_counter == 0)
        {
            _counter = furnitures.Length - 1;
        }
        else
        {
            _counter--;
        }
        RePositionCamera();
    }

    public void PurchaseFurniture01(int index)
    {
        if (PlayerPrefs.GetInt("Furniture0" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= furnitures[0].price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - furnitures[0].price[index]);
                PlayerPrefs.SetInt("Furniture0" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                PlayerPrefs.SetInt("FurnitureTypeSelected0",index);
                UpdateStats();
                particle.Play();
            }
        }
        else
        {
            PlayerPrefs.SetInt("FurnitureTypeSelected0",index);
            UpdateStats();
            particle.Play();
        }
    }
    
    public void PurchaseFurniture02(int index)
    {
        if (PlayerPrefs.GetInt("Furniture1" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= furnitures[1].price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - furnitures[1].price[index]);
                PlayerPrefs.SetInt("Furniture1" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                PlayerPrefs.SetInt("FurnitureTypeSelected1",index);
                UpdateStats();
                particle.Play();
            }
        }
        else
        {
            PlayerPrefs.SetInt("FurnitureTypeSelected1",index);
            UpdateStats();
            particle.Play();
        }
    }
    
    public void PurchaseFurniture03(int index)
    {
        if (PlayerPrefs.GetInt("Furniture2" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= furnitures[2].price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - furnitures[2].price[index]);
                PlayerPrefs.SetInt("Furniture2" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                PlayerPrefs.SetInt("FurnitureTypeSelected2",index);
                UpdateStats();
                particle.Play();
            }
        }
        else
        {
            PlayerPrefs.SetInt("FurnitureTypeSelected2",index);
            UpdateStats();
            particle.Play();
        }
    }
    
    public void PurchaseFurniture04(int index)
    {
        if (PlayerPrefs.GetInt("Furniture3" + index.ToString()) == 0)
        {
            if (GameManager.Instance.GetPlayerCash() >= furnitures[3].price[index])
            {
                GameManager.Instance.SetPlayerCash(GameManager.Instance.GetPlayerCash() - furnitures[3].price[index]);
                PlayerPrefs.SetInt("Furniture3" + index.ToString() , 1);
                StartCoroutine(UpdateLocks());
                PlayerPrefs.SetInt("FurnitureTypeSelected3",index);
                UpdateStats();
                particle.Play();
            }
        }
        else
        {
            PlayerPrefs.SetInt("FurnitureTypeSelected3",index);
            UpdateStats();
            particle.Play();
        }
    }

    public void SelectFurniture01(int index)
    {
        if (!PlayerPrefs.HasKey("FurnitureTypeSelected0"))
        {
            furnitures[0].cover.SetActive(true);
            for (int i = 0; i < furnitures[0].models.Length; i++)
            {
                furnitures[0].models[i].SetActive(false);
            }
        }
        else
        {
            furnitures[0].cover.SetActive(false);
            for (int i = 0; i < furnitures[0].models.Length; i++)
            {
                furnitures[0].models[i].SetActive(false);
            }
            furnitures[0].models[index].SetActive(true);
        }
    }
    
    public void SelectFurniture02(int index)
    {
        if (!PlayerPrefs.HasKey("FurnitureTypeSelected1"))
        {
            furnitures[1].cover.SetActive(true);
            for (int i = 0; i < furnitures[1].models.Length; i++)
            {
                furnitures[1].models[i].SetActive(false);
            }
        }
        else
        {
            furnitures[1].cover.SetActive(false);
            for (int i = 0; i < furnitures[1].models.Length; i++)
            {
                furnitures[1].models[i].SetActive(false);
            }
            furnitures[1].models[index].SetActive(true);
        }
    }
    
    public void SelectFurniture03(int index)
    {
        if (!PlayerPrefs.HasKey("FurnitureTypeSelected2"))
        {
            furnitures[2].cover.SetActive(true);
            for (int i = 0; i < furnitures[2].models.Length; i++)
            {
                furnitures[2].models[i].SetActive(false);
            }
        }
        else
        {
            furnitures[2].cover.SetActive(false);
            for (int i = 0; i < furnitures[2].models.Length; i++)
            {
                furnitures[2].models[i].SetActive(false);
            }
            furnitures[2].models[index].SetActive(true);
        }
    }
    
    public void SelectFurniture04(int index)
    {
        if (!PlayerPrefs.HasKey("FurnitureTypeSelected3"))
        {
            furnitures[3].cover.SetActive(true);
            for (int i = 0; i < furnitures[3].models.Length; i++)
            {
                furnitures[3].models[i].SetActive(false);
            }
        }
        else
        {
            furnitures[3].cover.SetActive(false);
            for (int i = 0; i < furnitures[3].models.Length; i++)
            {
                furnitures[3].models[i].SetActive(false);
            }
            furnitures[3].models[index].SetActive(true);
        }
    }

    public void PositionMarkers(bool show)
    {
        for (int i = 0; i < positionMarkers.Length; i++)
        {
            positionMarkers[i].SetActive(show);
        }
    }

    public void SelectFurniture(int index)
    {
        switch (index)
        {
            case 0:
                if (PlayerPrefs.GetInt("UnlockedStoreItem0") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem0",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected1",0);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 1:
                if (PlayerPrefs.GetInt("UnlockedStoreItem1") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem1",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected0",0);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 2:
                if (PlayerPrefs.GetInt("UnlockedStoreItem2") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem2",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected2",0);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 3:
                if (PlayerPrefs.GetInt("UnlockedStoreItem3") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem3",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected3",0);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 4:
                if (PlayerPrefs.GetInt("UnlockedStoreItem4") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem4",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected1",1);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 5:
                if (PlayerPrefs.GetInt("UnlockedStoreItem5") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem5",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected0",1);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 6:
                if (PlayerPrefs.GetInt("UnlockedStoreItem6") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem6",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected2",1);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 7:
                if (PlayerPrefs.GetInt("UnlockedStoreItem7") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem7",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected3",1);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 8:
                if (PlayerPrefs.GetInt("UnlockedStoreItem8") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem8",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected1",2);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 9:
                if (PlayerPrefs.GetInt("UnlockedStoreItem9") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem9",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected0",2);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 10:
                if (PlayerPrefs.GetInt("UnlockedStoreItem10") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem10",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected2",2);
                    UpdateStats();
                    particle.Play();
                }
                break;
            case 11:
                if (PlayerPrefs.GetInt("UnlockedStoreItem11") == 1)
                {
                    PlayerPrefs.SetInt("UnlockedStoreItem11",2);
                    PlayerPrefs.SetInt("FurnitureTypeSelected3",2);
                    UpdateStats();
                    particle.Play();
                }
                break;
        }
        UpdateNotificationIcons();
    }

    public void UpdateLockImages()
    {
        if (PlayerPrefs.HasKey("UnlockedStoreItem0"))
        {
            furnitures[1].locks[0].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem1"))
        {
            furnitures[0].locks[0].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem2"))
        {
            furnitures[2].locks[0].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem3"))
        {
            furnitures[3].locks[0].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem4"))
        {
            furnitures[1].locks[1].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem5"))
        {
            furnitures[0].locks[1].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem6"))
        {
            furnitures[2].locks[1].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem7"))
        {
            furnitures[3].locks[1].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem8"))
        {
            furnitures[1].locks[2].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem9"))
        {
            furnitures[0].locks[2].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem10"))
        {
            furnitures[2].locks[2].SetActive(false);
        }
        if (PlayerPrefs.HasKey("UnlockedStoreItem11"))
        {
            furnitures[3].locks[2].SetActive(false);
        }
        
        UpdateNotificationIcons();
    }

    public void UpdateNotificationIcons()
    {
        if (PlayerPrefs.GetInt("UnlockedStoreItem0") == 1)
        {
            notificationIcons[0].SetActive(true);
        }
        else
        {
            notificationIcons[0].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem1") == 1)
        {
            notificationIcons[1].SetActive(true);
        }
        else
        {
            notificationIcons[1].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem2") == 1)
        {
            notificationIcons[2].SetActive(true);
        }
        else
        {
            notificationIcons[2].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem3") == 1)
        {
            notificationIcons[3].SetActive(true);
        }
        else
        {
            notificationIcons[3].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem4") == 1)
        {
            notificationIcons[4].SetActive(true);
        }
        else
        {
            notificationIcons[4].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem5") == 1)
        {
            notificationIcons[5].SetActive(true);
        }
        else
        {
            notificationIcons[5].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem6") == 1)
        {
            notificationIcons[6].SetActive(true);
        }
        else
        {
            notificationIcons[6].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem7") == 1)
        {
            notificationIcons[7].SetActive(true);
        }
        else
        {
            notificationIcons[7].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem8") == 1)
        {
            notificationIcons[8].SetActive(true);
        }
        else
        {
            notificationIcons[8].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem9") == 1)
        {
            notificationIcons[9].SetActive(true);
        }
        else
        {
            notificationIcons[9].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem10") == 1)
        {
            notificationIcons[10].SetActive(true);
        }
        else
        {
            notificationIcons[10].SetActive(false);
        }
        if (PlayerPrefs.GetInt("UnlockedStoreItem11") == 1)
        {
            notificationIcons[11].SetActive(true);
        }
        else
        {
            notificationIcons[11].SetActive(false);
        }
        
        UpdateMainMenuNotificationIcon();
    }

    public void UpdateMainMenuNotificationIcon()
    {
        if (PlayerPrefs.GetInt("UnlockedStoreItem0") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem1") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem2") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem3") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem4") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem5") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem6") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem7") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem8") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem9") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem10") == 1 ||
            PlayerPrefs.GetInt("UnlockedStoreItem11") == 1)
        {
            menuNotificationIcon.SetActive(true);
        }
        else
        {
            menuNotificationIcon.SetActive(false);
        }
    }
}
