﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScript : MonoBehaviour
{
    public Image loadingFillBar;
    public GameObject mainScreen;
    public GameObject loadingScreen;
    public GameObject levelScreen;
    private bool _isRunning;
    private AsyncOperation _async = null;

    public static bool showLevelSelection;

    void Start()
    {
        if (PlayerPrefs.GetInt("Level") == 0)
        {
            StartCoroutine(LoadLevel());
            GameManager.Instance.CurrentLevel = 0;
        }
        
        StartCoroutine(Loading(1.5f,true));
        
        if (!PlayerPrefs.HasKey("VibrationToggle"))
        {
            PlayerPrefs.SetInt("VibrationToggle",1);
        }
        if (!PlayerPrefs.HasKey("SoundToggle"))
        {
            PlayerPrefs.SetInt("SoundToggle",1);
        }
    }

    private IEnumerator Loading(float duration , bool start)
    {
        if (_isRunning)
        {
            yield break;
        }
        _isRunning = true;
        float counter = 0;
        float startValue = 0;
        float endValue = 1;

        float newValue = startValue;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            newValue = Mathf.Lerp(startValue, endValue, counter / duration);
            loadingFillBar.fillAmount = newValue;
            yield return null;
        }
        _isRunning = false;
        
        if (start)
        {
            LoadGame();
        }
        else
        {
            _async.allowSceneActivation = true;
        }
    }

    public void LoadGame()
    {
        if (PlayerPrefs.GetInt("Level") > 0)
        {
            if (showLevelSelection)
            {
                loadingScreen.SetActive(false);
                levelScreen.SetActive(true);
            }
            else
            {
                loadingScreen.SetActive(false);
                mainScreen.SetActive(true);
            }
            StartCoroutine(LoadLevel());
        }
        else
        {
            _async.allowSceneActivation = true;
        }
    }

    public void StartGameButtonClick()
    {

    }

    public void PlayButtonClick()
    {
        mainScreen.SetActive(false);
        GameManager.Instance.CurrentLevel = PlayerPrefs.GetInt("Level");
        loadingScreen.SetActive(true);
        StartCoroutine(Loading(1,false));
    }
    
    public void PlayButtonClick(int index)     
    {
        mainScreen.SetActive(false);
        GameManager.Instance.CurrentLevel = index;
        loadingScreen.SetActive(true);
        StartCoroutine(Loading(1, false));
    }

    private IEnumerator LoadLevel()
    {
        yield return null;
        _async = SceneManager.LoadSceneAsync(1);
        _async.allowSceneActivation = false;
    }
}
