﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FurnitureScript : MonoBehaviour
{
    public int[] price;
    public TextMeshProUGUI[] priceLabels;
    public GameObject[] models;
    public GameObject[] locks;
    public GameObject cover;
    public Transform cameraPosition;
    
}
