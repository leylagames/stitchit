﻿using DG.Tweening;
using TMPro;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public DOTweenAnimation levelsPanel;
    public DOTweenAnimation mainPanel;
    public DOTweenAnimation shopPanel;
    public TextMeshProUGUI playLabel;
    public LevelScreenController levelController;
    public static bool showShop = false;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("SoundToggle"))
        {
            PlayerPrefs.SetInt("SoundToggle",1);
        }
        if (!PlayerPrefs.HasKey("MusicToggle"))
        {
            PlayerPrefs.SetInt("MusicToggle",1);
        }
        if (!PlayerPrefs.HasKey("VibrationToggle"))
        {
            PlayerPrefs.SetInt("VibrationToggle",1);
        }

        if (showShop)
        {
            ShowShop();
            showShop = false;
        }
    }

    public void PlayButtonClick()
    {
        levelController.StartCoroutine("LevelStart");
    }

    public void ShowShop()
    {
        shopPanel.DORestartById("ScaleIn");
        mainPanel.DORestartById("ScaleOut");
    }
}
