﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ToolData
{
    public int id;
    public GameObject priceImage;


}
public class ToolShopController : MonoBehaviour
{
    public AudioSource normalClickSound;
    public AudioSource unlockedSound;
    public TextMeshProUGUI totalCoins;
    public ToolData[] toolData;
    public GameObject[] selectionBackground;
    
    void Start()
    {
        if (!PlayerPrefs.HasKey("SewingMachine"))
        {
            PlayerPrefs.SetInt("SewingMachine",0);
        }
        
        UpdateCoins();
        InitializeShop();
    }

    void UpdateCoins()
    {
        totalCoins.text = GameManager.Instance.TotalCoins.ToString();

        for (int i = 0; i < toolData.Length; i++)
        {
            
            if (PlayerPrefs.GetInt("ShopItem" + i, 0) == 0 && GameManager.Instance.TotalCoins >= int.Parse(toolData[i].priceImage.transform.Find("Price").GetComponent<Text>().text.ToString()))
            {
                //toolData[i].priceImage.transform.parent.Find("Hidden").gameObject.SetActive(false);
                PlayerPrefs.SetInt("Sahir" + i,1);
            }
            else if (PlayerPrefs.GetInt("ShopItem" + i, 0) == 0 && GameManager.Instance.TotalCoins < int.Parse(toolData[i].priceImage.transform.Find("Price").GetComponent<Text>().text.ToString()))
            {
                if (!PlayerPrefs.HasKey("Sahir" + i))
                {
                    //toolData[i].priceImage.transform.parent.Find("Hidden").gameObject.SetActive(true);
                }
            }

        }

    }
    void InitializeShop()
    {
        for(int i=0; i<toolData.Length; i++)
        {
            if(PlayerPrefs.GetInt("ShopItem" + i, 0) == 1)
            {
                toolData[i].priceImage.SetActive(false);
                //toolData[i].priceImage.transform.parent.Find("Hidden").gameObject.SetActive(false);
            }
        }

        if (PlayerPrefs.HasKey("SewingMachine"))
        {
            SelectShopItems(PlayerPrefs.GetInt("SewingMachine"));
        }
    }

    public void BuyTool(int index)
    {
        if (PlayerPrefs.GetInt("ShopItem" + index, 0)==0 && (GameManager.Instance.TotalCoins >= int.Parse(toolData[index].priceImage.transform.Find("Price").GetComponent<Text>().text.ToString())))
        {
            toolData[index].priceImage.SetActive(false);
            //toolData[index].priceImage.transform.parent.Find("Hidden").gameObject.SetActive(false);
            GameManager.Instance.TotalCoins -= int.Parse(toolData[index].priceImage.transform.Find("Price").GetComponent<Text>().text.ToString());
            //set bool to 1 if bought
            PlayerPrefs.SetInt("ShopItem" +index, 1);
            UpdateCoins();
            SelectShopItems(index);
            unlockedSound.Play();
        }
        else if(PlayerPrefs.GetInt("ShopItem" + index, 0) == 1)
        {
            SelectShopItems(index);
            normalClickSound.Play();
        }
    }

    private void SelectShopItems(int index)
    {
        PlayerPrefs.SetInt("SewingMachine",index);
        RemoveAllSelectionBG();
        selectionBackground[index].SetActive(true);
    }


    public void RemoveAllSelectionBG()
    {
        for (int i = 0; i < selectionBackground.Length; i++)
        {
            selectionBackground[i].SetActive(false);
        }
    }
    
    public void RewardCoins()
    {
        GameManager.Instance.TotalCoins += 200;
        UpdateCoins();
    }
}