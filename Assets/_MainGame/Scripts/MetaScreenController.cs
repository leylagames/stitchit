﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MetaScreenController : MonoBehaviour
{
    public GameObject[] customers;
    private int customerIndex = 0;
    private void Start()
    {
        TurnOnCustomer();
    }
    public void TurnOnCustomer()
    {
        customerIndex = Random.Range(0, 4);
        customers[customerIndex].SetActive(true);
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.name == "ShopIcon")
                {
                    Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object

                    UnlockMetaObject(hit.transform.parent, hit.transform.gameObject);
                }
            }
        }
    }

    void UnlockMetaObject(Transform objectToUnlock, GameObject shopIcon)
    {
        shopIcon.SetActive(false);
        objectToUnlock.transform.GetChild(1).gameObject.SetActive(false);
        objectToUnlock.GetComponent<MeshRenderer>().enabled = true;
        objectToUnlock.transform.Find("UnlockParticle").GetComponent<ParticleSystem>().Play();
    }

    public void Ending()
    {
        int animIndex = Random.Range(1, 4);
        customers[customerIndex].GetComponent<Animator>().SetTrigger(animIndex.ToString());
        customers[customerIndex].transform.Find("Clothes").GetComponent<Animator>().SetTrigger(animIndex.ToString());
    }
}
