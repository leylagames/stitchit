﻿using UnityEngine;
[System.Serializable]
public class Waypoint : MonoBehaviour
{
    public bool stopDrawing;
    public PointType colorIndex = PointType.None;
    public int areaId;
    public bool isDrawn;
}
