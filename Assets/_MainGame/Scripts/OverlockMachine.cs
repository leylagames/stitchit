﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class OverlockMachine : MonoBehaviour
{
    [Header("UI")] public GameObject referenceImage;
    public GameObject toolsPanel;
    public EventTrigger overlockButtonTrigger;
    public LayerMask cameraMask;
    
    [Header("Gameplay")]
    public Animation machineAnim;
    public Vector3 offset;
    public float machineSpeed = 3f;
    public List<Transform> _points;

    [Header("Camera")]
    public Transform mainCamera;
    public Transform cameraPosOverlocking;

    private bool _stopMachine;
    private bool _startMachine;
    private int _counter;
    private Transform _overlockContainer;
    private bool _enableFollowCam;

    public void SetButtonTriggers()
    {
        EventTrigger trigger = overlockButtonTrigger;
		
        EventTrigger.Entry entryDown = new EventTrigger.Entry();
        entryDown.eventID = EventTriggerType.PointerDown;
        entryDown.callback.AddListener((data) =>
        {
            _startMachine = true;
        });
        trigger.triggers.Add(entryDown);
		
        EventTrigger.Entry entryUp = new EventTrigger.Entry();
        entryUp.eventID = EventTriggerType.PointerUp;
        entryUp.callback.AddListener((data) =>
        {
            _startMachine = false;
        });
        trigger.triggers.Add(entryUp);
        
        overlockButtonTrigger.gameObject.SetActive(true);
    }

    public void HideUI()
    {
        toolsPanel.SetActive(false);
        referenceImage.SetActive(false);
    }

    public void Initialize()
    {
        Camera.main.cullingMask = cameraMask;
        transform.parent.GetChild(0).gameObject.SetActive(false);
        gameObject.SetActive(true);
        SetButtonTriggers();
        GameController.Instance.currentLevel.obstaclesContainer.gameObject.SetActive(false);
        
        _counter = 0;
        _overlockContainer = GameController.Instance.currentLevel.overlockStitchContainer;

        for (int i = 0; i < _overlockContainer.childCount; i++)
        {
            _points.Add(_overlockContainer.GetChild(i));
        }
        
        _points.Add(_overlockContainer.GetChild(0));
        
        HideUI();
        transform.DOMove(_points[_counter].position, 0.5f).OnComplete(() =>
        {
            var targetPos = transform.position + offset;
            mainCamera.transform.DOMove(targetPos, 0.5f).OnComplete(() =>
            {
                _enableFollowCam = true;
            });
        });
        
        // mainCamera.transform.DOMove(cameraPosOverlocking.position, 1f);
        // mainCamera.transform.DORotate(cameraPosOverlocking.eulerAngles, 1f).OnComplete(() =>
        // {
        //     HideUI();
        //     transform.DOMove(_points[_counter].position, 0.5f).OnComplete(() =>
        //     {
        //         var targetPos = transform.position + offset;
        //         mainCamera.transform.DOMove(targetPos, 0.5f).OnComplete(() =>
        //         {
        //             _enableFollowCam = true;
        //         });
        //     });
        // });
    }

    public void Update()
    {
        if (_enableFollowCam)
        {
            var targetPos = transform.position + offset;
            mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, targetPos, Time.deltaTime * 10);
            transform.LookAt(_points[_counter].position);
        }
        
        if (_startMachine)
        {
            machineAnim.Play();
            var newpos = new Vector3(_points[_counter].position.x + 0.2f, _points[_counter].position.y,
                _points[_counter].position.z);
            transform.position =
                Vector3.MoveTowards(transform.position, _points[_counter].position, Time.deltaTime * machineSpeed);
            

            if (Vector3.Distance(transform.position, _points[_counter].transform.position) < 0.01f)
            {
                if (_counter < _points.Count - 1)
                {
                    _counter++;
                }
                else
                {
                    GameController.Instance.flipMechanics.RemoveCanvas();
                    gameObject.SetActive(false);
                }
            }
        }
        else
        {
            machineAnim.Stop();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("OverlockStitch"))
        {
            other.GetComponent<Collider>().enabled = false;
            other.transform.localScale = Vector3.zero;
            other.transform.GetChild(0).gameObject.SetActive(true);
            other.transform.GetChild(1).gameObject.SetActive(true);
            other.transform.DOScale(Vector3.one, 0.25f);
        }
    }
}
