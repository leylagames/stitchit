﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class CustomerClothesScript : MonoBehaviour
{
    public GameObject[] tops;
    public GameObject[] bottoms;

    public void SetClothes(int top, int bottom)
    {
        tops[top].SetActive(true);
        bottoms[bottom].SetActive(true);
    }
}
