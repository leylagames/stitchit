﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeScreenController : MonoBehaviour
{
    public GameObject soundToggle;
    public GameObject musicToggle;
    public GameObject vibarationToggle;
    void Start()
    {
        //Initialize prefs and data
        if (GameManager.Instance.SoundToggle == 0)
        {
            AudioListener.volume = 0;
            soundToggle.GetComponent<Animator>().SetTrigger("Off");
        }
        else if(GameManager.Instance.SoundToggle == 1)
        {
            AudioListener.volume = 1;
            soundToggle.GetComponent<Animator>().SetTrigger("On");
        }


        if (GameManager.Instance.MusicToggle == 0)
        {
            musicToggle.GetComponent<Animator>().SetTrigger("Off");
        }
        else if (GameManager.Instance.MusicToggle == 1)
        {
            musicToggle.GetComponent<Animator>().SetTrigger("On");
        }


        if (GameManager.Instance.VibrationToggle == 0)
        {
            vibarationToggle.GetComponent<Animator>().SetTrigger("Off");
        }
        else if (GameManager.Instance.VibrationToggle == 1)
        {
            vibarationToggle.GetComponent<Animator>().SetTrigger("On");
        }
    }

    void RemoveAds()
    {
        
    }

    public void ToggleSound()
    {
        if(GameManager.Instance.SoundToggle == 1)
        {
            GameManager.Instance.SoundToggle = 0;
            AudioListener.volume = 0;
            soundToggle.GetComponent<Animator>().SetTrigger("Off");
        }
        else if(GameManager.Instance.SoundToggle == 0)
        {
            GameManager.Instance.SoundToggle = 1;
            AudioListener.volume = 1;
            soundToggle.GetComponent<Animator>().SetTrigger("On");
        }
    }
    public void ToggleMusic()
    {
        if (GameManager.Instance.MusicToggle == 1)
        {
            GameManager.Instance.MusicToggle = 0;
            musicToggle.GetComponent<Animator>().SetTrigger("Off");
        }
        else if (GameManager.Instance.MusicToggle == 0)
        {
            GameManager.Instance.MusicToggle = 1;
            musicToggle.GetComponent<Animator>().SetTrigger("On");
        }
    }
    public void ToggleVibration()
    {
        if (GameManager.Instance.VibrationToggle == 1)
        {
            GameManager.Instance.VibrationToggle = 0;
            vibarationToggle.GetComponent<Animator>().SetTrigger("Off");
        }
        else if (GameManager.Instance.VibrationToggle == 0)
        {
            GameManager.Instance.VibrationToggle = 1;
            vibarationToggle.GetComponent<Animator>().SetTrigger("On");
        }
    }
}
