﻿using UnityEngine;
using Random = UnityEngine.Random;

public class BackgroundWall : MonoBehaviour
{
    public Material[] materials;
    private MeshRenderer _mesh;

    private void Awake()
    {
        _mesh = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        if (_mesh)
        {
            var rand = Random.Range(0, materials.Length);
            _mesh.material = materials[rand];
        }
    }
}
