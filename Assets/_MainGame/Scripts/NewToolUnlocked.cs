﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewToolUnlocked : MonoBehaviour
{
    public int[] unlockingLevels;
    public GameObject[] toolImages;
    public GameObject toolUnlockPanel;

    public Button getItButton;

    private int _toolUnlockedValue;

    private void Start()
    {
        for (int i = 0; i < unlockingLevels.Length; i++)
        {
            if (GameManager.Instance.CurrentLevel == unlockingLevels[i])
            {
                _toolUnlockedValue = i;
                StartCoroutine(ShowToolUnlock());
                break;
            }
        }
        
        getItButton.onClick.AddListener(() =>
        {
            ContinueButton();
				
        });
    }

    private IEnumerator ShowToolUnlock()
    {
        yield return new WaitForSeconds(1);
        toolUnlockPanel.SetActive(true);
        toolImages[_toolUnlockedValue].SetActive(true);

        switch (_toolUnlockedValue)
        {
            case 0:
                PlayerPrefs.SetInt("Stitch1",1);
                break;
            case 1:
                PlayerPrefs.SetInt("Machine1",1);
                break;
            case 2:
                PlayerPrefs.SetInt("Stitch2",1);
                break;
            case 3:
                PlayerPrefs.SetInt("Machine2",1);
                break;
            case 4:
                PlayerPrefs.SetInt("Machine3",1);
                break;
        }
    }

    public void ContinueButton()
    {
        toolUnlockPanel.SetActive(false);
    }
}
