﻿ Shader "Custom/CombineTextureAlphaDifference"
 {
     Properties 
     {
        _Color ("Main Color", Color) = (1,1,1,1)
        _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {} 
        _Cutoff ("Cutoff Alpha",Range(0,1)) = 0.5
        _BlendTex ("Blend (RGB)", 2D) = "white"
        _BlendAlpha ("Blend Alpha", float) = 0
         
     }
     SubShader 
     {
         Tags { "Queue"="Transparent" "RenderType"="Transparent" }
         
        Lighting Off
        LOD 200
        Blend SrcAlpha OneMinusSrcAlpha
  
        CGPROGRAM
        // #pragma surface surf Lambert
        #pragma surface surf Lambert alpha

        uniform float _Cutoff;
        fixed4 _Color;
        sampler2D _MainTex;
        sampler2D _BlendTex;
        float _BlendAlpha;
       
        
        struct Input {
          float2 uv_MainTex;
          float2 uv_BlendTex;
        };
  
        void surf (Input IN, inout SurfaceOutput o) {

              float4 previousTex = tex2D(_MainTex, IN.uv_MainTex);
                float4 newTex = tex2D(_BlendTex, IN.uv_BlendTex);
                
                if (newTex.a > _Cutoff)
                {
                    previousTex = newTex;
                }
                o.Albedo = previousTex.rgb;
                o.Alpha  = previousTex.a;

        }
        ENDCG
     }
  
     Fallback "Transparent/VertexLit"
 }